﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using UnityEngine;
using System.IO;

public class FaceSyntax {

    public List<List<int>> _vertexIdx;
    public Dictionary<string, int> _facePart;
    public List<string> _facePartList;

    private bool _adjustedOrNot;
    //public List<int> _foreHeadIdx;
    //public List<int> _wholeFaceIdx;
    //public List<int> _leftEyeIdx;
    //public List<int> _rightEyeIdx;
    //public List<int> _leftEyeBrowIdx;
    //public List<int> _rightEyeBrowIdx;

    //public List<int> _eyeCornerPointsIdx;
    //public List<int> _noseIdx;
    //public List<int> _nostrilIdx;
    //public List<int> _noseTipIdx;
    //public List<int> _lipOuterLineIdx;
    //public List<int> _lipInnerLineIdx;
    //public List<int> _mouthCornerPointsIdx;
    //public List<int> _jawIdx;
    //public List<int> _eyeLineIdx;
    //public List<int> _lipUpperOuterLineIdx;
    //public List<int> _lipUpperInnerLineIdx;
    //public List<int> _lipLowerInnerLineIdx;
    //public List<int> _lipLowerOuterLineIdx;
    //public List<int> _leftCheekIdx;
    //public List<int> _rightCheekIdx;
    //public List<int> _pitchAxisLinePointsIdx;
    //public List<int> _midLinePointsIdx;
    //public List<int> _noseAlaeIdx;
    
    public List<List<int>> _contourIdxLine;

    //public List<int> lip_index;

    // Use this for initialization
    public void LoadFaceVertexSyntaxFile() {
        //Read Vertex Syntax
        _vertexIdx = new List<List<int>>();
        _facePart = new Dictionary<string, int>();
        _facePartList = new List<string>();

        int currFacePartIdx = 0;
        using (StreamReader sr = new StreamReader("Assets/Resources/vertex_syntax/facial_feature_index.txt"))
        {
            // Read the stream to a string, and write the string to the console.
            String line;
            while ((line = sr.ReadLine()) != null)
            {
                String[] line_contents = line.Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (line_contents.Length < 3) // 데이터가 없을 때,
                {
                    continue;
                }

                List<int> vertexIndex = new List<int>();
                for (int i = 0; i < line_contents.Length - 2; i++)
                {
                    int idx = Int32.Parse(line_contents[i + 2]);
                    vertexIndex.Add(idx);
                }

                if(line_contents[0] == "m_vForeHead")
                {
                    _facePart.Add("foreHead", currFacePartIdx);
                    _facePartList.Add("foreHead");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vWholeFace")
                {
                    _facePart.Add("wholeFace", currFacePartIdx);
                    _facePartList.Add("wholeFace");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vLeftEyebrow")
                {
                    _facePart.Add("leftEyebrow", currFacePartIdx);
                    _facePartList.Add("leftEyebrow");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vRightEyebrow")
                {
                    _facePart.Add("rightEyebrow", currFacePartIdx);
                    _facePartList.Add("rightEyebrow");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vLeftEye")
                {
                    _facePart.Add("leftEye", currFacePartIdx);
                    _facePartList.Add("leftEye");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vRightEye")
                {
                    _facePart.Add("rightEye", currFacePartIdx);
                    _facePartList.Add("rightEye");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vEyeCornerPoints")
                {
                    _facePart.Add("eyeCornerPoints", currFacePartIdx);
                    _facePartList.Add("eyeCornerPoints");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vEyeLine")
                {
                    _facePart.Add("eyeLine", currFacePartIdx);
                    _facePartList.Add("eyeLine");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vNose")
                {
                    _facePart.Add("nose", currFacePartIdx);
                    _facePartList.Add("nose");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vNostril")
                {
                    _facePart.Add("nostril", currFacePartIdx);
                    _facePartList.Add("nostril");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vNoseTip")
                {
                    _facePart.Add("noseTip", currFacePartIdx);
                    _facePartList.Add("noseTip");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vLipOuterLine")
                {
                    _facePart.Add("lipOuterLine", currFacePartIdx);
                    _facePartList.Add("lipOuterLine");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vLipInnerLine")
                {
                    _facePart.Add("lipInnerLine", currFacePartIdx);
                    _facePartList.Add("lipInnerLine");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vMouthCornerPoints")
                {
                    _facePart.Add("mouthCornerPoints", currFacePartIdx);
                    _facePartList.Add("mouthCornerPoints");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vJaw")
                {
                    _facePart.Add("jaw", currFacePartIdx);
                    _facePartList.Add("jaw");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vLipUpperOuterLine")
                {
                    _facePart.Add("lipUpperOuterLine", currFacePartIdx);
                    _facePartList.Add("lipUpperOuterLine");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vLipUpperInnerLine")
                {
                    _facePart.Add("lipUpperInnerLine", currFacePartIdx);
                    _facePartList.Add("lipUpperInnerLine");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vLipLowerInnerLine")
                {
                    _facePart.Add("lipLowerInnerLine", currFacePartIdx);
                    _facePartList.Add("lipLowerInnerLine");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vLipLowerOuterLine")
                {
                    _facePart.Add("lipLowerOuterLine", currFacePartIdx);
                    _facePartList.Add("lipLowerOuterLine");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vLeftCheek")
                {
                    _facePart.Add("leftCheek", currFacePartIdx);
                    _facePartList.Add("leftCheek");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vRightCheek")
                {
                    _facePart.Add("rightCheek", currFacePartIdx);
                    _facePartList.Add("rightCheek");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vPitchAxisLinePoints")
                {
                    _facePart.Add("pitchAxisLinePoints", currFacePartIdx);
                    _facePartList.Add("pitchAxisLinePoints");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vMidlinePoints")
                {
                    _facePart.Add("midLinePoints", currFacePartIdx);
                    _facePartList.Add("midLinePoints");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
                else if (line_contents[0] == "m_vNoseAlae")
                {
                    _facePart.Add("noseAlae", currFacePartIdx);
                    _facePartList.Add("noseAlae");
                    currFacePartIdx++;
                    _vertexIdx.Add(vertexIndex);
                }
            }
        }

        List<int> vertexIndex_Itpl = new List<int>();
        vertexIndex_Itpl.Add(1667); vertexIndex_Itpl.Add(1719);
        _facePart.Add("lipOuterLine_1st_Itpl", currFacePartIdx);
        _facePartList.Add("lipOuterLine_1st_Itpl");
        currFacePartIdx++;
        _vertexIdx.Add(vertexIndex_Itpl);
        
        vertexIndex_Itpl = new List<int>();
        vertexIndex_Itpl.Add(341); vertexIndex_Itpl.Add(397);
        _facePart.Add("lipOuterLine_5th_Itpl", currFacePartIdx);
        _facePartList.Add("lipOuterLine_5th_Itpl");
        currFacePartIdx++;
        _vertexIdx.Add(vertexIndex_Itpl);
    }



    public void LoadFaceContourLine()
    {
        //Read Vertex Syntax

        _contourIdxLine = new List<List<int>>();
        GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_contour_feature_z_normal_line = new List<List<float>>();
        using (StreamReader sr = new StreamReader("Assets/Resources/vertex_syntax/contour_line.txt"))
        {
            // Read the stream to a string, and write the string to the console.
            String line;
            while ((line = sr.ReadLine()) != null)
            {
                String[] line_contents = line.Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                              
                List<int> vertexIndex = new List<int>();
                List<float> tempNoramlVal = new List<float>();
                for (int i = 0; i < line_contents.Length; i++)
                {
                    int idx = Int32.Parse(line_contents[i]);
                    vertexIndex.Add(idx);

                    tempNoramlVal.Add(0.0f);
                }
                _contourIdxLine.Add(vertexIndex);

                GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_contour_feature_z_normal_line.Add(tempNoramlVal);
            }
            //Console.WriteLine(line);
            //for (int i = 0; i < line.Length; i++)
            //{
            //    int vertex = Convert.ToInt32(line[i]);
            //    left_eye_vertex.Add(vertex);
            //}
        }

        List<int> jawIdx = new List<int>(_contourIdxLine.Count);
        for(int i = 0; i < _contourIdxLine.Count; i++)
        {
            jawIdx.Add(_contourIdxLine[i][0]);
        }
        _facePartList.Add("jaw");
        _facePart.Add("jaw", _facePart.Count);
        _vertexIdx.Add(jawIdx);
    }

    public void LoadEyeShapeVertexFile()
    {
        using (StreamReader sr = new StreamReader("Assets/Resources/vertex_syntax/left_eye_shape_vertex.txt"))
        {
            // Read the stream to a string, and write the string to the console.
            String line;
            List<int> vertexIndex = new List<int>();

            while ((line = sr.ReadLine()) != null)
            {
                String[] line_contents = line.Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < line_contents.Length; i++)
                {
                    int idx = Int32.Parse(line_contents[i]);
                    vertexIndex.Add(idx);
                }

            }
            _vertexIdx.Add(vertexIndex);
            _facePart.Add("leftEyeShape", _facePart.Count);
            _facePartList.Add("leftEyeShape");
        }

        using (StreamReader sr = new StreamReader("Assets/Resources/vertex_syntax/right_eye_shape_vertex.txt"))
        {
            // Read the stream to a string, and write the string to the console.
            String line;
            List<int> vertexIndex = new List<int>();

            while ((line = sr.ReadLine()) != null)
            {
                String[] line_contents = line.Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < line_contents.Length; i++)
                {
                    int idx = Int32.Parse(line_contents[i]);
                    vertexIndex.Add(idx);
                }

            }
            _vertexIdx.Add(vertexIndex);
            _facePart.Add("rightEyeShape", _facePart.Count);
            _facePartList.Add("rightEyeShape");
        }

    }

    public void AdjustFaceSyntaxToUnityVertexSystem()
    {
        for (int i = 0; i < _vertexIdx.Count; i++)
        {
            for (int j = 0; j < _vertexIdx[i].Count; j++)
            {
                _vertexIdx[i][j] = (int)VertexSystemManager.GetInstance.m_vertex_systems["face"].m_v_to_vt[_vertexIdx[i][j]].y;
            }
        }
        for (int i = 0; i < _contourIdxLine.Count; i++)
        {
            for (int j = 0; j < _contourIdxLine[i].Count; j++)
            {
                _contourIdxLine[i][j] = (int)VertexSystemManager.GetInstance.m_vertex_systems["face"].m_v_to_vt[_contourIdxLine[i][j]].y;
            }
        }
    }
}
