﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PathManager : Singleton<PathManager>
{
    // Use this for initialization

    public string _ModelDatabaseFolderPath;
    public string _SaveDatabaseFolderPath;
    public string _DeviceUniqueName;
    public int _StartImageIndex;

    [NonSerialized] public string _FaceDatabaseFolderPath;
    [NonSerialized] public string _EyeTextureModelFolderPath;
   
    void Awake()
    {
        Init();
    }

    void Init()
    {
        //_DeviceUniqueName = "01";
        //_DatabaseFolderPath = "E:/Research/Databases";
        _FaceDatabaseFolderPath = _ModelDatabaseFolderPath + "/face/3D/illuni/BaseFaceModel";
        _EyeTextureModelFolderPath = _FaceDatabaseFolderPath + "/Shape/Eye/model";
    }

    // Use this for initialization
    void Start () {
        //Init();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
