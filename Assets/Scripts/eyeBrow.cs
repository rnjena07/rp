﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class EyeBrow : MonoBehaviour {

    Renderer m_renderer;
    List<string> m_file_paths;
    List<List<Texture2D>> m_diffuse_texs;

    int m_image_per_class;
    string m_folder_path;

    public bool m_rand_or_not;
    //public int m_curr_class;
    public int m_curr_class_of_eyebrow;
    public int m_curr_file_of_eyebrow_class;
    public int m_nb_of_eyebrow;
    public int m_nb_of_eyebrow_class;
    public int m_nb_of_eyebrow_file_per_class;

    void Init()
    {
        m_rand_or_not = true;

        m_image_per_class = 37;

        m_renderer = new Renderer();
        m_renderer = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<Renderer>();

        m_file_paths = new List<string>();
        m_diffuse_texs = new List<List<Texture2D>>();

        m_folder_path = PathManager.GetInstance._FaceDatabaseFolderPath + "/Shape/Eyebrows";

        m_nb_of_eyebrow_class = 38;
        m_nb_of_eyebrow_file_per_class = 3;

        m_file_paths = System.IO.Directory.GetFiles(m_folder_path).OfType<string>().ToList();
        m_nb_of_eyebrow = m_file_paths.Count;

        for (int i = 0; i < m_nb_of_eyebrow_class; i++)
        {
            List<Texture2D> diffuse_tex = new List<Texture2D>();
            for (int j = 0; j < m_nb_of_eyebrow_file_per_class; j++)
            {
                diffuse_tex.Add(new Texture2D(1, 1));
            }
            m_diffuse_texs.Add(diffuse_tex);
        }
        for (int i = 0; i < m_nb_of_eyebrow_class; i++)
        {
            for (int j = 0; j < m_nb_of_eyebrow_file_per_class; j++)
            {
                string file_path = m_folder_path + "/" + i.ToString() + "_" + j.ToString() + ".png";
                if (System.IO.File.Exists(file_path))
                {
                    // Image file exists - load bytes into texture
                    var diffuseBytes = System.IO.File.ReadAllBytes(file_path);
                    Texture2D diffuse_tex = new Texture2D(1, 1);

                    diffuse_tex.LoadImage(diffuseBytes);
                    m_diffuse_texs[i][j] = diffuse_tex;
                }
            }
        }
    }

    public void SetEyeBrowTexture(int class_num, int file_num)
    {        
        m_renderer.material.SetTexture("Texture2D_3888C0E4", m_diffuse_texs[class_num][file_num]);
    }

    public void RandomEyeBrowIndex()
    {
        m_curr_class_of_eyebrow = UnityEngine.Random.Range(0, m_nb_of_eyebrow_class);
        m_curr_file_of_eyebrow_class = UnityEngine.Random.Range(0, m_diffuse_texs[m_curr_class_of_eyebrow].Count);
    }

    public void SetEyeBrowTexture()
    {
        float curr_u_num = UnityEngine.Random.Range(0.1f, 0.35f);
        float curr_v_num = UnityEngine.Random.Range(0.0f, 0.2f);
        float curr_scale_x_num = UnityEngine.Random.Range(9f, 12f);
        float curr_scale_y_num = UnityEngine.Random.Range(8f, 13f);
        float uv_rotate = UnityEngine.Random.Range(-0.1f, 0.1f);

        Vector4 uv_offset = new Vector4(curr_u_num, curr_v_num, 0.1f, -0.05f);
        Vector2 map_size = new Vector2(curr_scale_x_num, curr_scale_y_num);

        // diffuse map
        m_renderer.material.SetTexture("Texture2D_3888C0E4", m_diffuse_texs[m_curr_class_of_eyebrow][m_curr_file_of_eyebrow_class]);
        // uv offset
        m_renderer.material.SetVector("Vector4_8D876ACF", uv_offset);
        // map Size
        //m_renderer.material.SetVector("Vector2_A0B11460", map_size);
        // uv_rotate
        m_renderer.material.SetFloat("Vector1_5B637A76", uv_rotate);

        //UnityEngine.Debug.Log("Eye Class Is : " + m_curr_class_of_eyebrow);
        //UnityEngine.Debug.Log("Eye image Is : " + m_curr_file_of_eyebrow_class);
    }
    
    public void SetEyeBrowTextureFromClass(int class_num, int curr_file_of_eyebrow_class)
    {
        m_curr_class_of_eyebrow = class_num;

        if(curr_file_of_eyebrow_class == -1)
        {
            m_curr_file_of_eyebrow_class = UnityEngine.Random.Range(0, m_diffuse_texs[m_curr_class_of_eyebrow].Count);
        }
        else
        {
            m_curr_file_of_eyebrow_class = curr_file_of_eyebrow_class;
        }

        if(m_curr_file_of_eyebrow_class >= m_diffuse_texs[m_curr_class_of_eyebrow].Count)
        {
            m_curr_file_of_eyebrow_class = m_diffuse_texs[m_curr_class_of_eyebrow].Count - 1;
        }

        if (m_curr_file_of_eyebrow_class < 0)
        {
            m_curr_file_of_eyebrow_class = 0;
        }

        float curr_u_num = UnityEngine.Random.Range(0.1f, 0.35f);
        float curr_v_num = UnityEngine.Random.Range(0.0f, 0.2f);
        float curr_scale_x_num = UnityEngine.Random.Range(9f, 12f);
        float curr_scale_y_num = UnityEngine.Random.Range(8f, 13f);
        float uv_rotate = UnityEngine.Random.Range(-0.1f, 0.1f);

        Vector4 uv_offset = new Vector4(curr_u_num, curr_v_num, 0.1f, -0.05f);
        Vector2 map_size = new Vector2(curr_scale_x_num, curr_scale_y_num);

        // diffuse map
        m_renderer.material.SetTexture("Texture2D_3888C0E4", m_diffuse_texs[m_curr_class_of_eyebrow][m_curr_file_of_eyebrow_class]);
        // uv offset
        m_renderer.material.SetVector("Vector4_8D876ACF", uv_offset);
        // map Size
        //m_renderer.material.SetVector("Vector2_A0B11460", map_size);
        // uv_rotate
        m_renderer.material.SetFloat("Vector1_5B637A76", uv_rotate);

        //UnityEngine.Debug.Log("Eye Class Is : " + m_curr_class_of_eyebrow);
        //UnityEngine.Debug.Log("Eye image Is : " + m_curr_file_of_eyebrow_class);
    }


    public void EyeBrowTextureTurnOff()
    {
        m_renderer.material.SetInt("Boolean_CD0205E2", 0);
    }


    public void EyeBrowTextureTurnOn()
    {
        m_renderer.material.SetInt("Boolean_CD0205E2", 1);
    }

    // Use this for initialization
    void Start () {
        Init();
    }
         	
	// Update is called once per frame
	void Update () {
		
	}
}
