﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class BackgroundManager : Singleton<BackgroundManager> {

    GameObject target;
    Texture2D texture;
    string m_background_file_folder_path;
    string m_file_name_list_file_path;
    string[] m_background_file_names;
    string[] m_background_file_paths;
    Texture2D[] m_background_texture;

    Material m_front_plane_mtl;

    //public GameObject target;
    Texture2D m_diffuse_tex;
    Renderer m_renderer;     

    public void Init()
    {
        target = GameObject.FindWithTag("background");
        m_renderer = target.GetComponent<Renderer>();

        m_diffuse_tex = new Texture2D(1, 1);
        m_background_texture = new Texture2D[1];
       
        // Debug.Log("Debug Here : " + GameManager.GetInstance._DatabaseFolderPath);
        // m_background_file_folder_path = GameManager.GetInstance._DatabaseFolderPath + "/image/SUN2012pascalformat/JPEGImages";
        // m_file_name_list_file_path = GameManager.GetInstance._DatabaseFolderPath + "/image/SUN2012pascalformat/ImageSets/Main/train.txt";

        m_background_file_folder_path = PathManager.GetInstance._ModelDatabaseFolderPath + "/Image/Background/SUN2012pascalformat/JPEGImages";
        m_file_name_list_file_path = PathManager.GetInstance._ModelDatabaseFolderPath + "/Image/Background/SUN2012pascalformat/ImageSets/Main/train.txt";

        if (File.Exists(m_file_name_list_file_path))
        {
            m_background_file_names = File.ReadAllLines(m_file_name_list_file_path);
        }

        int nb_of_trg_texture = m_background_file_names.Length;

        nb_of_trg_texture = 10;

        m_background_file_paths = new string[nb_of_trg_texture];
        for (int i = 0; i < nb_of_trg_texture; i++)
        {
            m_background_file_paths[i] = m_background_file_folder_path + "/" + m_background_file_names[i] + ".jpg";
        }
        
        if (GameManager.GetInstance._PreLoadBackground == true)
        {
            ReadAllImage();
            SetRandomTexture();
        }
        else
        {
            ReadRandomImgSetTexture();
        }

        //if (System.IO.File.Exists(m_background_file_paths[0]))
        //{
        //    // Image file exists - load bytes into texture
        //    var bytes = System.IO.File.ReadAllBytes(m_background_file_paths[0]);
        //    var tex = new Texture2D(1, 1);
        //    tex.LoadImage(bytes);
        //    m_front_plane_mtl.mainTexture = tex;

        //    plane.transform.localScale = new Vector3(tex.width/20, tex.height/20, 1);
        //    //plane.transform.localScale = new Vector3(5, 5, 5);
        //    // Apply to Plane
        //    MeshRenderer mr = plane.GetComponent<MeshRenderer>();
        //    mr.material = m_front_plane_mtl;
        //}
        // plane.transform.localScale.Scale(100, 100, 100);
    }


    void Start()
    { 
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ReadRandomImgSetTexture()
    {
        int trgImgIdx = Random.Range(0, m_background_file_paths.Length);
        if (System.IO.File.Exists(m_background_file_paths[trgImgIdx]))
        {
            // Image file exists - load bytes into texture
            var bytes = System.IO.File.ReadAllBytes(m_background_file_paths[trgImgIdx]);

            m_diffuse_tex.LoadImage(bytes);

            m_renderer.material.SetTexture("_BaseColorMap", m_diffuse_tex);

            float posX = UnityEngine.Random.Range(-20.0f, 20.0f);
            float posY = UnityEngine.Random.Range(-20.0f, 20.0f);
            float posZ = -30.0f;
            float rotZ = UnityEngine.Random.Range(-10.0f, 10.0f);
            
            target.transform.position = new Vector3(posX, posY, posZ);
            target.transform.localScale = new Vector3(20, 1, 20);
            target.transform.localRotation = Quaternion.Euler(90, 0, rotZ);
        }
    }

    public void ReadAllImage()
    {
        m_background_texture = new Texture2D[m_background_file_paths.Length];
       
        for (int i = 0; i < m_background_file_paths.Length; i++)
        {
            if (System.IO.File.Exists(m_background_file_paths[i]))
            {
                // Image file exists - load bytes into texture
                var bytes = System.IO.File.ReadAllBytes(m_background_file_paths[i]);

                Texture2D tex = new Texture2D(1, 1);
                tex.LoadImage(bytes);

                m_background_texture[i] = tex;
            }
            else
            {
                UnityEngine.Debug.Log("<color=red>Image Loading ERROR!!! -> </color>" + m_background_file_paths[i]);
            }
        }
    }

    public void SetRandomTexture()
    {
        int trg_texture_idx = Random.Range(0, m_background_texture.Length);

        m_renderer.material.SetTexture("_BaseColorMap", m_background_texture[trg_texture_idx]);

        float posX = UnityEngine.Random.Range(-20.0f, 20.0f);
        float posY = UnityEngine.Random.Range(-20.0f, 20.0f);
        float posZ = -30.0f;
        float rotZ = UnityEngine.Random.Range(-10.0f, 10.0f);

        target.transform.position = new Vector3(posX, posY, posZ);
        target.transform.localScale = new Vector3(20, 1, 20);
        target.transform.localRotation = Quaternion.Euler(90, 0, rotZ);
    }
}
