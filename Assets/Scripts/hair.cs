using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

using UnityEditor;
using UnityEngine.UI;

public class Hair : Singleton<Hair>
{

    public GameObject target;
    public Mesh m_mesh;    

    public List<string> m_hair_model_names;
    List<string> m_hair_obj_model_names;
    List<string> m_hair_texture_model_names;
    List<string> m_hair_diffuse_map_file_paths;
    List<string> m_hair_normal_map_file_paths;
    List<string> m_hair_mask_map_file_paths;
    List<string> m_hair_specular_map_file_path;

    List<string> m_skull_cap_diffuse_map_file_paths;
    List<string> m_skull_cap_normal_map_file_paths;
    List<string> m_skull_cap_specular_map_file_paths;
    
    public List<ObjImporter> m_hair_obj_list;

    Renderer m_hair_Renderer;
    Renderer m_skull_cap_Renderer;

    Texture2D m_diffuse_tex;
    Texture2D m_normal_tex;
    Texture2D m_mask_tex;

    List<Texture2D> m_diffuse_tex_list;
    List<Texture2D> m_normal_tex_list;
    List<Texture2D> m_mask_tex_list;
    List<Texture2D> m_specular_tex_list;
    
    List<Texture2D> m_diffuse_tex_skullcap_list;
    List<Texture2D> m_normal_tex_skullcap_list;
    List<Texture2D> m_specular_tex_skullcap_list;
    
    string m_hair_data_folder_path;

    float m_alpha_cut_off;
    float m_alpha_cut_off_prepass;
    float m_alpha_cut_off_postpass;

    float m_normal_val;
    float m_smoothness_min;
    float m_smoothness_max;
    float m_ambient_occlusion_min;
    float m_ambient_occlusion_max;
    float m_coat_mask;

    float m_ior;

    public bool m_hair_show_or_not;

    Vector3 m_hair_default_position;
    Vector3 m_previous_hair_default_position;

    public Vector3[] m_original_prev_hair_vertex;
    public Vector3[] m_original_prev_haircap_vertex;

    [NonSerialized] public float _hair_h;
    [NonSerialized] public float _hair_s;
    [NonSerialized] public float _hair_v;
    [NonSerialized] public int _hair_curr_idx;
    [NonSerialized] public int _hair_prev_idx;

    [NonSerialized] public Dictionary<string, int> m_hair_class_map = new Dictionary<string, int>();

    // Use this for initialization
    void Start() {
        m_mesh = new Mesh();
        m_diffuse_tex = new Texture2D(1, 1);
        m_normal_tex = new Texture2D(1, 1);
        m_mask_tex = new Texture2D(1, 1);

        m_diffuse_tex_list = new List<Texture2D>();
        m_normal_tex_list = new List<Texture2D>();
        m_mask_tex_list = new List<Texture2D>();
        m_specular_tex_list = new List<Texture2D>();

        m_diffuse_tex_skullcap_list = new List<Texture2D>();
        m_normal_tex_skullcap_list = new List<Texture2D>();
        m_specular_tex_skullcap_list = new List<Texture2D>();

        m_hair_diffuse_map_file_paths = new List<string>();
        m_hair_normal_map_file_paths = new List<string>();
        m_hair_mask_map_file_paths = new List<string>();
        m_hair_specular_map_file_path = new List<string>();

        m_skull_cap_diffuse_map_file_paths = new List<string>();
        m_skull_cap_normal_map_file_paths = new List<string>();
        m_skull_cap_specular_map_file_paths = new List<string>();

        m_original_prev_hair_vertex = new Vector3[0];
        m_original_prev_haircap_vertex = new Vector3[0];

        //string tag = target.tag;

        //m_hair_Renderer = GameObject.Find("hair_obj").GetComponent<Renderer>();
        //m_skull_cap_Renderer = GameObject.Find("skullcap").GetComponent<Renderer>();

        ////Make sure to enable the Keywords
        //m_hair_Renderer.material.EnableKeyword("_BaseColorMap");
        //m_hair_Renderer.material.EnableKeyword("_NormalMap");
        //m_hair_Renderer.material.EnableKeyword("_MaskMap");

        //TODO: YJ
        m_hair_data_folder_path = Application.dataPath + "/Resources/hair";
        LoadHairModels();
        InitHairClassMap();
        _hair_curr_idx = 0;
        _hair_prev_idx = 0;

        m_hair_show_or_not = true;

        m_hair_default_position = new Vector3(0, 0, -300);
        m_previous_hair_default_position = new Vector3(0, 0, 0);

        //GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().SetHairBaseFace();
        GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().HairFFDRegistration();

        string hair_file_name = m_hair_model_names[0];
        string hair_file = hair_file_name.Substring(hair_file_name.Length - 3, 3);

        if (m_hair_class_map.ContainsKey("090"))
        {
            UnityEngine.Debug.Log("No Exist!!!");
        }
        UnityEngine.Debug.Log("Here!!!");
    }

    void LoadHairModels()
    {
        //////////////////////////////////////////
        //Load Hair!!!
        m_hair_obj_list = new List<ObjImporter>();

        m_hair_model_names = new List<string>();
        m_hair_obj_model_names = new List<string>();
        m_hair_texture_model_names = new List<string>();
        
        for(int i = 0; i < GameObject.FindWithTag("hair").transform.childCount; i++)
        {
            string hair_name = GameObject.FindWithTag("hair").transform.GetChild(i).name;
            m_hair_model_names.Add(hair_name);
        }

        string[] hair_folder_directories = Directory.GetDirectories(m_hair_data_folder_path);
        
        for (int i = 0; i < m_hair_model_names.Count; i++)
        {
            GameObject hair_object = GameObject.Find(m_hair_model_names[i]).transform.GetChild(0).gameObject;

            hair_object.transform.localPosition = m_hair_default_position;
                       
            if (hair_object.transform.childCount == 0)
            {
                hair_object.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            }
            else
            {
                for(int j = 0; j < hair_object.transform.childCount; j++)
                {
                    hair_object.transform.GetChild(j).GetComponent<MeshRenderer>().enabled = false;
                }
            }
        }

        //OBJ 파일 읽어오는 버젼
        ////      for (int i = 0; i < m_hair_model_names.Count; i++)
        //        for (int i = 0; i < 5; i++)
        //        {
        //            var timeElapsed = Time.realtimeSinceStartup;

        //            m_hair_obj_model_names.Add(m_hair_data_folder_path + "/" + m_hair_model_names[i] + "/hair.obj");
        //            //m_hair_texture_model_names.Add(m_hair_data_folder_path + "/" + m_hair_model_names[i] + "/hair.mtl");

        //            ObjImporter obj_hair = new ObjImporter();
        //            obj_hair.ImportOBJFile(m_hair_obj_model_names[i]);

        //            //obj_hair.ImportMTLFile(m_hair_texture_model_names[i]);

        //            m_hair_obj_list.Add(obj_hair);

        //            print("Hair Loading Timetime : " + (Time.realtimeSinceStartup - timeElapsed).ToString("f6"));
        //        }
    }

    void InitHairClassMap()
    {
        m_hair_class_map.Add("009", 0);
        m_hair_class_map.Add("078", 0);
        m_hair_class_map.Add("079", 0);

        m_hair_class_map.Add("080", 1);
        m_hair_class_map.Add("081", 1);
        m_hair_class_map.Add("082", 1);

        m_hair_class_map.Add("083", 2);
        m_hair_class_map.Add("084", 2);
        m_hair_class_map.Add("085", 2);

        m_hair_class_map.Add("007", 3);
        m_hair_class_map.Add("013", 3);
        m_hair_class_map.Add("077", 3);

        m_hair_class_map.Add("000", 4);
        m_hair_class_map.Add("004", 4);
        m_hair_class_map.Add("025", 4);
        m_hair_class_map.Add("033", 4);
        m_hair_class_map.Add("052", 4);

        m_hair_class_map.Add("022", 5);
        m_hair_class_map.Add("037", 5);
        m_hair_class_map.Add("038", 5);
        m_hair_class_map.Add("063", 5);

        m_hair_class_map.Add("023", 6);
        m_hair_class_map.Add("065", 6);
        m_hair_class_map.Add("066", 6);

        m_hair_class_map.Add("024", 7);
        m_hair_class_map.Add("026", 7);
        m_hair_class_map.Add("041", 7);

        m_hair_class_map.Add("039", 8);
        m_hair_class_map.Add("064", 8);
        m_hair_class_map.Add("067", 8);

        m_hair_class_map.Add("011", 9);
        m_hair_class_map.Add("012", 9);
        m_hair_class_map.Add("069", 9);

        m_hair_class_map.Add("005", 10);
        m_hair_class_map.Add("006", 10);
        m_hair_class_map.Add("035", 10);
        m_hair_class_map.Add("040", 10);
        m_hair_class_map.Add("043", 10);
        m_hair_class_map.Add("050", 10);
        m_hair_class_map.Add("051", 10);

        m_hair_class_map.Add("001", 11);
        m_hair_class_map.Add("002", 11);

        m_hair_class_map.Add("032", 12);
        m_hair_class_map.Add("034", 12);
        m_hair_class_map.Add("072", 12);

        m_hair_class_map.Add("015", 13);
        m_hair_class_map.Add("036", 13);
        m_hair_class_map.Add("074", 13);

        m_hair_class_map.Add("020", 14);
        m_hair_class_map.Add("030", 14);
        m_hair_class_map.Add("068", 14);

        m_hair_class_map.Add("016", 15);
        m_hair_class_map.Add("075", 15);
        m_hair_class_map.Add("076", 15);

        m_hair_class_map.Add("014", 16);
        m_hair_class_map.Add("053", 16);
        m_hair_class_map.Add("060", 16);
        m_hair_class_map.Add("056", 16);
        m_hair_class_map.Add("057", 16);
        
        m_hair_class_map.Add("019", 17);
        m_hair_class_map.Add("028", 17);
        m_hair_class_map.Add("029", 17);
        m_hair_class_map.Add("049", 17);
        
        m_hair_class_map.Add("008", 18);
        m_hair_class_map.Add("048", 18);
        m_hair_class_map.Add("058", 18);
        m_hair_class_map.Add("059", 18);
        m_hair_class_map.Add("062", 18);

        m_hair_class_map.Add("021", 19);
        m_hair_class_map.Add("027", 19);
        m_hair_class_map.Add("031", 19);

        m_hair_class_map.Add("042", 20);
        m_hair_class_map.Add("044", 20);
        m_hair_class_map.Add("061", 20);

        m_hair_class_map.Add("017", 21);
        m_hair_class_map.Add("045", 21);
        m_hair_class_map.Add("046", 21);
        m_hair_class_map.Add("047", 21);
        m_hair_class_map.Add("055", 21);

        m_hair_class_map.Add("003", 22);
        m_hair_class_map.Add("010", 22);
        m_hair_class_map.Add("018", 22);
        m_hair_class_map.Add("054", 22);

        m_hair_class_map.Add("070", 23);
        m_hair_class_map.Add("071", 23);
        m_hair_class_map.Add("073", 23);
    }

    void DeformCurrentHair()
    {
        m_mesh = m_mesh;
    }

    public void FitToFace()
    {
        m_mesh = m_mesh;
    }

    public void SetRandomHair()
    {
        _hair_curr_idx = UnityEngine.Random.Range(0, m_hair_model_names.Count);
        
        //GameObject.Find(m_hair_model_names[GameManager.GetInstance._hair_prev_idx]).transform.GetChild(0).transform.localPosition = m_hair_default_position;
        GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.localPosition = m_hair_default_position;
        if (GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.childCount == 0)
        {
            GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            if(m_original_prev_hair_vertex.Length != 0)
            {
                GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(0).GetComponent<MeshFilter>().mesh.vertices = m_original_prev_hair_vertex;
            }
        }
        else
        {
            for (int i = 0; i < GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.childCount; i++)
            {
                GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;

                if (GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(i).name == "skullcap")
                {
                    if (m_original_prev_haircap_vertex.Length != 0) // 다시 원래 vertex 상태로...
                    {
                        GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshFilter>().mesh.vertices = m_original_prev_haircap_vertex;
                    }
                }
                else
                {
                    if (m_original_prev_hair_vertex.Length != 0)
                    {
                        GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshFilter>().mesh.vertices = m_original_prev_hair_vertex;
                    }
                }        
            }
        }

        GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        if (GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.childCount == 0)
        {
            GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            for (int i = 0; i < GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.childCount; i++)
            {
                GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshRenderer>().enabled = true;
            }
        }

        _hair_prev_idx = _hair_curr_idx;
        //Texture and Material
        SetTexture();
    }

    public void SetHair(int idx)
    {
        _hair_curr_idx = idx;

        //GameObject.Find(m_hair_model_names[GameManager.GetInstance._hair_prev_idx]).transform.GetChild(0).transform.localPosition = m_hair_default_position;
        GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.localPosition = m_hair_default_position;
        if (GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.childCount == 0)
        {
            GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            if (m_original_prev_hair_vertex.Length != 0)
            {
                GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(0).GetComponent<MeshFilter>().mesh.vertices = m_original_prev_hair_vertex;
            }
        }
        else
        {
            for (int i = 0; i < GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.childCount; i++)
            {
                GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshRenderer>().enabled = false;

                if (GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(i).name == "skullcap")
                {
                    if (m_original_prev_haircap_vertex.Length != 0) // 다시 원래 vertex 상태로...
                    {
                        GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshFilter>().mesh.vertices = m_original_prev_haircap_vertex;
                    }
                }
                else
                {
                    if (m_original_prev_hair_vertex.Length != 0)
                    {
                        GameObject.Find(m_hair_model_names[_hair_prev_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshFilter>().mesh.vertices = m_original_prev_hair_vertex;
                    }
                }
            }
        }

        GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        if (GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.childCount == 0)
        {
            GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            for (int i = 0; i < GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.childCount; i++)
            {
                GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshRenderer>().enabled = true;
            }
        }

        _hair_prev_idx = _hair_curr_idx;
        //Texture and Material
        SetTexture();
    }



    //OBJ 파일 읽어와서 setting하는 버젼
    //public void SetRandomHair()
    //{
    //    GameManager.GetInstance._hair_curr_idx = UnityEngine.Random.Range(0, m_hair_obj_list.Count - 1);

    //    if (m_hair_obj_list[GameManager.GetInstance._hair_curr_idx].m_mesh_grp.Count == 1)
    //    {
    //        m_mesh = m_hair_obj_list[GameManager.GetInstance._hair_curr_idx].m_mesh_grp[0];
    //        GameObject.Find("hair_obj").GetComponent<MeshFilter>().mesh = m_mesh;
    //        //GameObject.Find("skullcap").transform.localPosition = new Vector3(0, 0, -100);
    //        GameObject.Find("skullcap").GetComponent<MeshRenderer>().enabled = false;
    //    }
    //    else
    //    {
    //        //GameObject.Find("skullcap").transform.localPosition = new Vector3(0, 0, 0);
    //        GameObject.Find("skullcap").GetComponent<MeshRenderer>().enabled = true;
    //        for (int i = 0; i < m_hair_obj_list[GameManager.GetInstance._hair_curr_idx].m_mesh_grp.Count; i++)
    //        {
    //            string name = m_hair_obj_list[GameManager.GetInstance._hair_curr_idx].m_groupNames[i];
    //            if (name.Contains("cap") == true)
    //            {
    //                m_mesh = m_hair_obj_list[GameManager.GetInstance._hair_curr_idx].m_mesh_grp[i];
    //                GameObject.Find("skullcap").GetComponent<MeshFilter>().mesh = m_mesh;
    //            }
    //            else
    //            {
    //                m_mesh = m_hair_obj_list[GameManager.GetInstance._hair_curr_idx].m_mesh_grp[i];
    //                GameObject.Find("hair_obj").GetComponent<MeshFilter>().mesh = m_mesh;
    //            }
    //        }
    //    }

    //    //Texture and Material
    //    SetTexture();
    //}

    public void SetTexture()
    {
        bool hair_cap_exist_or_not = false;
        if (GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).childCount == 0)
        {
            m_hair_Renderer = GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.GetChild(0).GetComponent<Renderer>();
            hair_cap_exist_or_not = false;
        }
        else
        {
            //Find Skull Cap Idx
            for (int i = 0; i < GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.childCount; i++)
            {
                if(GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.GetChild(i).name == "skullcap")
                {
                    m_skull_cap_Renderer = GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<Renderer>();
                    hair_cap_exist_or_not = true;
                }
                else
                {
                    m_hair_Renderer = GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<Renderer>();
                }
            }
        }    
       
        _hair_h = UnityEngine.Random.Range(0.0f, 1.0f);
        _hair_s = UnityEngine.Random.Range(0.0f, 0.7f);
        _hair_v = UnityEngine.Random.Range(0.0f, 1.0f);

        Color rgbHairCol = Color.HSVToRGB(_hair_h, _hair_s, _hair_v);
        m_hair_Renderer.material.SetColor("_BaseColor", rgbHairCol);
        
        if(hair_cap_exist_or_not == true)
        {
            m_skull_cap_Renderer.material.SetColor("_BaseColor", rgbHairCol);
        }

        //m_alpha_cut_off = UnityEngine.Random.Range(0.5f, 0.8f);
        //m_alpha_cut_off_prepass = UnityEngine.Random.Range(0.5f, 0.8f);
        //m_alpha_cut_off_postpass = UnityEngine.Random.Range(0.5f, 0.8f);

        //m_hair_Renderer.material.SetFloat("m_alpha_cut_offEnable",1.0f);
        //m_hair_Renderer.material.SetFloat("m_alpha_cut_off", m_alpha_cut_off);
        //m_hair_Renderer.material.SetFloat("m_alpha_cut_off_prepass", m_alpha_cut_off_prepass);
        //m_hair_Renderer.material.SetFloat("m_alpha_cut_off_postpass", m_alpha_cut_off_postpass);

        //m_hair_Renderer.material.SetFloat("_DoubleSidedEnable ",1.0f);

        //bool _smoothenessTestPass = false;

        //while (_smoothenessTestPass == false)
        //{
        //    m_smoothness_min = UnityEngine.Random.Range(0.0f, 0.8f);
        //    m_smoothness_max = UnityEngine.Random.Range(0.0f, 0.8f);
        //    if (m_smoothness_min < m_smoothness_max) {
        //        if (m_smoothness_max - m_smoothness_min >= 0.5)
        //        {
        //            m_hair_Renderer.material.SetFloat("_SmoothnessRemapMin", m_smoothness_min);
        //            m_hair_Renderer.material.SetFloat("_SmoothnessRemapMax", m_smoothness_max);
        //            _smoothenessTestPass = true;
        //        }
        //    }
        //}

        //bool _ambientOcclusionTestPass = false;

        //while (_ambientOcclusionTestPass == false)
        //{
        //    m_ambient_occlusion_min = UnityEngine.Random.Range(0.0f, 1.0f);
        //    m_ambient_occlusion_max = UnityEngine.Random.Range(0.0f, 1.0f);
        //    if (m_ambient_occlusion_min < m_ambient_occlusion_max)
        //    {
        //        if (m_ambient_occlusion_max - m_ambient_occlusion_min >= 0.3)
        //        {
        //            m_hair_Renderer.material.SetFloat("_AORemapMin", m_ambient_occlusion_min);
        //            m_hair_Renderer.material.SetFloat("_AORemapMax", m_ambient_occlusion_max);
        //            _ambientOcclusionTestPass = true;
        //        }
        //    }
        //}

        m_normal_val = UnityEngine.Random.Range(0.8f, 2.0f);
        m_hair_Renderer.material.SetFloat("_NormalScale", m_normal_val);

        m_coat_mask = UnityEngine.Random.Range(0.0f, 0.02f);
        m_hair_Renderer.material.SetFloat("_CoatMask", m_coat_mask);

        m_ior = UnityEngine.Random.Range(0.0f,1.75f);
        m_hair_Renderer.material.SetFloat("_Ior ", m_ior);

        if (GameObject.Find(m_hair_model_names[_hair_curr_idx]).transform.GetChild(0).transform.childCount != 0)
        {
            //나중에 여기에다가 haircap 시뮬 넣자
            //m_skull_cap_Renderer.material.SetTexture("_BaseColorMap", m_diffuse_tex_skullcap_list[_hair_curr_idx]);
        }
    }

    //OBJ 파일 읽어와서 했던 버젼
    //public void SetTexture()
    //{

    //    //m_alpha_cut_off = UnityEngine.Random.Range(0.5f, 0.8f);
    //    //m_alpha_cut_off_prepass = UnityEngine.Random.Range(0.5f, 0.8f);
    //    //m_alpha_cut_off_postpass = UnityEngine.Random.Range(0.5f, 0.8f);

    //    //m_hair_Renderer.material.SetFloat("m_alpha_cut_offEnable",1.0f);
    //    //m_hair_Renderer.material.SetFloat("m_alpha_cut_off", m_alpha_cut_off);
    //    //m_hair_Renderer.material.SetFloat("m_alpha_cut_off_prepass", m_alpha_cut_off_prepass);
    //    //m_hair_Renderer.material.SetFloat("m_alpha_cut_off_postpass", m_alpha_cut_off_postpass);



    //    m_hair_Renderer.material.SetFloat("_DoubleSidedEnable ", 1.0f);
    //    m_hair_Renderer.material.SetTexture("_BaseColorMap", m_diffuse_tex_list[_hair_curr_idx]);

    //    float h = UnityEngine.Random.Range(0.0f, 1.0f);
    //    float s = UnityEngine.Random.Range(0.0f, 0.7f);
    //    float v = UnityEngine.Random.Range(0.0f, 1.0f);

    //    Color rgbHairCol = Color.HSVToRGB(h, s, v);
    //    m_hair_Renderer.material.SetColor("_BaseColor", rgbHairCol);

    //    m_hair_Renderer.material.SetTexture("_MaskMap", m_mask_tex_list[_hair_curr_idx]);

    //    bool _smoothenessTestPass = false;

    //    while (_smoothenessTestPass == false)
    //    {
    //        m_smoothness_min = UnityEngine.Random.Range(0.0f, 0.8f);
    //        m_smoothness_max = UnityEngine.Random.Range(0.0f, 0.8f);
    //        if (m_smoothness_min < m_smoothness_max)
    //        {
    //            if (m_smoothness_max - m_smoothness_min >= 0.5)
    //            {
    //                m_hair_Renderer.material.SetFloat("_SmoothnessRemapMin", m_smoothness_min);
    //                m_hair_Renderer.material.SetFloat("_SmoothnessRemapMax", m_smoothness_max);
    //                _smoothenessTestPass = true;
    //            }
    //        }
    //    }

    //    bool _ambientOcclusionTestPass = false;

    //    while (_ambientOcclusionTestPass == false)
    //    {
    //        m_ambient_occlusion_min = UnityEngine.Random.Range(0.0f, 1.0f);
    //        m_ambient_occlusion_max = UnityEngine.Random.Range(0.0f, 1.0f);
    //        if (m_ambient_occlusion_min < m_ambient_occlusion_max)
    //        {
    //            if (m_ambient_occlusion_max - m_ambient_occlusion_min >= 0.3)
    //            {
    //                m_hair_Renderer.material.SetFloat("_AORemapMin", m_ambient_occlusion_min);
    //                m_hair_Renderer.material.SetFloat("_AORemapMax", m_ambient_occlusion_max);
    //                _ambientOcclusionTestPass = true;
    //            }
    //        }
    //    }

    //    m_hair_Renderer.material.SetTexture("_NormalMap", m_normal_tex_list[_hair_curr_idx]);
    //    m_normal_val = UnityEngine.Random.Range(0.8f, 2.0f);
    //    m_hair_Renderer.material.SetFloat("_NormalScale", m_normal_val);

    //    m_coat_mask = UnityEngine.Random.Range(0.0f, 0.02f);
    //    m_hair_Renderer.material.SetFloat("_CoatMask", m_coat_mask);

    //    m_ior = UnityEngine.Random.Range(0.0f, 1.75f);
    //    m_hair_Renderer.material.SetFloat("_Ior ", m_ior);

    //    m_skull_cap_Renderer.material.SetTexture("_BaseColorMap", m_diffuse_tex_skullcap_list[_hair_curr_idx]);
    //}

    // Update is called once per frame
    void Update () {
        //if (GameManager.GetInstance._currDeltaTime > GameManager.GetInstance._deltaTime)
        //{
        //    if (tag == "left_eye")
        //    {
        //        target.transform.localRotation = Quaternion.Euler(GameManager.GetInstance.leftEyeRotAng.x, GameManager.GetInstance.leftEyeRotAng.y, GameManager.GetInstance.leftEyeRotAng.z);
        //    }
        //    else if (tag == "right_eye")
        //    {
        //        target.transform.localRotation = Quaternion.Euler(GameManager.GetInstance.rightEyeRotAng.x, GameManager.GetInstance.rightEyeRotAng.y, GameManager.GetInstance.rightEyeRotAng.z);
        //    }
        //    //translateEyeAnchorCenter();
        //}
    }
}
