﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class OpManager : Singleton<OpManager>
{
    [NonSerialized] public bool _opShowOrNot;
    [NonSerialized] public bool _opGlassShowOrNot;

    public List<string> _opTagNames;
    public List<string> _opGlassTagNames;
    List<int> _opGlassIndexes;

    private Renderer m_op_Renderer;
    public int m_op_curr_idx;
    public int _currGlassOpIdx;
    public int _currGlassClass;
    public int _currOpIdx;
    
    public List<string> m_glass_model_names;
    [NonSerialized] public Dictionary<string, int> m_glass_class_map = new Dictionary<string, int>();


    [NonSerialized] public Dictionary<string, int> m_glass_name_map = new Dictionary<string, int>();


    public void InitOp()
    {
        ///////////////////////////////////////////////////////
        //Init Op
        _opTagNames = new List<string>();
        _opShowOrNot = false;
               
         ///////
        //get op tag list  안경 제외!
        int tag_num = 0;
        for (int i = 0; i < UnityEditorInternal.InternalEditorUtility.tags.Length; i++)
        {
            if (UnityEditorInternal.InternalEditorUtility.tags[i].Contains("op") == true && UnityEditorInternal.InternalEditorUtility.tags[i].Contains("glasses") == false)
            {
                string tagName = UnityEditorInternal.InternalEditorUtility.tags[i];
                if (GameObject.FindGameObjectWithTag(tagName) == true)
                {
                    _opTagNames.Add(tagName);
                    tag_num++;
                }
            }
        }

        for (int i = 0; i < _opTagNames.Count; i++)
        {
            if (GameObject.FindGameObjectWithTag(_opTagNames[i]).transform.childCount != 0)
            {
                if (GameObject.FindGameObjectWithTag(_opTagNames[i]).GetComponent<MeshRenderer>() == true)
                {
                    GameObject.FindGameObjectWithTag(_opTagNames[i]).GetComponent<MeshRenderer>().enabled = false;
                }
                for (int j = 0; j < GameObject.FindGameObjectWithTag(_opTagNames[i]).transform.childCount; j++)
                {
                    GameObject.FindGameObjectWithTag(_opTagNames[i]).transform.GetChild(j).GetComponent<MeshRenderer>().enabled = false;
                }
            }
            else
            {
                GameObject.FindGameObjectWithTag(_opTagNames[i]).GetComponent<MeshRenderer>().enabled = false;
            }
        }
        GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().OpFFDRegistration();
    }

    public void InitGlasses()
    {
        m_glass_class_map = new Dictionary<string, int>();

        //Init Eyeglass Class       
        m_glass_class_map.Add("000", 0);
        m_glass_class_map.Add("002", 0);
        m_glass_class_map.Add("013", 0);
        m_glass_class_map.Add("014", 0);

        m_glass_class_map.Add("003", 1);
        m_glass_class_map.Add("015", 1);
        m_glass_class_map.Add("016", 1);
        m_glass_class_map.Add("017", 1);

        m_glass_class_map.Add("018", 2);
        m_glass_class_map.Add("019", 2);
        m_glass_class_map.Add("020", 2);
        m_glass_class_map.Add("021", 2);

        m_glass_class_map.Add("022", 3);
        m_glass_class_map.Add("023", 3);
        m_glass_class_map.Add("024", 3);
        m_glass_class_map.Add("025", 3);

        m_glass_class_map.Add("005", 4);
        m_glass_class_map.Add("007", 4);
        m_glass_class_map.Add("009", 4);
        m_glass_class_map.Add("010", 4);
        m_glass_class_map.Add("001", 4);

        m_glass_class_map.Add("004", 5);
        m_glass_class_map.Add("006", 5);
        m_glass_class_map.Add("011", 5);
        m_glass_class_map.Add("012", 5);

        m_glass_class_map.Add("008", 6);
        m_glass_class_map.Add("026", 6);
        m_glass_class_map.Add("027", 6);
        m_glass_class_map.Add("028", 6);

        m_glass_class_map.Add("029", 7);
        m_glass_class_map.Add("030", 7);
        m_glass_class_map.Add("031", 7);
        m_glass_class_map.Add("032", 7);

        //Init glass name map
        m_glass_name_map = new Dictionary<string, int>();
        m_glass_model_names = new List<string>();

        ///////////////////////////////////////////////////////
        //Init Op
        _opGlassTagNames = new List<string>();
        _opGlassShowOrNot = false;

        /////////
        ////get op tag list  안경 제외!
        ///

        for (int i = 0; i < GameObject.FindWithTag("glasses").transform.childCount; i++)
        {
            string eyeglass_name = GameObject.FindWithTag("glasses").transform.GetChild(i).name;
            m_glass_model_names.Add(eyeglass_name);

            m_glass_name_map.Add(m_glass_model_names[i], i);
        }
        
        for (int i = 0; i < m_glass_model_names.Count; i++)
        {
            GameObject.FindWithTag("glasses").transform.Find(m_glass_model_names[i]).gameObject.SetActive(false);
        }

        //int tag_num = 0;
        //for (int i = 0; i < UnityEditorInternal.InternalEditorUtility.tags.Length; i++)
        //{
        //    if (UnityEditorInternal.InternalEditorUtility.tags[i].Contains("op") == true && UnityEditorInternal.InternalEditorUtility.tags[i].Contains("glasses") == true)
        //    {
        //        string tagName = UnityEditorInternal.InternalEditorUtility.tags[i];
        //        if (GameObject.FindGameObjectWithTag(tagName) == true)
        //        {
        //            _opGlassTagNames.Add(tagName);
        //            tag_num++;
        //        }
        //    }
        //}

        //for (int i = 0; i < _opGlassTagNames.Count; i++)
        //{
        //    if (GameObject.FindGameObjectWithTag(_opGlassTagNames[i]).transform.childCount != 0)
        //    {
        //        if (GameObject.FindGameObjectWithTag(_opGlassTagNames[i]).GetComponent<MeshRenderer>() == true)
        //        {
        //            GameObject.FindGameObjectWithTag(_opGlassTagNames[i]).GetComponent<MeshRenderer>().enabled = false;
        //        }
        //        for (int j = 0; j < GameObject.FindGameObjectWithTag(_opGlassTagNames[i]).transform.childCount; j++)
        //        {
        //            GameObject.FindGameObjectWithTag(_opGlassTagNames[i]).transform.GetChild(j).GetComponent<MeshRenderer>().enabled = false;
        //        }
        //    }
        //    else
        //    {
        //        GameObject.FindGameObjectWithTag(_opGlassTagNames[i]).GetComponent<MeshRenderer>().enabled = false;
        //    }
        //}
        GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().EyeGlassFFDRegistration();
    }

    public void RandOp()
    {
        if (GameManager.GetInstance._OpGenerateOrNot == true)
        {
            float rand_val = UnityEngine.Random.Range(0.0f, 100.0f);
            if (GameManager.GetInstance._OpExperimentOrNot == true)
                rand_val = 100;

            if (rand_val < 95)  // Op 끄기!
            {
                _opShowOrNot = false;
                if (GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.childCount != 0)
                {
                    if (GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>() == true)
                    {
                        GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>().enabled = false;
                    }
                    for (int j = 0; j < GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.childCount; j++)
                    {
                        GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.GetChild(j).GetComponent<MeshRenderer>().enabled = false;
                    }
                }
                else
                {
                    GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>().enabled = false;
                }
            }
            else
            {
                _opShowOrNot = true;

                if (GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.childCount != 0)
                {
                    if (GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>() == true)
                    {
                        GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>().enabled = false;
                    }
                    for (int j = 0; j < GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.childCount; j++)
                    {
                        GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.GetChild(j).GetComponent<MeshRenderer>().enabled = false;
                    }
                }
                else
                {
                    GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>().enabled = false;
                }

                GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                if (GameManager.GetInstance._OpExperimentOrNot == true)
                {
                    _currOpIdx = GameManager.GetInstance._OpClass;
                }
                else
                {
                    _currOpIdx = UnityEngine.Random.Range(0, _opTagNames.Count);
                }
                if (GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.childCount != 0)
                {
                    if (GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>() == true)
                    {
                        GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>().enabled = true;

                        //Set Random Base Color
                        m_op_Renderer = GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<Renderer>();
                        m_op_Renderer.material.EnableKeyword("_BaseColorMap");

                        float h = UnityEngine.Random.Range(0.0f, 1.0f);
                        float s = UnityEngine.Random.Range(0.0f, 1.0f);
                        float v = UnityEngine.Random.Range(0.0f, 1.0f);

                        Color rgb_op_clr;
                        if (UnityEngine.Random.Range(0.0f, 100.0f) < 50) rgb_op_clr = Color.HSVToRGB(h, s, v);
                        else rgb_op_clr = new Color(1.0f, 1.0f, 1.0f);

                        m_op_Renderer.material.SetColor("_BaseColor", rgb_op_clr);
                    }
                    for (int j = 0; j < GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.childCount; j++)
                    {
                        GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.GetChild(j).GetComponent<MeshRenderer>().enabled = true;

                        if (GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.GetChild(j).name != "lens")
                        {
                            //Set Random Base Color
                            m_op_Renderer = GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.GetChild(j).GetComponent<Renderer>();
                            m_op_Renderer.material.EnableKeyword("_BaseColorMap");

                            float h = UnityEngine.Random.Range(0.0f, 1.0f);
                            float s = UnityEngine.Random.Range(0.0f, 1.0f);
                            float v = UnityEngine.Random.Range(0.0f, 1.0f);

                            Color rgb_op_clr;
                            if (UnityEngine.Random.Range(0.0f, 100.0f) < 50) rgb_op_clr = Color.HSVToRGB(h, s, v);
                            else rgb_op_clr = new Color(1.0f, 1.0f, 1.0f);

                            m_op_Renderer.material.SetColor("_BaseColor", rgb_op_clr);
                        }
                    }
                }
                else
                {
                    GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>().enabled = true;

                    //Set Random Base Color
                    m_op_Renderer = GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<Renderer>();
                    m_op_Renderer.material.EnableKeyword("_BaseColorMap");

                    float h = UnityEngine.Random.Range(0.0f, 1.0f);
                    float s = UnityEngine.Random.Range(0.0f, 1.0f);
                    float v = UnityEngine.Random.Range(0.0f, 1.0f);

                    Color rgb_op_clr;
                    if (UnityEngine.Random.Range(0.0f, 100.0f) < 50) rgb_op_clr = Color.HSVToRGB(h, s, v);
                    else rgb_op_clr = new Color(1.0f, 1.0f, 1.0f);

                    m_op_Renderer.material.SetColor("_BaseColor", rgb_op_clr);
                }
                GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().AdjustOpToFace_FFD();
            }
        }
        else
        {
            _opShowOrNot = false;
            if (GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.childCount != 0)
            {
                if (GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>() == true)
                {
                    GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>().enabled = false;
                }
                for (int j = 0; j < GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.childCount; j++)
                {
                    GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).transform.GetChild(j).GetComponent<MeshRenderer>().enabled = false;
                }
            }
            else
            {
                GameObject.FindGameObjectWithTag(_opTagNames[_currOpIdx]).GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }

    public void RandGlassesOp()
    {
        if (GameManager.GetInstance._EyeGlassGenerateOrNot == true)
        {
            float rand_val = UnityEngine.Random.Range(0.0f, 100.0f);
            if (GameManager.GetInstance._EyeglassExperimentOrNot == true)
                rand_val = 100;
            if (rand_val < 80)  // Op 끄기!
            {
                _opGlassShowOrNot = false;
                GameObject.FindWithTag("glasses").transform.localPosition = new Vector3(-240, 100, 0);
                GameObject.FindWithTag("glasses").transform.Find(m_glass_model_names[_currGlassOpIdx]).gameObject.SetActive(false);                
            }
            else
            {
                _opGlassShowOrNot = true;
                GameObject.FindWithTag("glasses").transform.localPosition = new Vector3(0, 0, 0);
                GameObject.FindWithTag("glasses").transform.Find(m_glass_model_names[_currGlassOpIdx]).gameObject.SetActive(false);

                GameObject.FindWithTag("glasses").transform.Find(m_glass_model_names[_currGlassOpIdx]).transform.localPosition = new Vector3(0, 0, 0);

                if (GameManager.GetInstance._EyeglassExperimentOrNot == true)
                {
                    _currGlassOpIdx = GameManager.GetInstance._EyeglassClass;
                    _currGlassClass = m_glass_class_map[m_glass_model_names[_currGlassOpIdx].Substring(m_glass_model_names[_currGlassOpIdx].Length - 3, 3)];
                                    
                    UnityEngine.Debug.Log("_currGlassOpIdx : " + _currGlassOpIdx + " , " + "_currGlassClass : " + _currGlassClass);
                }
                else
                {
                    _currGlassOpIdx = UnityEngine.Random.Range(0, m_glass_model_names.Count);
                    _currGlassClass = m_glass_class_map[m_glass_model_names[_currGlassOpIdx].Substring(m_glass_model_names[_currGlassOpIdx].Length - 3, 3)];
                }

                GameObject.FindWithTag("glasses").transform.Find(m_glass_model_names[_currGlassOpIdx]).gameObject.SetActive(true);

                //////////////
                //안경색 바꾸기
                GameManager.GetInstance._glass_h = UnityEngine.Random.Range(0.0f, 1.0f);
                GameManager.GetInstance._glass_s = UnityEngine.Random.Range(0.0f, 1.0f);
                GameManager.GetInstance._glass_v = UnityEngine.Random.Range(0.0f, 1.0f);

                Color rgb_op_clr;
                if (UnityEngine.Random.Range(0.0f, 100.0f) < 50) rgb_op_clr = Color.HSVToRGB(GameManager.GetInstance._glass_h, GameManager.GetInstance._glass_s, GameManager.GetInstance._glass_v);
                else rgb_op_clr = new Color(1.0f, 1.0f, 1.0f);

                List<GameObject> glass_frame_list = GetGlassFrameGameObject(m_glass_model_names[_currGlassOpIdx]);

                for(int i = 0; i < glass_frame_list.Count; i++)
                {
                    m_op_Renderer = glass_frame_list[i].GetComponent<Renderer>();
                    m_op_Renderer.material.EnableKeyword("_BaseColorMap");
                    m_op_Renderer.material.SetColor("_BaseColor", rgb_op_clr);                    
                }
            }
        }
        else
        {
            _opGlassShowOrNot = false;
            GameObject.FindWithTag("glasses").transform.localPosition = new Vector3(-240, 100, 0);
            GameObject.FindWithTag("glasses").transform.Find(m_glass_model_names[_currGlassOpIdx]).gameObject.SetActive(false);
        }   
    }

    List<GameObject> GetGlassFrameGameObject(string glass_name)
    {
        Transform parent = GameObject.FindWithTag("glasses").transform.Find(glass_name).GetChild(0);
        if (parent == null) return null;

        List<GameObject> go_list = new List<GameObject>();

        for (int i = 0; i < parent.childCount; ++i)
        {
            GameObject go = parent.GetChild(i).transform.gameObject;

            string name = go.name;
                      
            if(name.Contains("Len") == true || name.Contains("len"))
            {
                continue;
            }
            else
            {
                if(go.GetComponent<Renderer>() == true)
                {
                    go_list.Add(go);
                }
            }
        }
        return go_list;
    }
      
    // Use this for initialization
    void Start () {
        InitOp();
        InitGlasses();

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
