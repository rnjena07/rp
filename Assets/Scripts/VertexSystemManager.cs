﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;

public class VertexSystemManager : Singleton<VertexSystemManager> {

    // Use this for initialization
    public struct VertexSystem
    {
        public string m_go_name;
        public int m_nb_of_vertex;
        public int m_nb_of_vertex_in_unity;
        public Vector2[] m_vt_to_v;
        public Vector2[] m_v_to_vt;
        public float[] m_algorithm_obj_vertex;
    }

    public Dictionary<string, VertexSystem> m_vertex_systems;
    
    public void Init()
    {
        m_vertex_systems = new Dictionary<string, VertexSystem>();
    }

    public void RegistGO2VertexSystem(Vector3[] unity_vertex, string name, string obj_file_path)
    {
        VertexSystem vertex_system = new VertexSystem();

        vertex_system.m_go_name = name;
        vertex_system.m_nb_of_vertex_in_unity = unity_vertex.GetLength(0);

        ////////////////////////////////////
        //Read Obj File as Text
        Vector3[] base_obj_vertex;

        List<Vector3> obj_vertex = new List<Vector3>();
              
        StreamReader the_obj_reader = new StreamReader(obj_file_path, Encoding.Default);
        string obj_line;

        int curr_obj_line_idx = 0;
        using (the_obj_reader)
        {
            obj_line = the_obj_reader.ReadLine();
            if (obj_line != null)
            {
                // While there's lines left in the text file, do this:
                do
                {
                    // Do whatever you need to do with the text line, it's a string now
                    // In this example, I split it into arguments based on comma
                    // deliniators, then send that array to DoStuff()
                    //string[] entries = obj_line.Split(null);
                    string[] entries = obj_line.Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    if (entries.Length == 4 && entries[0] == "v")
                    {
                        Vector3 vtx;
                        vtx.x = Convert.ToSingle(entries[1]);
                        vtx.y = Convert.ToSingle(entries[2]);
                        vtx.z = Convert.ToSingle(entries[3]);

                        obj_vertex.Add(vtx);
                        curr_obj_line_idx++;
                    }
                    obj_line = the_obj_reader.ReadLine();
                }
                while (obj_line != null);
            }
            // Done reading, close the reader and return true to broadcast success    
            the_obj_reader.Close();
        }

        base_obj_vertex = obj_vertex.ToArray();
        vertex_system.m_nb_of_vertex = curr_obj_line_idx;

        vertex_system.m_vt_to_v = new Vector2[vertex_system.m_nb_of_vertex_in_unity];
        vertex_system.m_v_to_vt = new Vector2[vertex_system.m_nb_of_vertex];
        ////////////////////////////////////
        //Unity의 메쉬에서 직접 vertex 값을 가져오고 mapping을 형성하자!!!

        //Mesh mesh = target.GetComponent<MeshFilter>().mesh;
      
        //////////////////////////////////////
        //Transfer to Unity Vertex syntax
        for (int i = 0; i < vertex_system.m_nb_of_vertex_in_unity; i++)
        {
            float v_unity_x = unity_vertex[i].x;
            float v_unity_y = unity_vertex[i].y;
            float v_unity_z = unity_vertex[i].z;

            for (int j = 0; j < vertex_system.m_nb_of_vertex; j++)
            {
                float v_x = -base_obj_vertex[j].x;
                float v_y = base_obj_vertex[j].y;
                float v_z = base_obj_vertex[j].z;

                if (v_unity_x == v_x && v_unity_y == v_y && v_unity_z == v_z)
                {
                    vertex_system.m_vt_to_v[i].x = i;
                    vertex_system.m_vt_to_v[i].y = j;
                    // Debug.Log("Vt to V Success : " + i + "  from  " + j);
                    break;
                }
            }
        }

        for (int i = 0; i < vertex_system.m_nb_of_vertex; i++)
        {
            vertex_system.m_v_to_vt[i].x = i;
            for (int j = 0; j < vertex_system.m_nb_of_vertex_in_unity; j++)
            {
                if (vertex_system.m_vt_to_v[j].y == i)
                {
                    vertex_system.m_v_to_vt[i].y = j;
                    break;
                }
            }
        }

        vertex_system.m_algorithm_obj_vertex = new float[3 * vertex_system.m_nb_of_vertex];

        for (int i = 0; i < vertex_system.m_nb_of_vertex; i++)
        {
            vertex_system.m_algorithm_obj_vertex[3 * i + 0] = base_obj_vertex[i].x;
            vertex_system.m_algorithm_obj_vertex[3 * i + 1] = base_obj_vertex[i].y;
            vertex_system.m_algorithm_obj_vertex[3 * i + 2] = base_obj_vertex[i].z;
        }

        m_vertex_systems.Add(name, vertex_system);
    }
    
    public Vector3[] SetGameObjectModelToAlgorithmVertex(string name, Vector3[] vertex_unity)
    {
        Vector3[] new_vector3s = new Vector3[m_vertex_systems[name].m_nb_of_vertex];

        //////////////////////////////////////
        //Transfer to Unity Vertex syntax
        for (int i = 0; i < m_vertex_systems[name].m_nb_of_vertex; i++)
        {
            int vertexIdx = (int)m_vertex_systems[name].m_v_to_vt[i].y;

            if (vertexIdx >= m_vertex_systems[name].m_nb_of_vertex_in_unity)
            {
                UnityEngine.Debug.Log("vertexIdx : " + vertexIdx);
            }
            new_vector3s[i].x = -vertex_unity[vertexIdx].x;
            new_vector3s[i].y = vertex_unity[vertexIdx].y;
            new_vector3s[i].z = vertex_unity[vertexIdx].z;
        }
        return new_vector3s;
    }

    public Vector3[] SetAlgorithmVertexToGameObjectModel(string name, Vector3[] vertex_algorithm)
    {
        Vector3[] new_vector3s = new Vector3[m_vertex_systems[name].m_nb_of_vertex_in_unity];

        //////////////////////////////////////
        //Transfer to Unity Vertex syntax
        for (int i = 0; i < m_vertex_systems[name].m_nb_of_vertex_in_unity; i++)
        {
            int vertexIdx = (int)m_vertex_systems[name].m_vt_to_v[i].y;

            if (vertexIdx >= m_vertex_systems[name].m_nb_of_vertex)
            {
                UnityEngine.Debug.Log("vertexIdx : " + vertexIdx);
            }
            new_vector3s[i].x = -vertex_algorithm[vertexIdx].x;
            new_vector3s[i].y = vertex_algorithm[vertexIdx].y;
            new_vector3s[i].z = vertex_algorithm[vertexIdx].z;
        }
        return new_vector3s;
    }

    public Vector3[] SetAlgorithmVertexToGameObjectModel(string name, float[] vertex_algorithm)
    {
        Vector3[] new_vector3s = new Vector3[m_vertex_systems[name].m_nb_of_vertex_in_unity];

        //////////////////////////////////////
        //Transfer to Unity Vertex syntax
        for (int i = 0; i < m_vertex_systems[name].m_nb_of_vertex_in_unity; i++)
        {
            int vertexIdx = (int)m_vertex_systems[name].m_vt_to_v[i].y;

            if (vertexIdx >= m_vertex_systems[name].m_nb_of_vertex)
            {
                UnityEngine.Debug.Log("vertexIdx : " + vertexIdx);
            }
            new_vector3s[i].x = -vertex_algorithm[3 * vertexIdx + 0];
            new_vector3s[i].y = vertex_algorithm[3 * vertexIdx + 1];
            new_vector3s[i].z = vertex_algorithm[3 * vertexIdx + 2];
        }
        return new_vector3s;
    }

    void Awake () {
        Init();
    }
	
	//// Update is called once per frame
	//void Update () {
		
	//}
}
