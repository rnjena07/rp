using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;

using System.Diagnostics;

using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra.Double;
using MathNet.Numerics.LinearAlgebra;

using mpm;

using System.Linq;

public class GameManager : Singleton<GameManager>
{
    //public static GameManager instance = null;   
    [NonSerialized] public string _faceTag;
    [NonSerialized] public string _lEyeTag;
    [NonSerialized] public string _rEyeTag;
    [NonSerialized] public string _lEyeBallTag;
    [NonSerialized] public string _rEyeBallTag;
    [NonSerialized] public string _lEyeLensTag;
    [NonSerialized] public string _rEyeLensTag;
    [NonSerialized] public string _backgroundTag;
    [NonSerialized] public string _teethTag;
    [NonSerialized] public string _glassesTag;
    [NonSerialized] public string _opTag;

    public float _deltaTime = 2;
    [NonSerialized] public float _currDeltaTime = 0;

    [NonSerialized] public Vector3 headRotAng;
    [NonSerialized] public Vector3 leftEyeRotAng;
    [NonSerialized] public Vector3 rightEyeRotAng;
        
    [NonSerialized] public List<string> _eye_class;
    [NonSerialized] public List<float> _eye_mean_h;
    [NonSerialized] public List<float> _eye_mean_s;
    [NonSerialized] public List<float> _eye_mean_v;

    [NonSerialized] public List<float> _eye_std_h;
    [NonSerialized] public List<float> _eye_std_s;
    [NonSerialized] public List<float> _eye_std_v;

    [NonSerialized] public List<float> _eye_min_h;
    [NonSerialized] List<float> _eye_min_s;
    [NonSerialized] public List<float> _eye_min_v;

    [NonSerialized] public List<float> _eye_max_h;
    [NonSerialized] public List<float> _eye_max_s;
    [NonSerialized] public List<float> _eye_max_v;


    //public List<float> _eye_mean_h;
    //public List<float> _eye_mean_s;
    //public List<float> _eye_mean_v;

    //public List<float> _eye_std_h;
    //public List<float> _eye_std_s;
    //public List<float> _eye_std_v;

    //public List<float> _eye_min_h;
    //public List<float> _eye_min_s;
    //public List<float> _eye_min_v;

    //public List<float> _eye_max_h;
    //public List<float> _eye_max_s;
    //public List<float> _eye_max_v;

    [NonSerialized] public float _eye_pupil_size;
    [NonSerialized] public float _eye_pupil_rotate;

    [NonSerialized] public float _glass_h;
    [NonSerialized] public float _glass_s;
    [NonSerialized] public float _glass_v;
    
    private Color _rnd_eye_color;
    [NonSerialized] public List<string> _eye_irish_diffuse_file_list;
    [NonSerialized] public string _eye_pupil_diffuse_file_path;


    [NonSerialized] public bool _hairShowOrNot;

    Stopwatch stopWatch;
    Stopwatch stopWatch_tot;
    
    public ObjImporter _obj_eye;
    public ObjImporter _obj_face;
    
    List<Texture2D> _eye_irish_diffuse_texs;
    Texture2D _eye_pupil_diffuse_texs;

    [Header("Main Function")]
    [SerializeField] public bool _FaceSimulOrGenerate;
    [SerializeField] public bool _LandmarkDataSetGeneration;
    [SerializeField] public bool _LandmarkWarpingDatasetGeneration;
    [SerializeField] public bool _FaceParamDataSetGeneration;
    [SerializeField] public bool _PreLoadBackground;

    [Header("Generator Function")]
    [SerializeField] public bool _FaceShapeGenerateOrNot;
    [SerializeField] public bool _FaceTextureGenerateOrNot;
    [SerializeField] public bool _FaceRotateOrNot;

    [SerializeField] public bool _EyeGenerateOrNot;
    [SerializeField] public bool _EyeRotateOrNot;

    [SerializeField] public bool _EyeBrowsGenerateOrNot;
    [SerializeField] public bool _HairGenerateOrNot;
    [SerializeField] public bool _OpGenerateOrNot;
    [SerializeField] public bool _EyeGlassGenerateOrNot;
    [SerializeField] public bool _ShowFaceFeatureOrNot;

    [SerializeField] public bool _RandomLightOrNot;
    [SerializeField] public bool _RandomBackgroundOrNot;

    [SerializeField] public bool _DataSaveOrNot;
    [SerializeField] public bool _FacialLandmarkSaveOrNot;

    [Header("Eyebrow Experiment")]
    [SerializeField] public bool _EyeBrowExperimentOrNot;
    [SerializeField] public int _EyeBrowClass;
    [SerializeField] public int _EyeBrowFile;


    [Header("Eye Experiment")]
    [SerializeField] public bool _EyeExperimentOrNot;
    [SerializeField] public int _EyeClass;
    [SerializeField] public int _IrishFileIdx;

    [Header("Skin Experiment")]
    [SerializeField] public bool _SkinInfoView;

    [Header("Hair Experiment")]
    [SerializeField] public bool _HairExperimentOrNot;
    [SerializeField] public int _HairClass;

    [Header("Eyeglass Experiment")]
    [SerializeField] public bool _EyeglassExperimentOrNot;
    [SerializeField] public int _EyeglassClass;

    [Header("Op Experiment")]
    [SerializeField] public bool _OpExperimentOrNot;
    [SerializeField] public int _OpClass;


    [NonSerialized] public bool m_nose_boundary_smooth_or_not = true;

    GameObject m_mpm_face;

    // Use this for initialization
    void Awake () {
        //if (instance == null)
        //    instance = this;
        //else if (instance != this)
        //    Destroy(gameObject);
        InitGame();
  	}
	
    void InitGame()
    {
        //_FaceSimulOrGenerate = true;

        if(_FaceSimulOrGenerate == true)
        {
            _FaceShapeGenerateOrNot = false;
            _FaceTextureGenerateOrNot = false;
            _FaceRotateOrNot = false;
            _EyeGenerateOrNot = false;
            _EyeRotateOrNot = false;
            _EyeBrowsGenerateOrNot = false;
            _HairGenerateOrNot = false;
            _OpGenerateOrNot = false;
            _EyeGlassGenerateOrNot = false;
            _RandomBackgroundOrNot = false;
            _RandomLightOrNot = false;
            _DataSaveOrNot = false;
            _FacialLandmarkSaveOrNot = false;
        }   

        if(_FaceParamDataSetGeneration == true)
        {
            _FaceSimulOrGenerate = false;
            _FaceShapeGenerateOrNot = true;
            _FaceTextureGenerateOrNot = true;
            _FaceRotateOrNot = true;
            _EyeGenerateOrNot = true;
            _EyeRotateOrNot = true;
            _EyeBrowsGenerateOrNot = true;
            _HairGenerateOrNot = true;
            _OpGenerateOrNot = true;
            _EyeGlassGenerateOrNot = true;
            _RandomBackgroundOrNot = true;
            _RandomLightOrNot = true;
            _DataSaveOrNot = true;
            _FacialLandmarkSaveOrNot = true;
        }

        if(_LandmarkWarpingDatasetGeneration == true)
        {
            _FaceParamDataSetGeneration = false;
            _EyeBrowsGenerateOrNot = false;
            _FaceShapeGenerateOrNot = true;
            _FaceTextureGenerateOrNot = true;
            _FaceRotateOrNot = true;
            _EyeGenerateOrNot = true;
            _EyeRotateOrNot = true;
            _HairGenerateOrNot = true;
            _OpGenerateOrNot = true;
            _EyeGlassGenerateOrNot = true;
            _RandomBackgroundOrNot = true;
            _RandomLightOrNot = true;
            _DataSaveOrNot = true;
            _FacialLandmarkSaveOrNot = true;

            _ShowFaceFeatureOrNot = false;
        }

        Hair.GetInstance._hair_curr_idx = 0;
        Hair.GetInstance._hair_prev_idx = 0;
        //GameObject.FindWithTag("MainCamera").GetComponent<Camera>().usePhysicalProperties = true;

        _obj_eye = new ObjImporter();
        string eyeMTLFilePath = PathManager.GetInstance._FaceDatabaseFolderPath + "/Shape/Eye/model/eyeBall.mtl";
        _obj_eye.ImportMTLFile(eyeMTLFilePath);

        //_obj_face = new ObjImporter();
        //string faceMTLFilePath = _FaceDatabaseFolderPath + "/Shape/Face/Ver_2/face.mtl";
        //_obj_face.ImportMTLFile(faceMTLFilePath);

        //string faceOBJFilePath = _FaceDatabaseFolderPath + "/Shape/Face/Ver_2/face.obj";
        //_obj_face.ImportOBJFile(faceOBJFilePath);

        _faceTag = "face";
        _lEyeTag = "left_eye";
        _rEyeTag = "right_eye";
        _lEyeBallTag = "left_eye_ball";
        _rEyeBallTag = "right_eye_ball";
        _lEyeLensTag = "left_eye_lens";
        _rEyeLensTag = "right_eye_lens";
        _backgroundTag = "background";
        _teethTag = "teeth";
        _glassesTag = "glasses";
        _opTag = "op";

        /////////////////////////////////////////////////////////////////////

        InitEyeTextureDataset(); // Initialize Face Model Files Names which is used... on training, also used on Eye Texture
        
        stopWatch = new Stopwatch();
        stopWatch_tot = new Stopwatch();

        headRotAng = new Vector3(0.0f, 180.0f, 0.0f);
        leftEyeRotAng = new Vector3(0.0f, 0.0f, 0.0f);
        rightEyeRotAng = new Vector3(0.0f, 0.0f, 0.0f);

        GameObject.FindGameObjectWithTag("light").transform.localPosition = new Vector3(-100, 170, 150);
             
        //m_mpm_face.GetComponent<PCA_FaceModel>().SetHairBaseFace();

        _hairShowOrNot = false;

        //OpManager.GetInstance.InitOp();
        //OpManager.GetInstance.InitGlasses();
        InitEye();

        BackgroundManager.GetInstance.Init();

        m_mpm_face = GameObject.FindGameObjectWithTag("face");
    }

    void InitEye()
    {
        _eye_class = new List<string>();
        _eye_mean_h = new List<float>();
        _eye_min_h = new List<float>();
        _eye_max_h = new List<float>();
        _eye_std_h = new List<float>();
        
        _eye_mean_s = new List<float>();
        _eye_min_s = new List<float>();
        _eye_max_s = new List<float>();
        _eye_std_s = new List<float>();

        _eye_mean_v = new List<float>();
        _eye_min_v = new List<float>();
        _eye_max_v = new List<float>();
        _eye_std_v = new List<float>();

        _eye_class.Add("brown");
        _eye_class.Add("green");
        _eye_class.Add("blue");

        //////////////////////////////////////////////////////////
        ///
        //////////////////////
        //Brown
        _eye_mean_h.Add(22.3846f / 360.0f);
        _eye_min_h.Add(16.0f / 360.0f);
        _eye_max_h.Add(38.0f / 360.0f);
        _eye_std_h.Add(7.240865f / 360.0f);

        _eye_mean_s.Add(88f / 100.0f);
        _eye_min_s.Add(70.0f / 100.0f);
        _eye_max_s.Add(100.0f / 100.0f);
        _eye_std_s.Add(7.633f / 100.0f);

        _eye_mean_v.Add(50.0f / 100.0f);
        _eye_min_v.Add(40.0f / 100.0f);
        _eye_max_v.Add(100.0f / 100.0f);
        _eye_std_v.Add(8.437f / 100.0f);

        //////////////////////
        //Green
        _eye_mean_h.Add(156.0f / 360.0f);
        //_eye_min_h.Add(50.0f / 360.0f);
        //_eye_max_h.Add(162.0f / 360.0f);
        _eye_min_h.Add(0.14f);
        _eye_max_h.Add(0.38f);
        _eye_std_h.Add(19.90889f / 360.0f);

        _eye_mean_s.Add(0.6f);
        _eye_min_s.Add(0.28f);
        _eye_max_s.Add(0.44f);
        _eye_std_s.Add(7.618f / 100.0f);

        _eye_mean_v.Add(0.83f);
        _eye_min_v.Add(0.55f);
        _eye_max_v.Add(100.0f / 100.0f);
        _eye_std_v.Add(9.34f / 100.0f);

        //////////////////////
        //Blue
        _eye_mean_h.Add(188.0f / 360.0f);
        _eye_min_h.Add(181.0f / 360.0f);
        _eye_max_h.Add(206.0f / 360.0f);
        _eye_std_h.Add(9.9664534f / 360.0f);

        _eye_mean_s.Add(50.0f / 100.0f);
        _eye_min_s.Add(0.0f / 100.0f);
        _eye_max_s.Add(100.0f / 100.0f);
        _eye_std_s.Add(30.12f / 100.0f);

        _eye_mean_v.Add(82.5f / 100.0f);
        _eye_min_v.Add(65.0f / 100.0f);
        _eye_max_v.Add(100.0f / 100.0f);
        _eye_std_v.Add(15.268f / 100.0f);


        _EyeClass = 0;
    }
    
    void InitEyeTextureDataset()
    {
        _eye_irish_diffuse_file_list = new List<string>();
        string[] eye_irish_file_paths = Directory.GetFiles(PathManager.GetInstance._EyeTextureModelFolderPath + "/iris");
        _eye_irish_diffuse_file_list = eye_irish_file_paths.ToList();

        _eye_irish_diffuse_texs = new List<Texture2D>();

        for (int i = 0; i < eye_irish_file_paths.Length; i++)
        {
            Texture2D diffuse_tex = new Texture2D(1, 1);
            var diffuseBytes = System.IO.File.ReadAllBytes(_eye_irish_diffuse_file_list[i]);
            var m_diffuse_tex = new Texture2D(1, 1);
            diffuse_tex.LoadImage(diffuseBytes);
            _eye_irish_diffuse_texs.Add(diffuse_tex);
        }
        _eye_pupil_diffuse_file_path = PathManager.GetInstance._EyeTextureModelFolderPath + "/pupil/pupil.png";

        _eye_pupil_diffuse_texs = new Texture2D(1, 1);
        var eye_pupil_diffuse_bytes = System.IO.File.ReadAllBytes(_eye_pupil_diffuse_file_path);
        _eye_pupil_diffuse_texs.LoadImage(eye_pupil_diffuse_bytes);
    }

    //void InitMPM()
    //{
    //    m_mpm_face.GetComponent<PCA_FaceModel>().ExpressionInfoInit();        
    //    m_mpm_face.GetComponent<PCA_FaceModel>().LoadMPM();
    //    m_mpm_face.GetComponent<PCA_FaceModel>().ParameterCoverInitialize();
    //    m_mpm_face.GetComponent<PCA_FaceModel>().GenerateVtToVMap();
    //    m_mpm_face.GetComponent<PCA_FaceModel>().InitFacialFeature();

    //    m_mpm_face.GetComponent<PCA_FaceModel>().ParameterCoverMapInitialize();
    //}

    public static float GenerateNormalRandom(float mean, float sigma, float min, float max)
    {
        float rand1 = UnityEngine.Random.Range(0.0f, 1.0f);
        float rand2 = UnityEngine.Random.Range(0.0f, 1.0f);

        float n = Mathf.Sqrt(-2.0f * Mathf.Log(rand1)) * Mathf.Cos((2.0f * Mathf.PI) * rand2);

        float generatedNumber = mean + sigma * n;
//        float generatedNumber = Mathf.FloorToInt(mean + sigma * n);

        generatedNumber = Mathf.Clamp(generatedNumber, min, max);

        return generatedNumber;
    }
    


    void AdjustHairToFace()
    {
        m_mpm_face.GetComponent<PCA_FaceModel>().AdjustHairToFace();
    }

    void RandomEye()
    {
        if (_EyeExperimentOrNot == false)
        {
            if (_EyeGenerateOrNot == true)
            {
                //Change Irish Texture
                _IrishFileIdx = UnityEngine.Random.Range(0, _eye_irish_diffuse_texs.Count);
                GameObject.FindGameObjectWithTag(_lEyeTag).GetComponent<Eye>().SetTexture(_eye_irish_diffuse_texs[_IrishFileIdx]);
                GameObject.FindGameObjectWithTag(_rEyeTag).GetComponent<Eye>().SetTexture(_eye_irish_diffuse_texs[_IrishFileIdx]);

                _EyeClass = UnityEngine.Random.Range(0, _eye_class.Count);
                //_eye_curr_idx = 2;
                RandomEyeColor();

                GameObject.FindGameObjectWithTag(_lEyeTag).GetComponent<Eye>().SetColorViaShader(_rnd_eye_color);
                GameObject.FindGameObjectWithTag(_rEyeTag).GetComponent<Eye>().SetColorViaShader(_rnd_eye_color);

                GameObject.FindGameObjectWithTag(_lEyeTag).GetComponent<Eye>().SetPupilSizeViaShader(_eye_pupil_size, _eye_pupil_rotate);
                GameObject.FindGameObjectWithTag(_rEyeTag).GetComponent<Eye>().SetPupilSizeViaShader(_eye_pupil_size, _eye_pupil_rotate);

                ////Change Eye Position                       
                //m_mpm_face.GetComponent<PCA_FaceModel>().ScaleTranslateEyeToFace();
            }
        }
        else
        {
            GameObject.FindGameObjectWithTag(_lEyeTag).GetComponent<Eye>().SetTexture(_eye_irish_diffuse_texs[_IrishFileIdx]);
            GameObject.FindGameObjectWithTag(_rEyeTag).GetComponent<Eye>().SetTexture(_eye_irish_diffuse_texs[_IrishFileIdx]);

            RandomEyeColor();

            GameObject.FindGameObjectWithTag(_lEyeTag).GetComponent<Eye>().SetColorViaShader(_rnd_eye_color);
            GameObject.FindGameObjectWithTag(_rEyeTag).GetComponent<Eye>().SetColorViaShader(_rnd_eye_color);

            GameObject.FindGameObjectWithTag(_lEyeTag).GetComponent<Eye>().SetPupilSizeViaShader(_eye_pupil_size, _eye_pupil_rotate);
            GameObject.FindGameObjectWithTag(_rEyeTag).GetComponent<Eye>().SetPupilSizeViaShader(_eye_pupil_size, _eye_pupil_rotate);

        }
    }

    void RandomLight()
    {
        if (_RandomLightOrNot == true)
        {
            float angX = GenerateNormalRandom(15, 30, 15 - 40, 15 + 40);
            float angY = GenerateNormalRandom(230, 30, 230 - 30, 230 + 30);
            float angZ = GenerateNormalRandom(42, 30, 42 - 30, 42 + 30);
            GameObject.FindGameObjectWithTag("light").transform.localRotation = Quaternion.Euler(angX, angY, angZ);

            float lux = GenerateNormalRandom(5f, 4.5f, 1, 20);
            GameObject.FindGameObjectWithTag("light").GetComponent<Light>().intensity = lux;
        }
    }

    void RandomHair()
    {
        if (_HairGenerateOrNot == true)
        {
            if (UnityEngine.Random.Range(0.0f, 100.0f) < 80)
            {
                _hairShowOrNot = true;
                GameObject.FindGameObjectWithTag("hair").GetComponent<Hair>().SetRandomHair();
                //UnityEngine.Debug.Log("Current Hair Idx : " + GameObject.FindGameObjectWithTag("hair").GetComponent<hair>().m_hair_file_idx);
                GameObject.FindGameObjectWithTag("hair").transform.localPosition = new Vector3(0, 0, 0);
                //m_mpm_face.GetComponent<PCA_FaceModel>().AdjustHairToFace();
                m_mpm_face.GetComponent<PCA_FaceModel>().AdjustHairToFace_FFD();
            }
            else
            {
                GameObject.FindGameObjectWithTag("hair").transform.localPosition = new Vector3(0, 0, -100);
                _hairShowOrNot = false;
            }
        }
        else
        {
            GameObject.FindGameObjectWithTag("hair").transform.localPosition = new Vector3(0, 0, -100);
            _hairShowOrNot = false;
        }
    }

    void RandomBackground()
    {
        if (_RandomBackgroundOrNot == true)
        {
            if (_PreLoadBackground == false)
            {
                BackgroundManager.GetInstance.ReadRandomImgSetTexture();
            }
            else
            {
                BackgroundManager.GetInstance.SetRandomTexture();
            }
        }
    }
    
    void RandomFaceShape()
    {
        if (_FaceShapeGenerateOrNot)
        {
            //float time_tot = Time.realtimeSinceStartup;
            //float time_elapsed;

            m_mpm_face.GetComponent<PCA_FaceModel>().GenerateRandomFace();

            //time_elapsed = Time.realtimeSinceStartup - time_tot;
            //UnityEngine.Debug.Log("GenerateRandomFace Time : " + time_elapsed);
            //time_tot = Time.realtimeSinceStartup;


            GameObject.FindGameObjectWithTag(_teethTag).GetComponent<Teeth>().SetTeethOrientation();

            //time_elapsed = Time.realtimeSinceStartup - time_tot;
            //UnityEngine.Debug.Log("SetTeethOrientation Time : " + time_elapsed);
            //time_tot = Time.realtimeSinceStartup;

            //Change Eye Position                       
            m_mpm_face.GetComponent<PCA_FaceModel>().ScaleTranslateEyeToFace();

            //time_elapsed = Time.realtimeSinceStartup - time_tot;
            //UnityEngine.Debug.Log("ScaleTranslateEyeToFace Time : " + time_elapsed);
            //time_tot = Time.realtimeSinceStartup;
        }
    }

    void NeutralExpressionFaceShape()
    {
        if (_FaceShapeGenerateOrNot)
        {
            //float time_tot = Time.realtimeSinceStartup;
            //float time_elapsed;

            m_mpm_face.GetComponent<PCA_FaceModel>().GenerateNeutralExpressionFace();

            //time_elapsed = Time.realtimeSinceStartup - time_tot;
            //UnityEngine.Debug.Log("GenerateRandomFace Time : " + time_elapsed);
            //time_tot = Time.realtimeSinceStartup;


            GameObject.FindGameObjectWithTag(_teethTag).GetComponent<Teeth>().SetTeethOrientation();

            //time_elapsed = Time.realtimeSinceStartup - time_tot;
            //UnityEngine.Debug.Log("SetTeethOrientation Time : " + time_elapsed);
            //time_tot = Time.realtimeSinceStartup;

            //Change Eye Position                       
            m_mpm_face.GetComponent<PCA_FaceModel>().ScaleTranslateEyeToFace();

            //time_elapsed = Time.realtimeSinceStartup - time_tot;
            //UnityEngine.Debug.Log("ScaleTranslateEyeToFace Time : " + time_elapsed);
            //time_tot = Time.realtimeSinceStartup;
        }
    }

    void RandomFaceTexture()
    {
        ////////////////////////////////////////////////////
        //TODO : YJ
        //Change Face Texture And Shading Map and Parameter
        if (_FaceTextureGenerateOrNot)
        {
            m_mpm_face.GetComponent<PCA_FaceModel>().RandFaceTextureIndex();
            m_mpm_face.GetComponent<PCA_FaceModel>().SetFaceTexture();
        }
    }

    void RandomFaceRotate()
    {
        if (_FaceRotateOrNot == true)
        {
            headRotAng.x = GenerateNormalRandom(0, 10, -30, 30);
            headRotAng.y = GenerateNormalRandom(0, 10, -30, 30);
            headRotAng.z = GenerateNormalRandom(0, 10, -20, 20);

            if (UnityEngine.Random.Range(0.0f, 99f) < 30)
            {
                headRotAng.x = 0.0f;
            }

            if (UnityEngine.Random.Range(0.0f, 99f) < 30)
            {
                headRotAng.y = 0.0f;
            }

            if (UnityEngine.Random.Range(0.0f, 99f) < 30)
            {
                headRotAng.z = 0.0f;
            }
            m_mpm_face.transform.localRotation = Quaternion.Euler(headRotAng.x, headRotAng.y, headRotAng.z);
        }
        else
        {
            headRotAng.x = 0.0f;
            headRotAng.y = 0.0f;
            headRotAng.z = 0.0f;
            m_mpm_face.transform.localRotation = Quaternion.Euler(headRotAng.x, headRotAng.y, headRotAng.z);
        }
    }

    void FaceRotateReset()
    {
        //headRotAng.x = 0.0f;
        //headRotAng.y = 0.0f;
        //headRotAng.z = 0.0f;
        m_mpm_face.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
    }

    void RandomEyeRotate()
    {
        if (_EyeRotateOrNot == true)
        {
            leftEyeRotAng.x = UnityEngine.Random.Range(-10f, 10f);
            leftEyeRotAng.y = UnityEngine.Random.Range(-30f, 30f);
            leftEyeRotAng.z = 0.0f;

            rightEyeRotAng.x = leftEyeRotAng.x;
            rightEyeRotAng.y = leftEyeRotAng.y;
            rightEyeRotAng.z = leftEyeRotAng.z;

            if (UnityEngine.Random.Range(0.0f, 99f) < 5)
            {
                rightEyeRotAng.x = UnityEngine.Random.Range(-10f, 10f);
                rightEyeRotAng.y = UnityEngine.Random.Range(-30f, 30f);
                rightEyeRotAng.z = 0.0f;
            }
            else
            {
                rightEyeRotAng.x = leftEyeRotAng.x;
                rightEyeRotAng.y = leftEyeRotAng.y;
                rightEyeRotAng.z = leftEyeRotAng.z;
            }
            GameObject.FindGameObjectWithTag(_lEyeTag).transform.localRotation = Quaternion.Euler(leftEyeRotAng.x, leftEyeRotAng.y, leftEyeRotAng.z);
            GameObject.FindGameObjectWithTag(_rEyeTag).transform.localRotation = Quaternion.Euler(rightEyeRotAng.x, rightEyeRotAng.y, rightEyeRotAng.z);
        }
        else
        {
            leftEyeRotAng.x = 0.0f;
            leftEyeRotAng.y = 0.0f;
            leftEyeRotAng.z = 0.0f;

            rightEyeRotAng.x = 0.0f;
            rightEyeRotAng.y = 0.0f;
            rightEyeRotAng.z = 0.0f;

            GameObject.FindGameObjectWithTag(_lEyeTag).transform.localRotation = Quaternion.Euler(leftEyeRotAng.x, leftEyeRotAng.y, leftEyeRotAng.z);
            GameObject.FindGameObjectWithTag(_rEyeTag).transform.localRotation = Quaternion.Euler(rightEyeRotAng.x, rightEyeRotAng.y, rightEyeRotAng.z);
        }
    }

    void RandomEyeBrow()
    {
        if (_EyeBrowExperimentOrNot == false)
        {
            if (_EyeBrowsGenerateOrNot == true)
            {
                m_mpm_face.GetComponent<EyeBrow>().EyeBrowTextureTurnOn();
                m_mpm_face.GetComponent<EyeBrow>().RandomEyeBrowIndex();
                m_mpm_face.GetComponent<EyeBrow>().SetEyeBrowTexture();
            }
            else
            {
                m_mpm_face.GetComponent<EyeBrow>().EyeBrowTextureTurnOff();
            }
        }
        else
        {
            m_mpm_face.GetComponent<EyeBrow>().EyeBrowTextureTurnOn();
            m_mpm_face.GetComponent<EyeBrow>().SetEyeBrowTextureFromClass(_EyeBrowClass, _EyeBrowFile);
        }
    }

    void RandOp()
    {
        OpManager.GetInstance.RandOp();
    }

    void RandGlassesOp()
    {
        OpManager.GetInstance.RandGlassesOp();
    }

    private void OnGUI()
    {
        if (_FaceSimulOrGenerate == true)
        {
            int m_slider_row_count = 4;
            for (int i = 0; i < m_mpm_face.GetComponent<PCA_FaceModel>().m_mpm.GetNbOfFaceShapeIdentity(); i++)
            {
                int pos_x = i / m_slider_row_count;
                int pos_y = i % m_slider_row_count;
                m_mpm_face.GetComponent<PCA_FaceModel>().m_face_shape_weight_cover[i] = GUI.HorizontalSlider(new Rect(125 + pos_x * 170, 125 + pos_y * 30, 150, 10), m_mpm_face.GetComponent<PCA_FaceModel>().m_face_shape_weight_cover[i], 0.0f, 1.0f);
            }

            for (int i = 0; i < m_mpm_face.GetComponent<PCA_FaceModel>().m_mpm.GetNbOfNoseIdentity(); i++)
            {
                int pos_x = i / m_slider_row_count;
                int pos_y = i % m_slider_row_count;
                m_mpm_face.GetComponent<PCA_FaceModel>().m_nose_weight_cover[i] = GUI.HorizontalSlider(new Rect(125 + pos_x * 170, 325 + pos_y * 30, 150, 10), m_mpm_face.GetComponent<PCA_FaceModel>().m_nose_weight_cover[i], 0.0f, 1.0f);
            }

            for (int i = 0; i < m_mpm_face.GetComponent<PCA_FaceModel>().m_mpm.GetNbOfEyeIdentity(); i++)
            {
                int pos_x = i / m_slider_row_count;
                int pos_y = i % m_slider_row_count;
                m_mpm_face.GetComponent<PCA_FaceModel>().m_right_eye_weight_cover[i] = GUI.HorizontalSlider(new Rect(125 + pos_x * 170, 525 + pos_y * 30, 150, 10), m_mpm_face.GetComponent<PCA_FaceModel>().m_right_eye_weight_cover[i], 0.0f, 1.0f);
            }

            if (GUI.Toggle(new Rect(160, 100, 150, 20), m_nose_boundary_smooth_or_not, "nose boundary smooth or not") != m_nose_boundary_smooth_or_not)
            {
                m_nose_boundary_smooth_or_not = !m_nose_boundary_smooth_or_not;
            }
        }

        if(_SkinInfoView == true)
        {
            int curr_face_texture_group = m_mpm_face.GetComponent<PCA_FaceModel>()._curr_texture_group_idx;
            int curr_face_texture_file_per_group = m_mpm_face.GetComponent<PCA_FaceModel>()._curr_texture_file_per_group_idx;


            int val = m_mpm_face.GetComponent<PCA_FaceModel>()._skin_annotation[curr_face_texture_group][curr_face_texture_file_per_group];

            string skin_class_label = "Skin Class : " + val.ToString();

            GUI.Label(new Rect(25, 200, 150, 60), skin_class_label);

            UnityEngine.Debug.Log("File Path : " + m_mpm_face.GetComponent<PCA_FaceModel>()._vFaceDiffuseMapWoEyeBrowsFilePath[curr_face_texture_group][curr_face_texture_file_per_group]);
            UnityEngine.Debug.Log("skin_class_label : " + skin_class_label);
        }
    }

    // Update is called once per frame
    void Update()
    {
        _currDeltaTime = _currDeltaTime + 1;
        if (_currDeltaTime > _deltaTime + 1)
        {
            //Camera Focal Length Simulation.... For Test
            //float target_focal_length = 100;
            //GameObject.FindWithTag("MainCamera").GetComponent<Camera>().focalLength = target_focal_length;
            //GameObject.FindWithTag("MainCamera").GetComponent<Camera>().transform.localPosition = new Vector3(0, 0, target_focal_length);
        
            //float time_tot = Time.realtimeSinceStartup;

            _currDeltaTime = 0;

            if (_FaceSimulOrGenerate == true)
            {
                m_mpm_face.GetComponent<PCA_FaceModel>().GenerateFaceWithExpression();
                //Change Eye Position                       
                m_mpm_face.GetComponent<PCA_FaceModel>().ScaleTranslateEyeToFace();
                RandomLight();

                if (_EyeBrowsGenerateOrNot == true)
                {
                    if (m_mpm_face.GetComponent<EyeBrow>().m_rand_or_not == true)
                    {
                        m_mpm_face.GetComponent<EyeBrow>().RandomEyeBrowIndex();
                    }
                    m_mpm_face.GetComponent<EyeBrow>().SetEyeBrowTexture();
                }

                if(_HairExperimentOrNot == true)
                {
                    _hairShowOrNot = true;
                    GameObject.FindGameObjectWithTag("hair").GetComponent<Hair>().SetHair(_HairClass);
                    //UnityEngine.Debug.Log("Current Hair Idx : " + GameObject.FindGameObjectWithTag("hair").GetComponent<hair>().m_hair_file_idx);
                    GameObject.FindGameObjectWithTag("hair").transform.localPosition = new Vector3(0, 0, 0);
                    //m_mpm_face.GetComponent<PCA_FaceModel>().AdjustHairToFace();
                    m_mpm_face.GetComponent<PCA_FaceModel>().AdjustHairToFace_FFD();
                }
            }
            else if(_LandmarkWarpingDatasetGeneration == true)
            {
                RandomFaceShape();
                RandomFaceTexture();
                RandomFaceRotate();

                RandomEye();
                RandomEyeRotate();

                RandomLight();

                RandomHair();
                RandomEyeBrow();

                RandOp();
                RandGlassesOp();

                RandomBackground();
                m_mpm_face.GetComponent<PCA_FaceModel>().highlightSpecificFaceFeature(_ShowFaceFeatureOrNot);

                m_mpm_face.GetComponent<PCA_FaceModel>().GetEyebrowLandmarkVertex(headRotAng.x, headRotAng.y, headRotAng.z);
                m_mpm_face.GetComponent<PCA_FaceModel>().CalcContourFeature_2(headRotAng.x, headRotAng.y, headRotAng.z);
                m_mpm_face.GetComponent<PCA_FaceModel>().setSpecificFaceFeature();

                string neutral_face_save_file_name = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ScreenRecorder>().imgNum.ToString() + "_neutral";
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ScreenRecorder>().CaptureScreen();

                FaceRotateReset();
                NeutralExpressionFaceShape();
                m_mpm_face.GetComponent<PCA_FaceModel>().GetEyebrowLandmarkVertex(0.0f, 0.0f, 0.0f);
                m_mpm_face.GetComponent<PCA_FaceModel>().CalcContourFeature_2(0.0f, 0.0f, 0.0f);
                m_mpm_face.GetComponent<PCA_FaceModel>().setSpecificFaceFeature();
                GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ScreenRecorder>().ScreenAndLandmarkSave(neutral_face_save_file_name);

            }
            else
            {
                float time_elapsed;
                ////////////////////////////////////////////////////
                //Face
                RandomFaceShape();
                RandomFaceTexture();
                RandomFaceRotate();
              
                //////////////////////////////////////////////////// 
                //NOTE : YJ - Ambient Occlusion Simulation

                ////////////////////////////////////////////////////
                //TODO : YJ
                //Change Eye Texture And Shading Map and Parameter

                RandomEye();           
                RandomEyeRotate();
                ////////////////////////////////////////////////////
                //TODO : YJ
                //Change Light Position

                RandomLight();                

                ////////////////////////////////////////////////////
                //Hair
                RandomHair();
                RandomEyeBrow();
                ////////////////////////////////////////////////////
                //
                RandOp();
                RandGlassesOp();
                ////////////////////////////////////////////////////

                RandomBackground();

                if (_FacialLandmarkSaveOrNot == true || _ShowFaceFeatureOrNot == true)
                {
                    m_mpm_face.GetComponent<PCA_FaceModel>().GetEyebrowLandmarkVertex(headRotAng.x, headRotAng.y, headRotAng.z);
                    m_mpm_face.GetComponent<PCA_FaceModel>().CalcContourFeature_2(headRotAng.x, headRotAng.y, headRotAng.z);
                    m_mpm_face.GetComponent<PCA_FaceModel>().setSpecificFaceFeature();
                }
                if (_ShowFaceFeatureOrNot == true)
                {
                    m_mpm_face.GetComponent<PCA_FaceModel>().highlightSpecificFaceFeature(_ShowFaceFeatureOrNot);
                }
                else
                {
                    m_mpm_face.GetComponent<PCA_FaceModel>().highlightSpecificFaceFeature(_ShowFaceFeatureOrNot);
                }

                if(_DataSaveOrNot == true)
                {
                    GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ScreenRecorder>().CaptureScreen();
                }
                //time_elapsed = Time.realtimeSinceStartup - time_tot;
                //UnityEngine.Debug.Log("CaptureScreen Time : " + time_elapsed);
                //time_tot = Time.realtimeSinceStartup;

                // highlightSpecificFaceFeature();   
            }
        }
    }
   
    public Color GetColor()
    { 
        return _rnd_eye_color;
    }

    public void RandomEyeColor()
    {
        float h = GenerateNormalRandom(_eye_mean_h[_EyeClass], _eye_std_h[_EyeClass], _eye_min_h[_EyeClass], _eye_max_h[_EyeClass]);
        float s = GenerateNormalRandom(_eye_mean_s[_EyeClass], _eye_std_s[_EyeClass], _eye_min_s[_EyeClass], _eye_max_s[_EyeClass]);
        float v = GenerateNormalRandom(_eye_mean_v[_EyeClass], _eye_std_v[_EyeClass], _eye_min_v[_EyeClass], _eye_max_v[_EyeClass]);

        _rnd_eye_color = Color.HSVToRGB(h, s, v);

        _eye_pupil_size = GenerateNormalRandom(0.25f, 0.05f, 0f, 0.5f);
        _eye_pupil_rotate = GenerateNormalRandom(0.5f, 0.34167f, 0f, 1.0f);
    }
}