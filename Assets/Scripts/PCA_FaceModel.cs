using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using mpm;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Diagnostics;
using System.Linq;

public class PCA_FaceModel : MonoBehaviour {

    public GameObject target;
    public Texture m_MainTexture, m_Normal, m_Metal;
    Renderer m_renderer;

    public List<GameObject> m_feature_object;

    Texture2D m_diffuse_tex;
    Texture2D m_normal_tex;
    Texture2D m_mask_tex;

    float[] m_face_shape_weight;
    float[] m_nose_weight;
    float[] m_right_eye_weight;

    public float[] m_face_shape_weight_cover;
    public float[] m_nose_weight_cover;
    public float[] m_right_eye_weight_cover;
    public float[] m_expression_blend_weight_cover;

    float[] m_expression_blend_weight_neutral_cover;

    //Map 변수들은 안쓰임... 나중에 범위 설정 직접 하기 위해서 놓음.
    float[] m_face_shape_weight_cover_map;
    float[] m_nose_weight_cover_map;
    float[] m_right_eye_weight_cover_map;
    float[] m_expression_blend_weight_cover_map;

    int m_nb_of_contour_landmark;
    Vector3[] m_base_obj_vertex;
    //float[] m_algorithm_obj_vertex;

    float[] m_transformed_algorithm_obj_vertex;
    float[] m_algorithm_obj_vertex_hair_base_face;

    List<Vector2> m_face_rand_dist_list;
    List<Vector2> m_nose_rand_dist_list;
    List<Vector2> m_eye_rand_dist_list;
    public List<int> m_face_weight_multiplier;
    public List<int> m_nose_weight_multiplier;
    public List<int> m_eye_weight_multiplier;


    List<Tuple<string, string, string>> m_expression_set_list;

    List<string> m_expression_list;
    List<string> m_expression_file_list;
    List<string> m_expression_class_list;
    List<string> m_expression_model_class_list;

    public FaceSyntax m_face_syntax;
    public List<List<float>> m_contour_feature_z_normal_line;

    //public float[] m_contour_feature_vertex;
    List<Vector3> m_contour_feature_vertex;
    Vector3[] m_eyebrow_vertex;

    List<float[]> m_geodesic_contour_feature_path;
    List<List<UnityEngine.Vector3>> m_geodesic_contour_feature_path_vector;

    [NonSerialized] public List<List<string>> _vFaceDiffuseMapFilePath;
    [NonSerialized] public List<List<string>> _vFaceNormalMapFilePath;
    [NonSerialized] public List<List<string>> _vFaceSpecularMapFilePath;

    [NonSerialized] public List<List<string>> _vFaceDiffuseMapWoEyeBrowsFilePath;
    [NonSerialized] public List<List<string>> _vFaceSkinAnnotationFilePath;
    [NonSerialized] public List<List<string>> _vEyeBrowsLandmarkAnnotationFilePath;

    [NonSerialized] Texture2D[][] _vFaceDiffuseTexture;
    [NonSerialized] Texture2D[][] _vFaceDiffuseWOEyeBrowTexture;

    public List<List<int>> _skin_annotation;

    string _FaceDiffuseMapFileName;
    string _FaceNormalMapFileName;
    string _FaceKsFileName;
    List<string> _FaceTextureModelFolderPath;
    List<string> _FaceTextureModelWoEyeBrowsFolderPath;

    public List<string> _FaceTextureModelName;

    List<string> _FaceSkinAnnotationFolderPath;
    List<string> _EyeBrowsLandmarkAnnotationFolderPath;

    public int _curr_texture_group_idx;
    public int _curr_texture_file_per_group_idx;

    int m_nb_of_eyebrows_landmark;
    List<List<Vector2[]>> _vEyeBrowsLandmark;

    string _face_tri_obj_file_path;

    public MPM m_mpm;
    private string m_base_model_path;
    // Use this for initialization
    void Start () {
        //_face_tri_obj_file_path = "Assets/Resources/face/illuni/face_tri.obj";

        m_diffuse_tex = new Texture2D(1, 1);
        m_normal_tex = new Texture2D(1, 1);
        m_mask_tex = new Texture2D(1, 1);

        //Fetch the Renderer from the GameObject
        m_renderer = GetComponent<Renderer>();
        //Make sure to enable the Keywords

        m_renderer.material.EnableKeyword("_BaseColorMap");
        m_renderer.material.EnableKeyword("_NORMALMAP");
        m_renderer.material.EnableKeyword("_MASKMAP");

        m_nb_of_contour_landmark = 17;

        m_geodesic_contour_feature_path = new List<float[]>(m_nb_of_contour_landmark);
        m_geodesic_contour_feature_path_vector = new List<List<Vector3>>(m_nb_of_contour_landmark);
        //for(int i = 0; i < m_nb_of_contour_landmark; m_nb_of_contour_landmark++)
        //{
        //    m_geodesic_contour_feature_path.Add(new float[0]);
        //}

        m_nb_of_eyebrows_landmark = 10;
        m_eyebrow_vertex = new Vector3[m_nb_of_eyebrows_landmark];

        InitPath();
        LoadFaceTexture();
        LoadFaceSkinAnnotation();
        LoadEyeBrowsLandmarkAnnotation();
        ExpressionInfoInit();
        LoadMPM();
        ParameterCoverInitialize();
        SetHairBaseFace();

        VertexSystemManager.GetInstance.RegistGO2VertexSystem(target.GetComponent<MeshFilter>().mesh.vertices, "face", m_base_model_path);

        //GenerateVtToVMap();
        InitFacialFeature();
        ParameterCoverMapInitialize();       
        //HairFFDRegistration();
    }

    void InitPath()
    {
        _FaceTextureModelName = new List<string>();
        //_FaceTextureModelName.Add("Reallusion");
        //_FaceTextureModelName.Add("Kist");
        //_FaceTextureModelName.Add("FaceGen");
        //_FaceTextureModelName.Add("Florence");
        _FaceTextureModelName.Add("ChildFace");

        _FaceTextureModelFolderPath = new List<string>();
        _FaceTextureModelWoEyeBrowsFolderPath = new List<string>();
        _FaceSkinAnnotationFolderPath = new List<string>();
        _EyeBrowsLandmarkAnnotationFolderPath = new List<string>();

        _vFaceDiffuseMapFilePath = new List<List<string>>();
        _vFaceDiffuseMapWoEyeBrowsFilePath = new List<List<string>>();
        _vFaceSkinAnnotationFilePath = new List<List<string>>();
        _vEyeBrowsLandmarkAnnotationFilePath = new List<List<string>>();

        _vFaceNormalMapFilePath = new List<List<string>>();
        _vFaceSpecularMapFilePath = new List<List<string>>();
               
        for (int i = 0; i < _FaceTextureModelName.Count; i++)
        {
            if (_FaceTextureModelName[i] != "ChildFace")
            {
                string path = PathManager.GetInstance._ModelDatabaseFolderPath + "/face/3D/extData_illuni/Ver_2/obj/" + _FaceTextureModelName[i] + "/map";
                _FaceTextureModelFolderPath.Add(path);

                string path_wo_eyebrows = PathManager.GetInstance._ModelDatabaseFolderPath + "/face/3D/extData_illuni/Ver_2/obj/" + _FaceTextureModelName[i] + "/map_wo_eyebrows";
                _FaceTextureModelWoEyeBrowsFolderPath.Add(path_wo_eyebrows);

                string path_skin = PathManager.GetInstance._ModelDatabaseFolderPath + "/face/3D/extData_illuni/Ver_2/obj/" + _FaceTextureModelName[i] + "/skin";
                _FaceSkinAnnotationFolderPath.Add(path_skin);
                
                string path_eyebrows_landmark = PathManager.GetInstance._ModelDatabaseFolderPath + "/face/3D/extData_illuni/Ver_2/obj/" + _FaceTextureModelName[i] + "/landmark";
                _EyeBrowsLandmarkAnnotationFolderPath.Add(path_eyebrows_landmark);
            }
            else
            {
                string path = PathManager.GetInstance._ModelDatabaseFolderPath + "/face/3D/extData_illuni/Ver_2/obj/" + _FaceTextureModelName[i];
                _FaceTextureModelFolderPath.Add(path);

                string path_wo_eyebrows = PathManager.GetInstance._ModelDatabaseFolderPath + "/face/3D/extData_illuni/Ver_2/obj/" + _FaceTextureModelName[i] + "/map_wo_eyebrows";
                _FaceTextureModelWoEyeBrowsFolderPath.Add(path_wo_eyebrows);

                string path_skin = PathManager.GetInstance._ModelDatabaseFolderPath + "/face/3D/extData_illuni/Ver_2/obj/" + _FaceTextureModelName[i] + "/skin";
                _FaceSkinAnnotationFolderPath.Add(path_skin);

                string path_eyebrows_landmark = PathManager.GetInstance._ModelDatabaseFolderPath + "/face/3D/extData_illuni/Ver_2/obj/" + _FaceTextureModelName[i] + "/landmark";
                _EyeBrowsLandmarkAnnotationFolderPath.Add(path_eyebrows_landmark);
            }
        }
    }

    void LoadFaceTexture()
    {
        for (int j = 0; j < _FaceTextureModelFolderPath.Count; j++)
        {
            if (_FaceTextureModelName[j] != "ChildFace")
            {
                string path = _FaceTextureModelFolderPath[j];
                string[] faceDiffusePathArray = Directory.GetFiles(@path, "face_diffuse_*.png");
                string[] faceNormalPathArray = Directory.GetFiles(@path, "face_normal_*.png");
                string[] faceSpecularPathArray = Directory.GetFiles(@path, "face_specular_*.png");

                _vFaceDiffuseMapFilePath.Add(faceDiffusePathArray.ToList());
                _vFaceNormalMapFilePath.Add(faceNormalPathArray.ToList());
                _vFaceSpecularMapFilePath.Add(faceSpecularPathArray.ToList());

                /////////////////////////////
                //WO Eye Brows
                string path_wo_eyebrows = _FaceTextureModelWoEyeBrowsFolderPath[j];
                string[] faceDiffuseWOEyebrowsPathArray = Directory.GetFiles(@path_wo_eyebrows, "face_diffuse_*.png");
                _vFaceDiffuseMapWoEyeBrowsFilePath.Add(faceDiffuseWOEyebrowsPathArray.ToList());
            }
            else
            {
                int nb_of_child_face = 18;

                string path = _FaceTextureModelFolderPath[j];
                string[] faceDiffusePathArray = new string[nb_of_child_face];
                string[] faceNormalPathArray = new string[nb_of_child_face];
                string[] faceSpecularPathArray = new string[nb_of_child_face];

                for (int i = 0; i < nb_of_child_face; i++)
                {
                    string folder_path = _FaceTextureModelFolderPath[j];
                    string file_path = folder_path + "/" + i.ToString("000") + "/face_diffuse.png";
                    faceDiffusePathArray[i] = file_path;
                }

                _vFaceDiffuseMapFilePath.Add(faceDiffusePathArray.ToList());
                _vFaceNormalMapFilePath.Add(faceNormalPathArray.ToList());
                _vFaceSpecularMapFilePath.Add(faceSpecularPathArray.ToList());


                /////////////////////////////
                //WO Eye Brows
                string path_wo_eyebrows = _FaceTextureModelWoEyeBrowsFolderPath[j];
                string[] faceDiffuseWOEyebrowsPathArray = Directory.GetFiles(@path_wo_eyebrows, "face_diffuse_*.png");
                _vFaceDiffuseMapWoEyeBrowsFilePath.Add(faceDiffuseWOEyebrowsPathArray.ToList());
            }
        }

        _vFaceDiffuseTexture = new Texture2D[_vFaceDiffuseMapFilePath.Count][];
        _vFaceDiffuseWOEyeBrowTexture = new Texture2D[_vFaceDiffuseMapFilePath.Count][];

        for (int i = 0; i < _vFaceDiffuseMapFilePath.Count; i++)
        {
            _vFaceDiffuseTexture[i] = new Texture2D[_vFaceDiffuseMapFilePath[i].Count];

            Texture2D[] texture_list = new Texture2D[_vFaceDiffuseMapFilePath[i].Count];
            Texture2D[] texture_wo_eyebrows_list = new Texture2D[_vFaceDiffuseMapFilePath[i].Count];
            for (int j = 0; j < _vFaceDiffuseMapFilePath[i].Count; j++)
            {
                if(!(GameManager.GetInstance._FaceParamDataSetGeneration == true && GameManager.GetInstance._EyeBrowsGenerateOrNot == true))
                {
                    var diffuseBytes = System.IO.File.ReadAllBytes(_vFaceDiffuseMapFilePath[i][j]);
                    Texture2D tex = new Texture2D(1, 1);
                    tex.LoadImage(diffuseBytes);
                    texture_list[j] = tex;
                }

                var diffuseBytes_wo_eyebrows = System.IO.File.ReadAllBytes(_vFaceDiffuseMapWoEyeBrowsFilePath[i][j]);
                Texture2D tex_wo_eyebrows = new Texture2D(1, 1);
                tex_wo_eyebrows.LoadImage(diffuseBytes_wo_eyebrows);
                texture_wo_eyebrows_list[j] = tex_wo_eyebrows;
            }
            _vFaceDiffuseTexture[i] = texture_list;
            _vFaceDiffuseWOEyeBrowTexture[i] = texture_wo_eyebrows_list;
        }
    }

    void LoadFaceSkinAnnotation()
    {
        for (int i = 0; i < _FaceSkinAnnotationFolderPath.Count; i++)
        {
            List<string> file_list = new List<string>();
            for(int j = 0; j < _vFaceDiffuseMapWoEyeBrowsFilePath[i].Count; j++)
            {
                string diffuse_path = _vFaceDiffuseMapWoEyeBrowsFilePath[i][j];               
                string diffuse_name = Path.GetFileNameWithoutExtension(diffuse_path);
                string file_path = _FaceSkinAnnotationFolderPath[i] + "/" + diffuse_name + ".txt";

                file_list.Add(file_path);
            }
            _vFaceSkinAnnotationFilePath.Add(file_list);                
        }
        
        _skin_annotation = new List<List<int>>();
        for (int i = 0; i < _vFaceSkinAnnotationFilePath.Count; i++)
        {
            List<int> data_per_group = new List<int>();
            for (int j = 0; j < _vFaceSkinAnnotationFilePath[i].Count; j++)
            {
                int data_val = new int();
                if (File.Exists(_vFaceSkinAnnotationFilePath[i][j]))
                {
                    string[] data = File.ReadAllLines(_vFaceSkinAnnotationFilePath[i][j]);
                    data_val = Int32.Parse(data[0]);
                    //data_val[1] = Int32.Parse(data[1]);

                    //UnityEngine.Debug.Log("file : " + i + " : " + _vFaceDiffuseMapFilePath[i][j]);
                    //UnityEngine.Debug.Log("skin : " + data_val[0] +  " , "  + data_val[1]);
                    //UnityEngine.Debug.Log("skin : " + data_val[0]);
                }
                data_per_group.Add(data_val);
            }
            _skin_annotation.Add(data_per_group);
        }
        //_skin_annotation
    }



    void LoadEyeBrowsLandmarkAnnotation()
    {
        for (int i = 0; i < _EyeBrowsLandmarkAnnotationFolderPath.Count; i++)
        {
            List<string> file_list = new List<string>();
            for (int j = 0; j < _vFaceDiffuseMapWoEyeBrowsFilePath[i].Count; j++)
            {
                string diffuse_path = _vFaceDiffuseMapWoEyeBrowsFilePath[i][j];
                string diffuse_name = Path.GetFileNameWithoutExtension(diffuse_path);
                string file_path = _EyeBrowsLandmarkAnnotationFolderPath[i] + "/" + diffuse_name + ".txt";

                file_list.Add(file_path);
            }
            _vEyeBrowsLandmarkAnnotationFilePath.Add(file_list);
        }

        //Read
        _vEyeBrowsLandmark = new List<List<Vector2[]>>(_vEyeBrowsLandmarkAnnotationFilePath.Count);

        for (int i = 0; i < _vEyeBrowsLandmarkAnnotationFilePath.Count; i++)
        {
            List<Vector2[]> eye_brows_landmarks = new List<Vector2[]>(_vEyeBrowsLandmarkAnnotationFilePath[i].Count);
            for (int j = 0; j < _vEyeBrowsLandmarkAnnotationFilePath[i].Count; j++)
            {
                Vector2[] eye_brows_landmark = new Vector2[m_nb_of_eyebrows_landmark];
                if (File.Exists(_vEyeBrowsLandmarkAnnotationFilePath[i][j]))
                {
                    Vector2[] landmark = ReadLandmark(_vEyeBrowsLandmarkAnnotationFilePath[i][j]);

                    for(int num = 0; num < m_nb_of_eyebrows_landmark; num++)
                    {
                        eye_brows_landmark[num].x = landmark[num].x;
                        eye_brows_landmark[num].y = landmark[num].y;
                    }
                }
                eye_brows_landmarks.Add(eye_brows_landmark);
            }
            _vEyeBrowsLandmark.Add(eye_brows_landmarks);
        }

        //_skin_annotation = new List<List<int[]>>();
        //for (int i = 0; i < _vFaceSkinAnnotationFilePath.Count; i++)
        //{
        //    List<int[]> data_per_group = new List<int[]>();
        //    for (int j = 0; j < _vFaceSkinAnnotationFilePath[i].Count; j++)
        //    {
        //        int[] data_val = new int[2];
        //        if (File.Exists(_vFaceSkinAnnotationFilePath[i][j]))
        //        {
        //            string[] data = File.ReadAllLines(_vFaceSkinAnnotationFilePath[i][j]);
        //            data_val[0] = Int32.Parse(data[0]);
        //            data_val[1] = Int32.Parse(data[1]);

        //            UnityEngine.Debug.Log("file : " + i + " : " + _vFaceDiffuseMapFilePath[i][j]);
        //            UnityEngine.Debug.Log("skin : " + data_val[0] + " , " + data_val[1]);
        //        }
        //        data_per_group.Add(data_val);
        //    }
        //    _skin_annotation.Add(data_per_group);
        //}
        ////_skin_annotation
    }


    public void ExpressionInfoInit()
    {
        m_expression_set_list = new List<Tuple<string, string, string>>();

        m_expression_set_list.Add(new Tuple<string, string, string>("Neutral", "neutral", "neutral"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Angry", "angry", "emotion"));
        m_expression_set_list.Add(new Tuple<string, string, string>("BigEyes", "big_eyes", "eye"));
        m_expression_set_list.Add(new Tuple<string, string, string>("EyeBrowsRaise", "eyebrows_raise", "eye"));
        m_expression_set_list.Add(new Tuple<string, string, string>("EyeClose", "eye_close", "eye"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Irritation_00", "irritation_00", "emotion"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Jjinggrim", "jjinggrim", "emotion"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Joke_00", "joke_00", "mouth"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Joke_01", "joke_01", "mouth"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Joke_02", "joke_02", "mouth"));
        m_expression_set_list.Add(new Tuple<string, string, string>("LEyebrowRaise", "l_eyebrow_raise", "eye"));
        m_expression_set_list.Add(new Tuple<string, string, string>("REyebrowRaise", "r_eyebrow_raise", "eye"));
        m_expression_set_list.Add(new Tuple<string, string, string>("MouthOpen_00", "mouth_open_00", "mouth"));
        m_expression_set_list.Add(new Tuple<string, string, string>("MouthOpen_01", "mouth_open_01", "mouth"));
        m_expression_set_list.Add(new Tuple<string, string, string>("MouthOpen_02", "mouth_open_02", "mouth"));
        m_expression_set_list.Add(new Tuple<string, string, string>("MouthOpen_03", "mouth_open_03", "mouth"));
        m_expression_set_list.Add(new Tuple<string, string, string>("MouthOpen_04", "mouth_open_04", "mouth"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Proud", "proud", "emotion"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Sad", "sad", "emotion"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Smile_00", "smile_00", "emotion"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Smile_01", "smile_01", "emotion"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Smile_02", "smile_02", "emotion"));
        m_expression_set_list.Add(new Tuple<string, string, string>("Surprise", "surprise", "emotion"));

        m_expression_list = new List<string>();
        m_expression_file_list = new List<string>();
        m_expression_model_class_list = new List<string>();

        for (int i = 0; i < m_expression_set_list.Count; i++)
        {
            m_expression_list.Add(m_expression_set_list[i].Item1);
            m_expression_file_list.Add(m_expression_set_list[i].Item2);
            m_expression_model_class_list.Add(m_expression_set_list[i].Item3);
        }

        m_expression_class_list = new List<string>();

        m_expression_class_list.Add("neutral");
        m_expression_class_list.Add("emotion");
        m_expression_class_list.Add("mouth");
        m_expression_class_list.Add("eye");
        m_expression_class_list.Add("mouth_eye");
    }

    public void LoadMPM()
    {
        m_mpm = new MPM();

        int test_val;
        test_val = m_mpm.GetTestPing();

        string path = Application.streamingAssetsPath;
        string shape_data_path = "E:/Research/Databases/face/3D/illuni/BaseFaceModel/Shape";  // 만약 데이터베이스 경로를 서버-로컬 파일로 하고 싶다면 이걸로.
        //shape_data_path = Application.dataPath + "/Resources";

        string data_path = Application.dataPath + "/Resources/mpm";

        m_base_model_path = data_path + "/baseModel.obj";

        string binaryData_face_path = data_path + "/mpm/pca_Face.bin";
        string binaryData_nose_path = data_path + "/mpm/pca_Nose.bin";
        string binaryData_right_eye_path = data_path + "/mpm/pca_Eye.bin";
        string propertyFaceIndexFilePath = data_path + "/property";
        string expressionDataPath = data_path + "/expression";
        string facialAttributeDataPath = data_path + "/facial_attribute";

        //int res = m_mpm.Init(model_path, binaryData_face_path, binaryData_nose_path, binaryData_right_eye_path, propertyFaceIndexFilePath, expressionDataPath);
        int res = m_mpm.Init_W_Exp(m_base_model_path, binaryData_face_path, binaryData_nose_path, binaryData_right_eye_path, propertyFaceIndexFilePath, facialAttributeDataPath, expressionDataPath, m_expression_list.ToArray(), m_expression_file_list.ToArray());
        UnityEngine.Debug.Log("Init_W_Exp Result : " + res);

        string eye_property_path = shape_data_path + "/Eye/property";
        string base_eye_model_path = shape_data_path + "/Eye/model/eyeLens.obj";

        res = m_mpm.InitEyeMerger(eye_property_path, base_eye_model_path);
        UnityEngine.Debug.Log("InitEyeMerger Result : " + res);


        string teeth_property_path = shape_data_path + "/Teeth/property";
        string base_teeth_top_model_path = shape_data_path + "/Teeth/teethTop_triangle.obj";
        string base_teeth_bottom_model_path = shape_data_path + "/Teeth/teethBottom_triangle.obj";
        string base_tongue_model_path = shape_data_path + "/Teeth/tongue_triangle.obj";

        res = m_mpm.InitTeethMerger(teeth_property_path, base_teeth_top_model_path, base_teeth_bottom_model_path, base_tongue_model_path);

        UnityEngine.Debug.Log("InitTeethMerger Result : " + res);
    }

    public void ParameterCoverMapInitialize()
    {
        int res;
        res = m_mpm.SetParamCoverMapWithConstant(60.0f, 12.0f, 4.0f);

        UnityEngine.Debug.Log("mpm ParameterCoverMapInitialize Result : " + res);
    }
    public void ParameterCoverInitialize()
    {
        m_face_shape_weight_cover_map = new float[m_mpm.GetNbOfFaceShapeIdentity()];
        m_nose_weight_cover_map = new float[m_mpm.GetNbOfNoseIdentity()];
        m_right_eye_weight_cover_map = new float[m_mpm.GetNbOfEyeIdentity()];
        m_expression_blend_weight_cover_map = new float[m_mpm.GetNbOfBlendShapeExpression()];

        m_face_shape_weight_cover = new float[m_mpm.GetNbOfFaceShapeIdentity()];
        m_nose_weight_cover = new float[m_mpm.GetNbOfNoseIdentity()];
        m_right_eye_weight_cover = new float[m_mpm.GetNbOfEyeIdentity()];
        m_expression_blend_weight_cover = new float[m_mpm.GetNbOfBlendShapeExpression()];
        m_expression_blend_weight_neutral_cover = new float[m_mpm.GetNbOfBlendShapeExpression()];
        for (int i = 0; i < m_mpm.GetNbOfFaceShapeIdentity(); i++)
        {
            m_face_shape_weight_cover[i] = 0.0f;
        }
        for (int i = 0; i < m_mpm.GetNbOfNoseIdentity(); i++)
        {
            m_nose_weight_cover[i] = 0.0f;
        }
        for (int i = 0; i < m_mpm.GetNbOfEyeIdentity(); i++)
        {
            m_right_eye_weight_cover[i] = 0.0f;
        }

        m_face_rand_dist_list = new List<Vector2>(m_mpm.GetNbOfFaceShapeIdentity());
        for (int i = 0; i < m_mpm.GetNbOfFaceShapeIdentity(); i++)
        {
            m_face_rand_dist_list.Add(new Vector2(-1.0f, 1.0f));
        }

        m_nose_rand_dist_list = new List<Vector2>(m_mpm.GetNbOfNoseIdentity());
        for (int i = 0; i < m_mpm.GetNbOfNoseIdentity(); i++)
        {
            m_nose_rand_dist_list.Add(new Vector2(-1.0f, 1.0f));
        }

        m_eye_rand_dist_list = new List<Vector2>(m_mpm.GetNbOfEyeIdentity());
        for (int i = 0; i < m_mpm.GetNbOfEyeIdentity(); i++)
        {
            m_eye_rand_dist_list.Add(new Vector2(-1.0f, 1.0f));
        }

        m_eye_rand_dist_list[2] = new Vector2(0.0f, 1.0f);
        m_eye_rand_dist_list[4] = new Vector2(0.0f, 1.0f);
        m_eye_rand_dist_list[5] = new Vector2(0.0f, 1.0f);
        m_eye_rand_dist_list[6] = new Vector2(0.0f, 1.0f);
        m_eye_rand_dist_list[8] = new Vector2(-1.0f, 0.0f);
        m_eye_rand_dist_list[10] = new Vector2(-1.0f, 0.0f);

        ///////////////
        //Set Multiplier. 머신러닝 학습할 때, 0 ~ 1 사이의 아웃풋으로 만들기 위해서 사용됨
        
        m_face_weight_multiplier = new List<int>(m_mpm.GetNbOfFaceShapeIdentity());

        for(int i = 0; i < m_mpm.GetNbOfFaceShapeIdentity(); i++)
        {
            m_face_weight_multiplier.Add(1);
            if (m_face_rand_dist_list[i].y == 0)
            {
                m_face_weight_multiplier[i] = -1;
            }
        }

        m_nose_weight_multiplier = new List<int>(m_mpm.GetNbOfNoseIdentity());

        for (int i = 0; i < m_mpm.GetNbOfNoseIdentity(); i++)
        {
            m_nose_weight_multiplier.Add(1);
            if (m_nose_rand_dist_list[i].y == 0)
            {
                m_nose_weight_multiplier[i] = -1;
            }
        }

        m_eye_weight_multiplier = new List<int>(m_mpm.GetNbOfEyeIdentity());

        for (int i = 0; i < m_mpm.GetNbOfEyeIdentity(); i++)
        {
            m_eye_weight_multiplier.Add(1);
            if (m_eye_rand_dist_list[i].y == 0)
            {
                m_eye_weight_multiplier[i] = -1;
            }
        }

    }

    public void InitFacialFeature()
    {
        m_face_syntax = new FaceSyntax();
        m_face_syntax = new FaceSyntax();
        m_face_syntax.LoadFaceVertexSyntaxFile();
        m_face_syntax.LoadFaceContourLine();
        m_face_syntax.LoadEyeShapeVertexFile();
        
        int offset = 0;
        m_feature_object = new List<GameObject>();
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["leftEye"]].Count; i++, offset++)
        {
            m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 0.0f));
        }
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["rightEye"]].Count; i++, offset++)
        {
            m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 1.0f));
        }
        //for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["leftEyebrow"]].Count; i++, offset++)
        //{
        //    m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
        //    m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //    m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 1.0f));
        //}
        //for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["rightEyebrow"]].Count; i++, offset++)
        //{
        //    m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
        //    m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        //    m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 1.0f));
        //}
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["nose"]].Count; i++, offset++)
        {
            m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 1.0f));
        }
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["lipOuterLine"]].Count; i++, offset++)
        {
            m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 1.0f));
        }
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["lipInnerLine"]].Count; i++, offset++)
        {
            m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 1.0f));
        }
        //for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["jaw"]].Count; i++, offset++)
        for (int i = 0; i < m_nb_of_contour_landmark; i++, offset++)
        {
            m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 1.0f));
        }
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["noseAlae"]].Count; i++, offset++)
        {
            m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 1.0f));
        }

        for (int i = 0; i < m_nb_of_eyebrows_landmark; i++, offset++)
        {
            m_feature_object.Add(GameObject.CreatePrimitive(PrimitiveType.Sphere));
            m_feature_object[offset].transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            m_feature_object[offset].GetComponent<Renderer>().material.SetColor("_BaseColor", new Color(1.0f, 0.0f, 0.0f, 1.0f));
        }

        m_face_syntax.AdjustFaceSyntaxToUnityVertexSystem();

        m_contour_feature_vertex = new List<Vector3>(m_nb_of_contour_landmark);
        for(int i =0; i < m_nb_of_contour_landmark; i++)
        {
            m_contour_feature_vertex.Add(new Vector3(0.0f, 0.0f, 0.0f));
        }
    }
    
    public void InitGeodesicContourFeature()
    {

    }

    void GenerateRandomIdentityParam()
    {
        int face_class_number = UnityEngine.Random.Range(1, 6);
        List<int> face_idxes = new List<int>();
        for (int i = 0; i < face_class_number; i++)
        {
            int face_idx;
            if(UnityEngine.Random.Range(0, 100) < 30)
            {
                face_idx = 4;
            }
            else
                face_idx = UnityEngine.Random.Range(0, m_mpm.GetNbOfFaceShapeIdentity());

            bool exist_or_not = false;
            for (int j = 0; j < face_idxes.Count; j++)
            {
                if (face_idx == face_idxes[j])
                {
                    //이미 등록됨
                    exist_or_not = true;
                    break;
                }
            }
            if (exist_or_not == true)
            {
                i--;
                continue;
            }
            else
            {
                face_idxes.Add(face_idx);
            }
        }

        for (int i = 0; i < m_mpm.GetNbOfFaceShapeIdentity(); i++)
        {
            bool registered_idx = false;
            for (int j = 0; j < face_idxes.Count; j++)
            {
                if (i == face_idxes[j])
                {
                    registered_idx = true;
                }
            }
            if (registered_idx == true)
            {
                m_face_shape_weight_cover[i] = UnityEngine.Random.Range(m_face_rand_dist_list[i].x, m_face_rand_dist_list[i].y);
            }
            else
            {
                m_face_shape_weight_cover[i] = 0.0f;
            }
        }

        //////Normal Distribution!!!

        float norm_sum = 0.0f;
        for (int i = 0; i < m_mpm.GetNbOfFaceShapeIdentity(); i++)
        {
            norm_sum = norm_sum + (float)Math.Sqrt((double)Math.Abs(m_face_shape_weight_cover[i]));
        }
        //norm_sum = (float)Math.Sqrt((double)norm_sum);
        norm_sum = (float)norm_sum;

        for (int i = 0; i < m_mpm.GetNbOfFaceShapeIdentity(); i++)
        {
            m_face_shape_weight_cover[i] = m_face_shape_weight_cover[i] / norm_sum;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////

        int nose_class_number = UnityEngine.Random.Range(1, 3 + 1);
        List<int> nose_idxes = new List<int>();
        for (int i = 0; i < nose_class_number; i++)
        {
            //int nose_idx;
            //if (UnityEngine.Random.Range(0, 100) < 30)
            //{
            //    nose_idx = 4;
            //}
            //else
            //    nose_idx = UnityEngine.Random.Range(0, m_mpm.GetNbOfNoseIdentity());

            int nose_idx = UnityEngine.Random.Range(0, m_mpm.GetNbOfNoseIdentity()); 
    
            bool exist_or_not = false;
            for (int j = 0; j < nose_idxes.Count; j++)
            {
                if (nose_idx == nose_idxes[j])
                {
                    //이미 등록됨
                    exist_or_not = true;
                    break;
                }
            }
            if (exist_or_not == true)
            {
                i--;
                continue;
            }
            else
            {
                nose_idxes.Add(nose_idx);
            }
        }

        for (int i = 0; i < m_mpm.GetNbOfNoseIdentity(); i++)
        {
            bool registered_idx = false;
            for (int j = 0; j < nose_idxes.Count; j++)
            {
                if (i == nose_idxes[j])
                {
                    registered_idx = true;
                }
            }
            if (registered_idx == true)
            {
                m_nose_weight_cover[i] = UnityEngine.Random.Range(m_nose_rand_dist_list[i].x, m_nose_rand_dist_list[i].y);
            }
            else
            {
                m_nose_weight_cover[i] = 0.0f;
            }
        }

        //////Normal Distribution!!!

        norm_sum = 0.0f;
        for (int i = 0; i < m_mpm.GetNbOfNoseIdentity(); i++)
        {
            norm_sum = norm_sum + (float)Math.Sqrt((double)Math.Abs(m_nose_weight_cover[i]));
        }
        //norm_sum = (float)Math.Sqrt((double)norm_sum);
        norm_sum = (float)norm_sum;

        for (int i = 0; i < m_mpm.GetNbOfNoseIdentity(); i++)
        {
            m_nose_weight_cover[i] = m_nose_weight_cover[i] / norm_sum;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////

        int eye_class_number = UnityEngine.Random.Range(1, 4 + 1);
        List<int> eye_idxes = new List<int>();
        for (int i = 0; i < eye_class_number; i++)
        {
            int eye_idx = UnityEngine.Random.Range(0, m_mpm.GetNbOfEyeIdentity());

            bool exist_or_not = false;
            for (int j = 0; j < eye_idxes.Count; j++)
            {
                if (eye_idx == eye_idxes[j])
                {
                    //이미 등록됨
                    exist_or_not = true;
                    break;
                }
            }
            if (exist_or_not == true)
            {
                i--;
                continue;
            }
            else
            {
                eye_idxes.Add(eye_idx);
            }
        }

       // int eye_idx = UnityEngine.Random.Range(0, m_mpm.GetNbOfEyeIdentity());

        for (int i = 0; i < m_mpm.GetNbOfEyeIdentity(); i++)
        {
            bool registered_idx = false;
            for (int j = 0; j < eye_idxes.Count; j++)
            {
                if (i == eye_idxes[j])
                {
                    registered_idx = true;
                }
            }
            if (registered_idx == true)
            {
                m_right_eye_weight_cover[i] = UnityEngine.Random.Range(m_eye_rand_dist_list[i].x, m_eye_rand_dist_list[i].y);
            }
            else
            {
                m_right_eye_weight_cover[i] = 0.0f;
            }
        }
        
        //////Normal Distribution!!!
        
        norm_sum = 0.0f;
        for (int i = 0; i < m_mpm.GetNbOfEyeIdentity(); i++)
        {
            norm_sum = norm_sum + (float)Math.Sqrt((double)Math.Abs(m_right_eye_weight_cover[i]));
        }
        //norm_sum = (float)Math.Sqrt((double)norm_sum);
        norm_sum = (float)norm_sum;

        for (int i = 0; i < m_mpm.GetNbOfEyeIdentity(); i++)
        {
            m_right_eye_weight_cover[i] = m_right_eye_weight_cover[i] / norm_sum;
        }
    }

    void GenerateRandomExpressionParam()
    {
        //Blend Shape Gen

        string expression_name = m_expression_class_list[UnityEngine.Random.Range(0, m_expression_class_list.Count)];

        List<int> expression_idx = new List<int>();
        List<bool> stacked_expression = new List<bool>();

        if (expression_name != "mouth_eye")
        {
            stacked_expression.Add(false);
            expression_idx = new List<int>(new int[1]);
        }
        else if (expression_name == "mouth_eye")
        {
            stacked_expression.Add(false);
            stacked_expression.Add(false);
            expression_idx = new List<int>(new int[2]);
        }

        while (true)
        {
            if (expression_name != "mouth_eye")
            {
                int idx = UnityEngine.Random.Range(0, m_expression_model_class_list.Count);
                if (m_expression_model_class_list[idx] == expression_name)
                {
                    expression_idx[0] = idx;
                    stacked_expression[0] = true;
                    break;
                }
            }
            else if (expression_name == "mouth_eye")
            {
                int idx = UnityEngine.Random.Range(0, m_expression_model_class_list.Count);
                if (m_expression_model_class_list[idx] == "mouth" && stacked_expression[0] == false)
                {
                    expression_idx[0] = idx;
                    stacked_expression[0] = true;
                }
                else if (m_expression_model_class_list[idx] == "eye" && stacked_expression[1] == false)
                {
                    expression_idx[1] = idx;
                    stacked_expression[1] = true;
                }
            }
            bool allProcessedOrNot = true;
            for (int i = 0; i < stacked_expression.Count; i++)
            {
                if (stacked_expression[i] == false)
                    allProcessedOrNot = false;
            }
            if (allProcessedOrNot == true)
            {
                break;
            }
        }

        if (expression_idx.Count == 1 && expression_idx[0] == 0)
        {
            for (int i = 0; i < m_mpm.GetNbOfBlendShapeExpression(); i++)
            {
                m_expression_blend_weight_cover[i] = 0.0f;
            }
        }
        else
        {
            for (int i = 0; i < m_mpm.GetNbOfBlendShapeExpression(); i++)
            {
                bool exist_or_not = false;
                for (int j = 0; j < expression_idx.Count; j++)
                {
                    if (i + 1 == expression_idx[j])
                    {
                        exist_or_not = true;
                        break;

                    }
                }
                if (exist_or_not == true)
                {
                    m_expression_blend_weight_cover[i] = UnityEngine.Random.Range(0.0f, 1.0f);
                }
                else
                {
                    m_expression_blend_weight_cover[i] = 0.0f;

                }
            }
        }
    }

    void ResetExpressionParam()
    {
        for (int i = 0; i < m_mpm.GetNbOfBlendShapeExpression(); i++)
        {
            m_expression_blend_weight_neutral_cover[i] = 0.0f;
        }
    }

    public void GenerateFace()
    {       
        int size_vtx = Marshal.SizeOf(VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0]) * VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex.Length;
        int sizem_face_shape_weight = Marshal.SizeOf(m_face_shape_weight_cover[0]) * m_face_shape_weight_cover.Length;
        int sizem_nose_weight = Marshal.SizeOf(m_nose_weight_cover[0]) * m_nose_weight_cover.Length;
        int size_eye_weight = Marshal.SizeOf(m_right_eye_weight_cover[0]) * m_right_eye_weight_cover.Length;

        m_mpm.GetMPM(VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex, m_face_shape_weight_cover, m_nose_weight_cover, m_right_eye_weight_cover, GameManager.GetInstance.m_nose_boundary_smooth_or_not, 2);

        VertexSystemManager.GetInstance.SetAlgorithmVertexToGameObjectModel("face", target.GetComponent<MeshFilter>().mesh.vertices);
    }

    public void GenerateFaceWithExpression()
    {
        m_mpm.GetMPBM_SpecificExpression(VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex, m_face_shape_weight_cover, m_nose_weight_cover, m_right_eye_weight_cover, m_expression_blend_weight_cover, GameManager.GetInstance.m_nose_boundary_smooth_or_not, 2);
        target.GetComponent<MeshFilter>().mesh.vertices = VertexSystemManager.GetInstance.SetAlgorithmVertexToGameObjectModel("face", VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex);      
    }

    public void GenerateFaceWithExpressionNeutral()  //GenerateFace와 실질적으로 같지만 혹시 몰라서 이걸로 한다 (GenerateFaceWithExpression이 가장 최신이므로)
    {
        m_mpm.GetMPBM_SpecificExpression(VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex, m_face_shape_weight_cover, m_nose_weight_cover, m_right_eye_weight_cover, m_expression_blend_weight_neutral_cover, GameManager.GetInstance.m_nose_boundary_smooth_or_not, 2);
        target.GetComponent<MeshFilter>().mesh.vertices = VertexSystemManager.GetInstance.SetAlgorithmVertexToGameObjectModel("face", VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex);
    }

    public void GenerateRandomFace()
    {
        GenerateRandomIdentityParam();
        GenerateRandomExpressionParam();
        //GenerateFace();
        GenerateFaceWithExpression();
    }

    public void GenerateNeutralExpressionFace()
    {
        //아 이부분 진짜 개막장임. 멀티쓰래드로 저장하다보니, 표정 파람을 0으로 세팅하면, 기존거가 0으로 저장됨
        ResetExpressionParam();
        GenerateFaceWithExpressionNeutral();
    }


    public void ReadRandomImgSetTexture()
    {       
        if (GameManager.GetInstance._EyeBrowsGenerateOrNot == false && GameManager.GetInstance._EyeBrowExperimentOrNot == false)
        {
            int trgModelIdx = UnityEngine.Random.Range(0, _vFaceDiffuseMapFilePath.Count);
            int trgImgIdx = UnityEngine.Random.Range(0, _vFaceDiffuseMapFilePath[trgModelIdx].Count);

            if (System.IO.File.Exists(_vFaceDiffuseMapFilePath[trgModelIdx][trgImgIdx]))
            {
                // Image file exists - load bytes into texture
                var diffuseBytes = System.IO.File.ReadAllBytes(_vFaceDiffuseMapFilePath[trgModelIdx][trgImgIdx]);

                m_diffuse_tex.LoadImage(diffuseBytes);
                //m_renderer.material.SetTexture("_BaseColorMap", m_diffuse_tex);
                m_renderer.material.SetTexture("Texture2D_EE23292E", m_diffuse_tex);
            }
        }
        else
        {
            int trgModelIdx = UnityEngine.Random.Range(0, _vFaceDiffuseMapWoEyeBrowsFilePath.Count);
            int trgImgIdx = UnityEngine.Random.Range(0, _vFaceDiffuseMapWoEyeBrowsFilePath[trgModelIdx].Count);

            if (System.IO.File.Exists(_vFaceDiffuseMapWoEyeBrowsFilePath[trgModelIdx][trgImgIdx]))
            {
                // Image file exists - load bytes into texture
                var diffuseBytes = System.IO.File.ReadAllBytes(_vFaceDiffuseMapWoEyeBrowsFilePath[trgModelIdx][trgImgIdx]);

                m_diffuse_tex.LoadImage(diffuseBytes);
                //m_renderer.material.SetTexture("_BaseColorMap", m_diffuse_tex);
                m_renderer.material.SetTexture("Texture2D_EE23292E", m_diffuse_tex);
            }
        }
    }

    public void RandFaceTextureIndex()
    {
        _curr_texture_group_idx = UnityEngine.Random.Range(0, _vFaceDiffuseMapFilePath.Count);
        _curr_texture_file_per_group_idx = UnityEngine.Random.Range(0, _vFaceDiffuseMapFilePath[_curr_texture_group_idx].Count);
    }

    public void SetFaceTexture()
    {
        if (GameManager.GetInstance._EyeBrowsGenerateOrNot == false && GameManager.GetInstance._EyeBrowExperimentOrNot == false)
        {
            m_renderer.material.SetTexture("Texture2D_EE23292E", _vFaceDiffuseTexture[_curr_texture_group_idx][_curr_texture_file_per_group_idx]);
        }
        else
        {
            m_renderer.material.SetTexture("Texture2D_EE23292E", _vFaceDiffuseWOEyeBrowTexture[_curr_texture_group_idx][_curr_texture_file_per_group_idx]);
        }
    }

    public void SetHairBaseFace()
    {
        //m_mpm.SetHairBaseFace(m_algorithm_obj_vertex_hair_base_face, 3, GameManager.instance._obj_face._mesh.vertexCount);

        //m_mpm.InitHairMerger(2048, 755, 1082, 465, 1281);

        //string hairBaseFaceModelPath = "E:/illuni/research/rp/Assets/Resources/face/illuni/face.obj";

        m_mpm.LoadFFDBaseFace(m_base_model_path);
        //m_mpm.LoadHairBaseFace(hairBaseFaceModelPath);
    }

    public void ScaleTranslateEyeToFace()
    {
        //Calculate x0, y0, z0
        float x1, y1, z1, s1;
        x1 = new float();
        y1 = new float();
        z1 = new float();
        s1 = new float();

        m_mpm.GetEyeScaleTranslationToFitFace(ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0], 3, VertexSystemManager.GetInstance.m_vertex_systems["face"].m_nb_of_vertex, ref x1, ref y1, ref z1, ref s1);

        GameObject.FindGameObjectWithTag("left_eye").transform.localPosition = new Vector3(x1, y1, z1);
        GameObject.FindGameObjectWithTag("right_eye").transform.localPosition = new Vector3(-x1, y1, z1);

        GameObject.FindGameObjectWithTag("left_eye").transform.localScale = new Vector3(s1, s1, s1);
        GameObject.FindGameObjectWithTag("right_eye").transform.localScale = new Vector3(s1, s1, s1);
    }


    public void AdjustHairToFace()
    {
        if (GameObject.Find(GameObject.FindGameObjectWithTag("hair").GetComponent<Hair>().m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.childCount == 0)
        {
            Mesh hair_mesh = GameObject.Find(GameObject.FindGameObjectWithTag("hair").GetComponent<Hair>().m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.GetChild(0).GetComponent<MeshFilter>().mesh;

            int rows_hair = 3;
            int cols_hair = hair_mesh.vertexCount;
            int rows_face = 3;

            Vector3[] hair_data = hair_mesh.vertices;
            Vector3[] hair_data2 = (Vector3[])hair_data.Clone();

            GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_original_prev_hair_vertex = (Vector3[])hair_data.Clone();

            m_mpm.AdjustHairToFace(ref hair_data2[0].x, rows_hair, cols_hair, ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0], rows_face, VertexSystemManager.GetInstance.m_vertex_systems["face"].m_nb_of_vertex);

            hair_mesh.vertices = hair_data2;

            GameObject.Find(GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.GetChild(0).GetComponent<MeshFilter>().mesh = hair_mesh;
        }
        else
        {
            for (int j = 0; j < GameObject.Find(GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.childCount; j++)
            {
                if (GameObject.Find(GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.GetChild(j).name == "skullcap")
                {
                    Mesh haircap_mesh = GameObject.Find(GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.GetChild(j).GetComponent<MeshFilter>().mesh;

                    int rows_hair = 3;
                    int cols_hair = haircap_mesh.vertexCount;
                    int rows_face = 3;

                    Vector3[] hair_data = haircap_mesh.vertices;
                    Vector3[] hair_data2 = (Vector3[])hair_data.Clone();

                    GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_original_prev_haircap_vertex = (Vector3[])hair_data.Clone();

                    m_mpm.AdjustHairToFace(ref hair_data2[0].x, rows_hair, cols_hair, ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0], rows_face, VertexSystemManager.GetInstance.m_vertex_systems["face"].m_nb_of_vertex);

                    haircap_mesh.vertices = hair_data2;

                    GameObject.Find(GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.GetChild(j).GetComponent<MeshFilter>().mesh = haircap_mesh;
                }
                else
                {
                    Mesh hair_mesh = GameObject.Find(GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.GetChild(j).GetComponent<MeshFilter>().mesh;

                    int rows_hair = 3;
                    int cols_hair = hair_mesh.vertexCount;
                    int rows_face = 3;

                    Vector3[] hair_data = hair_mesh.vertices;
                    Vector3[] hair_data2 = (Vector3[])hair_data.Clone();

                    GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_original_prev_hair_vertex = (Vector3[])hair_data.Clone();

                    m_mpm.AdjustHairToFace(ref hair_data2[0].x, rows_hair, cols_hair, ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0], rows_face, VertexSystemManager.GetInstance.m_vertex_systems["face"].m_nb_of_vertex);

                    hair_mesh.vertices = hair_data2;

                    GameObject.Find(GameObject.FindGameObjectWithTag("Hair").GetComponent<Hair>().m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.GetChild(j).GetComponent<MeshFilter>().mesh = hair_mesh;
                }
            }
        }
    }

    public void HairFFDRegistration()
    {       
        for (int i = 0; i < Hair.GetInstance.m_hair_model_names.Count; i++)
        {
            GameObject hair_object = GameObject.Find(Hair.GetInstance.m_hair_model_names[i]).transform.GetChild(0).gameObject;

            int nb_of_hair_child = hair_object.transform.childCount;

            for(int j = 0; j < nb_of_hair_child; j++)
            {
                string hair_name = i.ToString() + j.ToString();  //unique hair name을 만든다. 헤어 하나당, 서브 오브젝트(hair / haircap)이 있을 수 있으므로 이진 단위로 만든다.
                m_mpm.HairFFDRegistration(ref GameObject.Find(Hair.GetInstance.m_hair_model_names[i]).transform.GetChild(0).transform.GetChild(j).GetComponent<MeshFilter>().mesh.vertices[0].x,
                    GameObject.Find(Hair.GetInstance.m_hair_model_names[i]).transform.GetChild(0).transform.GetChild(j).GetComponent<MeshFilter>().mesh.vertexCount,
                    hair_name);
            }
        }
    }

    public void AdjustHairToFace_FFD()
    {
        for(int i = 0; i < GameObject.Find(Hair.GetInstance.m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.childCount; i++)
        {
            Vector3[] hair_mesh = new Vector3[GameObject.Find(Hair.GetInstance.m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshFilter>().mesh.vertexCount];

            string unique_hair_name = Hair.GetInstance._hair_curr_idx.ToString() + i.ToString();
         
            m_mpm.GetHairVertexFromHead(ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0],
            ref hair_mesh[0].x,
            unique_hair_name);         

            GameObject.Find(Hair.GetInstance.m_hair_model_names[Hair.GetInstance._hair_curr_idx]).transform.GetChild(0).transform.GetChild(i).GetComponent<MeshFilter>().mesh.vertices = hair_mesh;
        }
    }


    //////////////////////
    public void OpFFDRegistration()
    {
        for (int i = 0; i < OpManager.GetInstance._opTagNames.Count; i++)
        {
            GameObject op_object = GameObject.FindWithTag(OpManager.GetInstance._opTagNames[i]).gameObject;
            
            if(op_object.GetComponent<MeshFilter>() == true)
            {
                string op_name = OpManager.GetInstance._opTagNames[i] + "_root"; //unique hair name을 만든다. 헤어 하나당, 서브 오브젝트(hair / haircap)이 있을 수 있으므로 이진 단위로 만든다.

                Mesh op_object_mesh = GameObject.FindWithTag(OpManager.GetInstance._opTagNames[i]).gameObject.GetComponent<MeshFilter>().mesh;

                m_mpm.OpFFDRegistration(ref op_object_mesh.vertices[0].x,
                    op_object_mesh.vertexCount,
                    op_name);
            }

            int nb_of_op_child = op_object.transform.childCount;

            for (int j = 0; j < nb_of_op_child; j++)
            {
                string op_name = OpManager.GetInstance._opTagNames[i] + "_" + j.ToString(); //unique hair name을 만든다. 헤어 하나당, 서브 오브젝트(hair / haircap)이 있을 수 있으므로 이진 단위로 만든다.

                Mesh sub_op_object_mesh = GameObject.FindWithTag(OpManager.GetInstance._opTagNames[i]).gameObject.transform.GetChild(j).gameObject.GetComponent<MeshFilter>().mesh;

                m_mpm.OpFFDRegistration(ref sub_op_object_mesh.vertices[0].x,
                    sub_op_object_mesh.vertexCount,
                    op_name);
            }
        }
    }

    public void AdjustOpToFace_FFD()
    {
        if (GameObject.FindWithTag(OpManager.GetInstance._opTagNames[OpManager.GetInstance._currOpIdx]).gameObject.GetComponent<MeshFilter>() == true)
        {
            Vector3[] op_mesh = new Vector3[GameObject.FindWithTag(OpManager.GetInstance._opTagNames[OpManager.GetInstance._currOpIdx]).gameObject.GetComponent<MeshFilter>().mesh.vertexCount];

            string unique_op_name = OpManager.GetInstance._opTagNames[OpManager.GetInstance._currOpIdx] + "_root";

            m_mpm.GetOpVertexFromHead(ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0],
            ref op_mesh[0].x,
            unique_op_name);

            GameObject.FindWithTag(OpManager.GetInstance._opTagNames[OpManager.GetInstance._currOpIdx]).gameObject.GetComponent<MeshFilter>().mesh.vertices = op_mesh;

        }
        for (int i = 0; i < GameObject.FindWithTag(OpManager.GetInstance._opTagNames[OpManager.GetInstance._currOpIdx]).gameObject.transform.childCount; i++)
        {
            //GameObject go = GameObject.FindWithTag(OpManager.GetInstance._opTagNames[OpManager.GetInstance.m_glasses_curr_idx]).gameObject.transform.GetChild(i).gameObject;
            Vector3[] op_mesh = new Vector3[GameObject.FindWithTag(OpManager.GetInstance._opTagNames[OpManager.GetInstance._currOpIdx]).gameObject.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.vertexCount];

            string unique_op_name = OpManager.GetInstance._opTagNames[OpManager.GetInstance._currOpIdx] + "_" + i.ToString();

            m_mpm.GetOpVertexFromHead(ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0],
            ref op_mesh[0].x,
            unique_op_name);

            GameObject.FindWithTag(OpManager.GetInstance._opTagNames[OpManager.GetInstance._currOpIdx]).gameObject.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.vertices = op_mesh;
        }   
    }

    //////////////////////
    public void EyeGlassFFDRegistration()
    {      
        for (int i = 0; i < OpManager.GetInstance._opGlassTagNames.Count; i++)
        {
            GameObject op_object = GameObject.FindWithTag(OpManager.GetInstance._opGlassTagNames[i]).gameObject;

            int nb_of_op_child = op_object.transform.childCount;

            for (int j = 0; j < nb_of_op_child; j++)
            {
                string op_name = OpManager.GetInstance._opGlassTagNames[i] + "_" + j.ToString();  //unique hair name을 만든다. 헤어 하나당, 서브 오브젝트(hair / haircap)이 있을 수 있으므로 이진 단위로 만든다.
               
                Mesh sub_op_object_mesh = GameObject.FindWithTag(OpManager.GetInstance._opGlassTagNames[i]).gameObject.transform.GetChild(j).gameObject.GetComponent<MeshFilter>().mesh;
                                
                m_mpm.EyeGlassFFDRegistration(ref sub_op_object_mesh.vertices[0].x,
                    sub_op_object_mesh.vertexCount,
                    op_name);
            }
        }
    }

    public void AdjustEyeGlassToFace_FFD()
    {      
        for (int i = 0; i < GameObject.FindWithTag(OpManager.GetInstance._opGlassTagNames[OpManager.GetInstance._currGlassOpIdx]).gameObject.transform.childCount; i++)
        {
            //GameObject go = GameObject.FindWithTag(OpManager.GetInstance._opGlassTagNames[OpManager.GetInstance.m_glasses_curr_idx]).gameObject.transform.GetChild(i).gameObject;
            Vector3[] op_mesh = new Vector3[GameObject.FindWithTag(OpManager.GetInstance._opGlassTagNames[OpManager.GetInstance._currGlassOpIdx]).gameObject.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.vertexCount];

            string unique_op_name = OpManager.GetInstance._opGlassTagNames[OpManager.GetInstance._currGlassOpIdx] + "_" + i.ToString();
                    m_mpm.GetEyeGlassVertexFromHead(ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0],
            ref op_mesh[0].x,
            unique_op_name);

            GameObject.FindWithTag(OpManager.GetInstance._opGlassTagNames[OpManager.GetInstance._currGlassOpIdx]).gameObject.transform.GetChild(i).gameObject.GetComponent<MeshFilter>().mesh.vertices = op_mesh;
        }
    }


    //////////////////////

    //OBJ 파일 읽어오던 버젼
    //public void AdjustHairToFace()
    //{
    //    for (int i = 0; i < GameObject.FindGameObjectWithTag("hair").GetComponent<hair>().m_hair_obj_list[GameManager.instance._hair_curr_idx].m_mesh_grp.Count; i++)
    //    {
    //        string name = GameObject.FindGameObjectWithTag("hair").GetComponent<hair>().m_hair_obj_list[GameManager.instance._hair_curr_idx].m_groupNames[i];

    //        if (name.Contains("cap") == true)
    //        {
    //            Mesh hair_mesh = GameObject.FindGameObjectWithTag("hair").transform.Find("skullcap").GetComponent<MeshFilter>().mesh;

    //            int rows_hair = 3;
    //            int cols_hair = hair_mesh.vertexCount;
    //            int rows_face = 3;

    //            Vector3[] hair_data = hair_mesh.vertices;
    //            Vector3[] hair_data2 = (Vector3[])hair_data.Clone();

    //            m_mpm.AdjustHairToFace(ref hair_data2[0].x, rows_hair, cols_hair, ref m_algorithm_obj_vertex[0], rows_face, m_nb_of_vertex);

    //            hair_mesh.vertices = hair_data2;

    //            GameObject.FindGameObjectWithTag("hair").transform.Find("skullcap").GetComponent<MeshFilter>().mesh = hair_mesh;
    //        }
    //        else
    //        {
    //            Mesh hair_mesh = GameObject.FindGameObjectWithTag("hair").transform.Find("hair_obj").GetComponent<MeshFilter>().mesh;

    //            int rows_hair = 3;
    //            int cols_hair = hair_mesh.vertexCount;
    //            int rows_face = 3;

    //            Vector3[] hair_data = hair_mesh.vertices;
    //            Vector3[] hair_data2 = (Vector3[])hair_data.Clone();

    //            m_mpm.AdjustHairToFace(ref hair_data2[0].x, rows_hair, cols_hair, ref m_algorithm_obj_vertex[0], rows_face, m_nb_of_vertex);

    //            hair_mesh.vertices = hair_data2;

    //            GameObject.FindGameObjectWithTag("hair").transform.Find("hair_obj").GetComponent<MeshFilter>().mesh = hair_mesh;
    //        }          
    //    }
    //}

    public void GetTeethOrientation(ref float ty, ref float tz, ref float s, ref float rx)
    {
       int res =  m_mpm.GetTeethOrientation(ref ty, ref tz, ref s, ref rx);
    }

    public void SetTeethOrientation(ref float v_teeth_top_original, ref float v_teeth_top, int v_teeth_top_count, ref float v_teeth_bottom_original, ref float v_teeth_bottom, int v_teeth_bottom_count, ref float v_tongue_original, ref float v_tongue, int v_tongue_count)
    {
        int res = m_mpm.SetTeethOrientation(ref v_teeth_top_original, ref v_teeth_top, v_teeth_top_count, ref v_teeth_bottom_original, ref v_teeth_bottom, v_teeth_bottom_count, ref v_tongue_original, ref v_tongue, v_tongue_count);
    }

    public void setSpecificFaceFeature()
    {
        Vector3[] meshVert = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices;

        int offset = 0;
        
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["leftEye"]].Count; i++, offset++)
        {
            m_feature_object[offset].transform.position = transform.TransformVector(GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["leftEye"]][i]]);
        }
        for (int i = 0; i <m_face_syntax._vertexIdx[m_face_syntax._facePart["rightEye"]].Count; i++, offset++)
        {
            m_feature_object[offset].transform.position = transform.TransformVector(GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["rightEye"]][i]]);
        }
        //for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["leftEyebrow"]].Count; i++, offset++)
        //{
        //    m_feature_object[offset].transform.position = transform.TransformVector(GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["leftEyebrow"]][i]]);
        //}
        //for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["rightEyebrow"]].Count; i++, offset++)
        //{
        //    m_feature_object[offset].transform.position = transform.TransformVector(GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["rightEyebrow"]][i]]);
        //}
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["nose"]].Count; i++, offset++)
        {
            m_feature_object[offset].transform.position = transform.TransformVector(GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["nose"]][i]]);
        }
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["lipOuterLine"]].Count; i++, offset++)
        {
            if (i == 1)
            {
                Vector3 pos = new Vector3();
                pos = (0.3f * GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["lipOuterLine_1st_Itpl"]][0]] +
                       0.7f * GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["lipOuterLine_1st_Itpl"]][1]]);

                m_feature_object[offset].transform.position = transform.TransformVector(pos);
            }
            else if (i == 5)
            {
                Vector3 pos = new Vector3();
                pos = (0.3f * GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["lipOuterLine_5th_Itpl"]][0]] +
                        0.7f * GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["lipOuterLine_5th_Itpl"]][1]]);

                m_feature_object[offset].transform.position = transform.TransformVector(pos);
            }
            else
            {
                m_feature_object[offset].transform.position = transform.TransformVector(GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["lipOuterLine"]][i]]);
            }
        }
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["lipInnerLine"]].Count; i++, offset++)
        {
            m_feature_object[offset].transform.position = transform.TransformVector(GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["lipInnerLine"]][i]]);
        }
       //for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["jaw"]].Count; i++, offset++)
        for (int i = 0; i < m_nb_of_contour_landmark; i++, offset++)
        {
            //m_feature_object[offset].transform.position = transform.TransformVector(GameObject.FindGameObjectWithTag(GameManager.instance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["jaw"]][i]]);
            m_feature_object[offset].transform.position = m_contour_feature_vertex[i];
        }
        for (int i = 0; i < m_face_syntax._vertexIdx[m_face_syntax._facePart["noseAlae"]].Count; i++, offset++)
        {
            m_feature_object[offset].transform.position = transform.TransformVector(GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[m_face_syntax._vertexIdx[m_face_syntax._facePart["noseAlae"]][i]]);
        }

        for (int i = 0; i < m_nb_of_eyebrows_landmark; i++, offset++)
        {
            m_feature_object[offset].transform.position = m_eyebrow_vertex[i];
        }
    }


    public void CalcContourFeature(float headAngleX, float headAngleY, float headAngleZ)
    {
        Mesh mesh = target.GetComponent<MeshFilter>().mesh;

        target.GetComponent<MeshFilter>().mesh.RecalculateNormals();
        target.GetComponent<MeshFilter>().mesh.RecalculateTangents();

        //UnityEngine.Debug.Log("  mesh.normals.Length : " + mesh.normals.Length);
        //UnityEngine.Debug.Log("  mesh.normals : " + mesh.normals[0].x + "   " + mesh.normals[0].y + "   " + mesh.normals[0].z);
        //headRotAng.x = UnityEngine.Random.Range(-30.0f, 30.0f);
        //headRotAng.y = UnityEngine.Random.Range(-40.0f, 40.0f) + 180.0f;
        //headRotAng.z = UnityEngine.Random.Range(-20.0f, 20.0f);

        Quaternion rotAng = Quaternion.Euler(headAngleX, headAngleY, headAngleZ);

        for (int i = 0; i < m_contour_feature_z_normal_line.Count; i++)
        {
            for (int j = 0; j < m_contour_feature_z_normal_line[i].Count; j++)
            {
                Vector3 vertexNormalinWorld = rotAng * mesh.normals[m_face_syntax._contourIdxLine[i][j]];
                m_contour_feature_z_normal_line[i][j] = 1 * vertexNormalinWorld.z; //unity는 z방향이 반대니까
            }
        }

        for (int i = 0; i < m_contour_feature_z_normal_line.Count; i++)
        {
            int minIdx = -1;
            int lastIdx = -1;
            bool prevHideOrNot = true;
            bool changedOrNot = false;

            //다 가려진 경우
            for (int j = 0; j < m_contour_feature_z_normal_line[i].Count; j++)
            {
                if (m_contour_feature_z_normal_line[i][j] >= 0)
                {
                    prevHideOrNot = false;
                    break;
                }
            }
            if (prevHideOrNot != false)
            {
                minIdx = m_contour_feature_z_normal_line[i].Count - 1;
            }
            else
            {
                prevHideOrNot = true;
                for (int j = 0; j < m_contour_feature_z_normal_line[i].Count; j++)
                {
                    //if(m_contour_feature_z_normal_line[i][j] < minZNormalVal && m_contour_feature_z_normal_line[i][j] >= 0)
                    //{
                    //    minZNormalVal = m_contour_feature_z_normal_line[i][j];
                    //    minIdx = j;
                    //}
                    if (m_contour_feature_z_normal_line[i][j] >= 0 && prevHideOrNot == true)
                    {
                        prevHideOrNot = false;
                        minIdx = j;
                        lastIdx = minIdx;
                        changedOrNot = true;
                    }
                    else if (m_contour_feature_z_normal_line[i][j] < 0 && prevHideOrNot == false)
                    {
                        prevHideOrNot = true;
                        minIdx = j - 1;
                    }
                    else if (m_contour_feature_z_normal_line[i][j] < 0 && prevHideOrNot == true)
                    {
                        prevHideOrNot = true;
                    }
                    else // if (m_contour_feature_z_normal_line[i][j] >= 0 && prevHideOrNot == false) //  if (m_contour_feature_z_normal_line[i][j] < 0 && prevHideOrNot == true)
                    {
                        prevHideOrNot = false;
                    }
                }
            }

            if (changedOrNot == true)
            {
                m_face_syntax._vertexIdx[m_face_syntax._facePart["jaw"]][i] = m_face_syntax._contourIdxLine[i][minIdx];
            }
            else
            {
                m_face_syntax._vertexIdx[m_face_syntax._facePart["jaw"]][i] = m_face_syntax._contourIdxLine[i][m_face_syntax._contourIdxLine[i].Count - 1];
            }
        }
    }

    //Using Geodesic. 현재 문제점 : Orthogonal Projection에서는 완벽한데, Perspective Projection에서는 문제가 있다.
    public void CalcContourFeature_2(float headAngleX, float headAngleY, float headAngleZ)
    {
        Quaternion rotAng = Quaternion.Euler(headAngleX, headAngleY, headAngleZ);
        Mesh mesh = target.GetComponent<MeshFilter>().mesh;
        target.GetComponent<MeshFilter>().mesh.RecalculateNormals();
        target.GetComponent<MeshFilter>().mesh.RecalculateTangents();
        
        m_mpm.CalcGeodesicCurveAndNeighborhoodVertexID(ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0]);

        m_geodesic_contour_feature_path.Clear();
        m_geodesic_contour_feature_path_vector.Clear();
        for (int i = 0; i < m_nb_of_contour_landmark; i++)
        {
            int size_0 = m_mpm.GetGeodesicNeighborhoodSize_0(i);            
            m_geodesic_contour_feature_path.Add(new float[size_0 * 3]);

            int size = new int();
            m_mpm.GetGeodesicPath(i, ref m_geodesic_contour_feature_path[i][0], ref size);

            List<UnityEngine.Vector3> geodesicPath = new List<UnityEngine.Vector3>();
            for (int j = 0; j < size_0; j++)
            {               
                Vector3 pos = new Vector3();
                pos.x = -m_geodesic_contour_feature_path[i][3 * j + 0];
                pos.y = m_geodesic_contour_feature_path[i][3 * j + 1];
                pos.z = m_geodesic_contour_feature_path[i][3 * j + 2];

                geodesicPath.Add(pos);
            }
            m_geodesic_contour_feature_path_vector.Add(geodesicPath);
        }
        
        for (int i = 0; i < m_nb_of_contour_landmark; i++)
        {
            int size_0 = m_mpm.GetGeodesicNeighborhoodSize_0(i);

            bool frontViewOrNot = true;
            int pointIdx = -1;

            for (int j = size_0 - 1; j >= 0; j--)
            {
                int size_1 = m_mpm.GetGeodesicNeighborhoodSize_1(i, j);
                //calc normal
                //check whether normal is negative or not			
                for (int k = 0; k < size_1; k++)
                {
                    int vtxId_0 = m_mpm.GetGeodesicNeighborhoodVertexID(i, j, k, 0);
                    int vtxId_1 = m_mpm.GetGeodesicNeighborhoodVertexID(i, j, k, 1);
                    int vtxId_2 = m_mpm.GetGeodesicNeighborhoodVertexID(i, j, k, 2);

                    int vtxId_0_unity = (int)VertexSystemManager.GetInstance.m_vertex_systems["face"].m_v_to_vt[vtxId_0].y;
                    int vtxId_1_unity = (int)VertexSystemManager.GetInstance.m_vertex_systems["face"].m_v_to_vt[vtxId_1].y;
                    int vtxId_2_unity = (int)VertexSystemManager.GetInstance.m_vertex_systems["face"].m_v_to_vt[vtxId_2].y;
                    
                    Vector3 vertexNormalinWorld_0 = rotAng * mesh.normals[vtxId_0_unity];
                    Vector3 vertexNormalinWorld_1 = rotAng * mesh.normals[vtxId_1_unity];
                    Vector3 vertexNormalinWorld_2 = rotAng * mesh.normals[vtxId_2_unity];

                    bool currFrontViewOrNot = true;
                    if (vertexNormalinWorld_0.z < 0 || vertexNormalinWorld_1.z < 0 || vertexNormalinWorld_2.z < 0)
//                    if (!(vertexNormalinWorld_0.z < 0 || vertexNormalinWorld_1.z < 0 || vertexNormalinWorld_2.z < 0))
                    {
                        currFrontViewOrNot = false;
                    }
                    ////std::cout << faceId << std::endl;
                    ////std::cout << currFrontViewOrNot << std::endl;
                    if (currFrontViewOrNot == false)
                    {
                        frontViewOrNot = false;
                    }
                }
                if (frontViewOrNot == false)
                {
                    pointIdx = j;
                    break;
                }
            }

            if (frontViewOrNot == true)
            {
                pointIdx = 0;
            }

            m_contour_feature_vertex[i] = rotAng * m_geodesic_contour_feature_path_vector[i][pointIdx];
        
            //m_contourVertex[i] = cv::Mat(3, 1, CV_32FC1);
            //m_contourVertex[i].at<float>(0, 0) = m_geodesicContourPath[i].at<float>(0, pointIdx);
            //m_contourVertex[i].at<float>(1, 0) = m_geodesicContourPath[i].at<float>(1, pointIdx);
            //m_contourVertex[i].at<float>(2, 0) = m_geodesicContourPath[i].at<float>(2, pointIdx);
        }

        //for (int i = 0; i < m_nb_of_vertex; i++)
        //{
        //    Vector3 point = new Vector3();
        //    point.x = m_algorithm_obj_vertex[3 * i + 0];
        //    point.y = m_algorithm_obj_vertex[3 * i + 1];
        //    point.z = m_algorithm_obj_vertex[3 * i + 2];

        //    point = rotAng * point;

        //    m_transformed_algorithm_obj_vertex[3 * i + 0] = point.x;
        //    m_transformed_algorithm_obj_vertex[3 * i + 1] = point.x;
        //    m_transformed_algorithm_obj_vertex[3 * i + 2] = point.z;
        //}
        //m_mpm.GetContourVertex(ref m_transformed_algorithm_obj_vertex[0], ref m_contour_feature_vertex[0]);
    }

    public void TransformAlgorithmVertex()
    {
        
    }
    
    public void highlightSpecificFaceFeature(bool showOrNot)
    { 
        for (int i = 0; i < m_feature_object.Count; i++)
        {
            //m_feature_object[i].SetActive(showOrNot);
            m_feature_object[i].GetComponent<MeshRenderer>().enabled = showOrNot;
        }         
    }

    public void GenerateCurrentFaceModelFromPCAFacePart()
    {
        
    }
    
    Vector2[] ReadLandmark(string file_path)
    {       
        string[] data = File.ReadAllLines(file_path);

        string[] temp = data[1].Split(' ');

        int nb_of_landmark = Int32.Parse(temp[1]);

        Vector2[] landmark = new Vector2[nb_of_landmark];

        for(int i = 0; i < nb_of_landmark; i++)
        { 
            string[] temp_str = data[3 + i].Split(' ');
            float[] data_val = new float[2];
            data_val[0] = float.Parse(temp_str[0]);
            data_val[1] = float.Parse(temp_str[1]);

            landmark[i].x = data_val[0];
            landmark[i].y = data_val[1];
        }
        return landmark;
    }

    public void GetEyebrowLandmarkVertex(float headAngleX, float headAngleY, float headAngleZ)
    {
        Quaternion rotAng = Quaternion.Euler(headAngleX, headAngleY, headAngleZ);
        Vector2[] eyebrow_landmark = _vEyeBrowsLandmark[_curr_texture_group_idx][_curr_texture_file_per_group_idx];
        m_mpm.GetVertexFromUV(ref eyebrow_landmark[0].x, m_nb_of_eyebrows_landmark, ref VertexSystemManager.GetInstance.m_vertex_systems["face"].m_algorithm_obj_vertex[0], ref m_eyebrow_vertex[0].x);

        for(int i =0; i < m_nb_of_eyebrows_landmark; i++)
        {
            m_eyebrow_vertex[i].x = -m_eyebrow_vertex[i].x; // Unity의 x 좌표계가 정반대라서 (정확히는 z 방향이 반대인데, 카메라를 회전시켜버림)
            m_eyebrow_vertex[i] = rotAng * m_eyebrow_vertex[i];
        }
    }

    // Update is called once per frame
    void Update () {
    }

}