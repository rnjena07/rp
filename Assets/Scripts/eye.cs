﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Runtime.InteropServices;
using System.Text;

public class Eye : MonoBehaviour {

    public GameObject target;
    public Mesh mesh;
    public Vector3 m_eye_center;
    public Vector3 m_eye_tip;

    public Vector3 m_eye_anchor_center;

    public List<int> m_eye_anchor_line_vertex;
    

    Renderer m_eyeball_renderer;
    Renderer m_eyelens_renderer;
      
    //Renderer m_Renderer;

    Texture2D m_diffuse_tex;
    Texture2D m_normal_tex;

    float m_eyeball_normal_tex;
    float m_eyeball_coat_mask;
    float m_eyelens_metallic;
    float m_eyelens_smoothness;
    float m_eyelens_coat_mask;

    // Use this for initialization
    void Start () {
         
        m_diffuse_tex = new Texture2D(1, 1);
        m_normal_tex = new Texture2D(1, 1);
   

        string tag = target.tag;
        m_eye_anchor_line_vertex = new List<int>();

        if (tag == "left_eye")
        {
            mesh = GameObject.FindGameObjectWithTag(GameManager.GetInstance._lEyeBallTag).GetComponent<MeshFilter>().mesh;
            m_eyeball_renderer = GameObject.FindGameObjectWithTag(GameManager.GetInstance._lEyeBallTag).GetComponent<Renderer>();
            m_eyelens_renderer = GameObject.FindGameObjectWithTag(GameManager.GetInstance._lEyeLensTag).GetComponent<Renderer>();
        }
        else if (tag == "right_eye")
        {
            mesh = GameObject.FindGameObjectWithTag(GameManager.GetInstance._rEyeBallTag).GetComponent<MeshFilter>().mesh;
            m_eyeball_renderer = GameObject.FindGameObjectWithTag(GameManager.GetInstance._rEyeBallTag).GetComponent<Renderer>();
            m_eyelens_renderer = GameObject.FindGameObjectWithTag(GameManager.GetInstance._rEyeLensTag).GetComponent<Renderer>();
        }
        
        //Make sure to enable the Keywords
        m_eyeball_renderer.material.EnableKeyword("_NormalMap");

        //Translate To EyeAnchorCenter
    }


    public void SetTexture(Texture2D tex)
    {
        m_eyeball_renderer.material.SetTexture("Texture2D_C1944EB2", tex);
    }
    
    //public void ReadRandomImgSetTexture()
    //{
    //    int trgImgIdx = GameManager.GetInstance._EyeDiffuseMapFileIdx;

    //    if (System.IO.File.Exists(GameManager.GetInstance._vEyeDiffuseMapFilePath[trgImgIdx]))
    //    {
    //        // Image file exists - load bytes into texture
    //        var diffuseBytes = System.IO.File.ReadAllBytes(GameManager.GetInstance._vEyeDiffuseMapFilePath[trgImgIdx]);
    //        //var m_diffuse_tex = new Texture2D(1, 1);
    //        m_diffuse_tex.LoadImage(diffuseBytes);

    //        var normalBytes = System.IO.File.ReadAllBytes(GameManager.GetInstance._vEyeNormalMapFilePath[trgImgIdx]);
    //        //var m_normal_tex = new Texture2D(1, 1);
    //        m_normal_tex.LoadImage(normalBytes);

    //        //m_Renderer.material.mainTexture = tex;
    //        m_eyeball_renderer.material.SetTexture("_BaseColorMap", m_diffuse_tex);
    //        m_eyeball_renderer.material.SetTexture("_NORMALMAP", m_normal_tex);

    //        m_eyeball_normal_tex = UnityEngine.Random.Range(0.8f, 2.0f);
    //        m_eyeball_renderer.material.SetFloat("_NormalScale", m_eyeball_normal_tex);

    //        m_eyeball_coat_mask = UnityEngine.Random.Range(0.01f, 0.2f);
    //        m_eyeball_renderer.material.SetFloat("m_eyeball_coat_mask", m_eyeball_coat_mask);

    //        ////////////////////////////////////////////////////////////////////////////
    //        m_eyelens_metallic = UnityEngine.Random.Range(0.0f, 0.2f);
    //        m_eyelens_smoothness = UnityEngine.Random.Range(0.0f, 0.3f);
    //        m_eyelens_coat_mask = UnityEngine.Random.Range(0.0f, 0.2f);
    //        //m_eyelens_renderer.material.SetFloat("_Metallic", m_eyelens_metallic);
    //        //m_eyelens_renderer.material.SetFloat("_Smoothness", m_eyelens_smoothness);
    //        m_eyelens_renderer.material.SetFloat("_CoatMask", m_eyelens_coat_mask);


    //        //  faceTexture.mainTexture = tex;

    //        // Apply to Plane
    //        //  MeshRenderer mr = target.GetComponent<MeshRenderer>();
    //        //  mr.material = faceTexture;
    //    }
    //}

     public void SetColorViaShader(Color color)
    {
        //float h = UnityEngine.Random.Range(0.0f, 1.0f);
        //float s = UnityEngine.Random.Range(0.0f, 1.0f);
        //float v = UnityEngine.Random.Range(0.5f, 1.0f);
               
        //Color rgbEyeCol = Color.HSVToRGB(h, s, v);

        m_eyeball_renderer.material.SetColor("Color_A34CD6CF", color);
    }

    public void SetPupilSizeViaShader(float size, float rotate)
    {
        //float h = UnityEngine.Random.Range(0.0f, 1.0f);
        //float s = UnityEngine.Random.Range(0.0f, 1.0f);
        //float v = UnityEngine.Random.Range(0.5f, 1.0f);

        //Color rgbEyeCol = Color.HSVToRGB(h, s, v);

        m_eyeball_renderer.material.SetFloat("Vector1_6B87C648", size);
        m_eyeball_renderer.material.SetFloat("Vector1_A81F6A72", rotate);
    }

    // Update is called once per frame
    void Update () {
     
    }
}
