using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

// Screen Recorder will save individual images of active scene in any resolution and of a specific image format
// including raw, jpg, png, and ppm.  Raw and PPM are the fastest image formats for saving.
//
// You can compile these images into a video using ffmpeg:
// ffmpeg -i screen_3840x2160_%d.ppm -y test.avi

public class ScreenRecorder : MonoBehaviour
{
    // 4k = 3840 x 2160   1080p = 1920 x 1080
    public int captureWidth = 960;
    public int captureHeight = 540;

    // optional game object to hide during screenshots (usually your scene canvas hud)
    public GameObject hideGameObject;

    // optimize for many screenshots will not destroy any objects so future screenshots will be fast
    public bool optimizeForManyScreenshots = true;

    // configure with raw, jpg, png, or ppm (simple raw format)
    public enum Format { RAW, JPG, PNG, PPM };
    public Format format = Format.PNG;

    // folder to write output (defaults to data path)
    public string folder;

    // private vars for screenshot
    private Rect rect;
    private RenderTexture renderTexture;
    private Texture2D screenShot;
    private int counter = 0; // image #

    // commands
    private bool captureScreenshot = false;
    private bool captureVideo = false;

    public int imgNum;
    private string textFileName;

    private string dbImageFolderPath;
    private string dbLandmarkFolderPath;
    private string dbParameterFolderPath;
    private string dbEyeFolderPath;
    private string dbOpFolderPath;

    void Start()
    {
        imgNum = PathManager.GetInstance._StartImageIndex;

        dbImageFolderPath = PathManager.GetInstance._SaveDatabaseFolderPath + "/image";
        dbLandmarkFolderPath = PathManager.GetInstance._SaveDatabaseFolderPath + "/landmark";
        dbParameterFolderPath = PathManager.GetInstance._SaveDatabaseFolderPath + "/facial_parameter";
        dbEyeFolderPath = PathManager.GetInstance._SaveDatabaseFolderPath + "/eye";
        dbOpFolderPath = PathManager.GetInstance._SaveDatabaseFolderPath + "/op";

        if (Directory.Exists(PathManager.GetInstance._SaveDatabaseFolderPath) == false)
        {
            Directory.CreateDirectory(PathManager.GetInstance._SaveDatabaseFolderPath);
        }

        string saveDatabaseFolderPath = PathManager.GetInstance._SaveDatabaseFolderPath.Replace("\\", "/");
        string[] dbFolderSubset = saveDatabaseFolderPath.Split('/');

        string currFolder = dbFolderSubset[0];
        for (int i = 0; i < dbFolderSubset.Length - 1; i++)
        {
            currFolder = currFolder + "/" + dbFolderSubset[i + 1];
            if (Directory.Exists(currFolder) == false)
            {
                Directory.CreateDirectory(currFolder);
            }            
        }

        //if (Directory.Exists(PathManager.GetInstance._DatabaseFolderPath + "/face/2D") == false)
        //{
        //    Directory.CreateDirectory(PathManager.GetInstance._DatabaseFolderPath + "/face/2D");
        //}

        //if (Directory.Exists(PathManager.GetInstance._DatabaseFolderPath + "/face/2D/generated_dataset") == false)
        //{
        //    Directory.CreateDirectory(PathManager.GetInstance._DatabaseFolderPath + "/face/2D/generated_dataset");
        //}

        //if (Directory.Exists(PathManager.GetInstance._DatabaseFolderPath + "/face/2D/generated_dataset/illuni") == false)
        //{
        //    Directory.CreateDirectory(PathManager.GetInstance._DatabaseFolderPath + "/face/2D/generated_dataset/illuni");
        //}

        if (Directory.Exists(dbImageFolderPath) == false)
        {
            Directory.CreateDirectory(dbImageFolderPath);
        }

        if (Directory.Exists(dbLandmarkFolderPath) == false)
        {
            Directory.CreateDirectory(dbLandmarkFolderPath);
        }

        if (Directory.Exists(dbParameterFolderPath) == false)
        {
            Directory.CreateDirectory(dbParameterFolderPath);
        }

        if (Directory.Exists(dbEyeFolderPath) == false)
        {
            Directory.CreateDirectory(dbEyeFolderPath);
        }

        if (Directory.Exists(dbOpFolderPath) == false)
        {
            Directory.CreateDirectory(dbOpFolderPath);
        }
    }

// create a unique filename using a one-up variable
private string uniqueFilename(int width, int height)
    {
        // if folder not specified by now use a good default
        if (folder == null || folder.Length == 0)
        {
            folder = Application.dataPath;
            if (Application.isEditor)
            {
                // put screenshots in folder above asset path so unity doesn't index the files
                var stringPath = folder + "/..";
                folder = Path.GetFullPath(stringPath);
            }
            folder += "/screenshots";

            // make sure directoroy exists
            System.IO.Directory.CreateDirectory(folder);

            // count number of files of specified format in folder
            string mask = string.Format("screen_{0}x{1}*.{2}", width, height, format.ToString().ToLower());
            counter = Directory.GetFiles(folder, mask, SearchOption.TopDirectoryOnly).Length;
        }

        // use width, height, and counter for unique file name
        var filename = string.Format("{0}/screen_{1}x{2}_{3}.{4}", folder, width, height, imgNum, format.ToString().ToLower());
        textFileName = string.Format("{0}/screen_{1}x{2}_{3}.txt", folder, width, height, imgNum);
        // up counter for next call
        counter++;
        imgNum++;
        // return unique filename
        return filename;
    }

    public void CaptureScreenshot()
    {
        captureScreenshot = true;
    }

    public void CaptureScreen()
    {
        // hide optional game object if set
        if (hideGameObject != null) hideGameObject.SetActive(false);

        Camera camera = this.GetComponent<Camera>(); // NOTE: added because there was no reference to camera in original script; must add this script to Camera

        captureWidth = camera.pixelWidth;
        captureHeight = camera.pixelHeight;

        // create screenshot objects if needed
        if (renderTexture == null)
        {
            // creates off-screen render texture that can rendered into
            rect = new Rect(0, 0, captureWidth, captureHeight);
            renderTexture = new RenderTexture(captureWidth, captureHeight, 24);
            screenShot = new Texture2D(captureWidth, captureHeight, TextureFormat.RGB24, false);
        }

        // get main camera and manually render scene into rt
        camera.targetTexture = renderTexture;
        camera.Render();

        // read pixels will read from the currently active render texture so make our offscreen 
        // render texture active and then read the pixels
        RenderTexture.active = renderTexture;
        screenShot.ReadPixels(rect, 0, 0);

        // reset active camera texture and render texture
        camera.targetTexture = null;
        RenderTexture.active = null;

        //file name
        string file_name = PathManager.GetInstance._DeviceUniqueName + "_" + imgNum.ToString();

        // get our unique filename
        string imgFilePath = string.Format("{0}/{1}.{2}", dbImageFolderPath, file_name, format.ToString().ToLower());
        string landmarkFilePath = string.Format("{0}/{1}.txt", dbLandmarkFolderPath, file_name);
        string parameterFilePath = string.Format("{0}/{1}.txt", dbParameterFolderPath, file_name);
        string eyeFilePath = string.Format("{0}/{1}.txt", dbEyeFolderPath, file_name);
        string opFilePath = string.Format("{0}/{1}.txt", dbOpFolderPath, file_name);

        // pull in our file header/data bytes for the specified image format (has to be done from main thread)
        byte[] fileHeader = null;
        byte[] fileData = null;
        if (format == Format.RAW)
        {
            fileData = screenShot.GetRawTextureData();
        }
        else if (format == Format.PNG)
        {
            fileData = screenShot.EncodeToPNG();
        }
        else if (format == Format.JPG)
        {
            fileData = screenShot.EncodeToJPG();
        }
        else // ppm
        {
            // create a file header for ppm formatted file
            string headerStr = string.Format("P6\n{0} {1}\n255\n", rect.width, rect.height);
            fileHeader = System.Text.Encoding.ASCII.GetBytes(headerStr);
            fileData = screenShot.GetRawTextureData();
        }

        List<GameObject> featureObject = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_feature_object;
        List<Vector3> featureObjectPosition = new List<Vector3>(featureObject.Count);

        for (int i = 0; i < featureObject.Count; i++)
        {
            Vector3 ptInWorld = camera.WorldToScreenPoint(featureObject[i].transform.localPosition);
            Vector3 ptInWorldRescaled = new Vector3();

            ptInWorldRescaled.x = ptInWorld.x * (float)(captureWidth) / (float)camera.pixelWidth;
            ptInWorldRescaled.y = (float)(captureHeight) - ptInWorld.y * (float)(captureHeight) / (float)camera.pixelHeight - 1;
            ptInWorldRescaled.z = ptInWorld.z;

            featureObjectPosition.Add(ptInWorldRescaled);
        }

        if (imgNum == 0)
        {
            List<int> face_weight_multiplier = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_face_weight_multiplier;
            List<int> nose_weight_multiplier = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_nose_weight_multiplier;
            List<int> eye_weight_multiplier = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_eye_weight_multiplier;

            string parameterMultiplierFilePath = string.Format("{0}/_mul.txt", dbParameterFolderPath);
            new System.Threading.Thread(() =>
            {
                //Save Text File
                var paramWriter = File.CreateText(parameterMultiplierFilePath);
                //////////////////////////////////////////////////////////////////////////////

                paramWriter.WriteLine("Identity Face Parameter Multiplier");
                paramWriter.WriteLine(face_weight_multiplier.Count);
                for (int i = 0; i < face_weight_multiplier.Count; i++)
                {
                    paramWriter.WriteLine(face_weight_multiplier[i]);
                }

                paramWriter.WriteLine("Identity Nose Parameter Multiplier");
                paramWriter.WriteLine(nose_weight_multiplier.Count);
                for (int i = 0; i < nose_weight_multiplier.Count; i++)
                {
                    paramWriter.WriteLine(nose_weight_multiplier[i]);
                }

                paramWriter.WriteLine("Identity Eye Parameter Multiplier");
                paramWriter.WriteLine(eye_weight_multiplier.Count);
                for (int i = 0; i < eye_weight_multiplier.Count; i++)
                {
                    paramWriter.WriteLine(eye_weight_multiplier[i]);
                }
                paramWriter.Close();
            }).Start();
        }

        float[] face_shape_weight_cover = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_face_shape_weight_cover;
        float[] nose_weight_cover = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_nose_weight_cover;
        float[] eye_weight_cover = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_right_eye_weight_cover;
        float[] expression_weight_cover = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_expression_blend_weight_cover;


        int curr_face_texture_group = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>()._curr_texture_group_idx;
        int curr_face_texture_file_per_group = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>()._curr_texture_file_per_group_idx;

        int skin_annotation = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>()._skin_annotation[curr_face_texture_group][curr_face_texture_file_per_group];
        string face_texture_group = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>()._FaceTextureModelName[curr_face_texture_group];
      
        int eyebrow_class = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<EyeBrow>().m_curr_class_of_eyebrow;

        // create new thread to save the image to file (only operation that can be done in background)
        new System.Threading.Thread(() =>
        {
            // create file and write optional header with image bytes
            var f = System.IO.File.Create(imgFilePath);
            if (fileHeader != null) f.Write(fileHeader, 0, fileHeader.Length);
            f.Write(fileData, 0, fileData.Length);
            f.Close();
            Debug.Log(string.Format("Wrote screenshot {0} of size {1}", imgFilePath, fileData.Length));
            
            if (GameManager.GetInstance._FacialLandmarkSaveOrNot == true)
            {
                //Save Text File
                var writer = File.CreateText(landmarkFilePath);
                //////////////////////////////////////////////////////////////////////////////
                for (int i = 0; i < featureObjectPosition.Count; i++)
                {
                    writer.WriteLine(featureObjectPosition[i].x + "  " + featureObjectPosition[i].y + "  " + featureObjectPosition[i].z);
                }
                writer.Close();
            }

            //Save Text File
            var paramWriter = File.CreateText(parameterFilePath);
            //////////////////////////////////////////////////////////////////////////////

            float rotAngX = GameManager.GetInstance.headRotAng.x * 3.14159265358979323846264338327950288f / 180.0f;
            float rotAngY = GameManager.GetInstance.headRotAng.y * 3.14159265358979323846264338327950288f / 180.0f;
            float rotAngZ = GameManager.GetInstance.headRotAng.z * 3.14159265358979323846264338327950288f / 180.0f;
            
            paramWriter.WriteLine("Pose");
            paramWriter.WriteLine(rotAngX);
            paramWriter.WriteLine(rotAngY);
            paramWriter.WriteLine(rotAngZ);

            paramWriter.WriteLine("Identity Face Parameter");
            paramWriter.WriteLine(face_shape_weight_cover.GetLength(0));
            for (int i = 0; i < face_shape_weight_cover.GetLength(0); i++)
            {
                paramWriter.WriteLine(face_shape_weight_cover[i]);
            }

            paramWriter.WriteLine("Identity Nose Parameter");
            paramWriter.WriteLine(nose_weight_cover.GetLength(0));
            for (int i = 0; i < nose_weight_cover.GetLength(0); i++)
            {
                paramWriter.WriteLine(nose_weight_cover[i]);
            }

            paramWriter.WriteLine("Identity Eye Parameter");
            paramWriter.WriteLine(eye_weight_cover.GetLength(0));
            for (int i = 0; i < eye_weight_cover.GetLength(0); i++)
            {
                paramWriter.WriteLine(eye_weight_cover[i]);
            }

            paramWriter.WriteLine("Blend Expression Parameter");

            paramWriter.WriteLine(expression_weight_cover.GetLength(0));
            for (int i = 0; i < expression_weight_cover.GetLength(0); i++)
            {
                paramWriter.WriteLine(expression_weight_cover[i]);
            }
        
            paramWriter.Close();
            
            //Save Text File
            var eyeWriter = File.CreateText(eyeFilePath);
            //////////////////////////////////////////////////////////////////////////////
            eyeWriter.WriteLine(GameManager.GetInstance._eye_class[GameManager.GetInstance._EyeClass]);
            eyeWriter.Close();

            //Save Text File
            var opWriter = File.CreateText(opFilePath);
            //////////////////////////////////////////////////////////////////////////////
            ///   

            opWriter.WriteLine("Skin");
            opWriter.WriteLine(skin_annotation);
            opWriter.WriteLine(face_texture_group);
            opWriter.WriteLine(curr_face_texture_file_per_group);

            //opWriter.WriteLine(skin_annotation[1]);

            if (GameManager.GetInstance._HairGenerateOrNot == true)
            {
                if (GameManager.GetInstance._hairShowOrNot == true)
                {
                    string hair_file_name = Hair.GetInstance.m_hair_model_names[Hair.GetInstance._hair_curr_idx];
                    string hair_name = hair_file_name.Substring(hair_file_name.Length - 3, 3);

                    opWriter.WriteLine("Hair");
                    if(Hair.GetInstance.m_hair_class_map.ContainsKey(hair_name) == true)
                    {
                        opWriter.WriteLine(Hair.GetInstance.m_hair_model_names[Hair.GetInstance._hair_curr_idx]);
                        opWriter.WriteLine(Hair.GetInstance.m_hair_class_map[hair_name].ToString());
                        opWriter.WriteLine(Hair.GetInstance._hair_h);
                        opWriter.WriteLine(Hair.GetInstance._hair_s);
                        opWriter.WriteLine(Hair.GetInstance._hair_v);
                    }
                    else
                    {
                        opWriter.WriteLine("No Registered File");
                        UnityEngine.Debug.Log("Hair Save - No Registered File");
                    }
                }
                else
                {
                    opWriter.WriteLine("Hair");
                    opWriter.WriteLine(-1);
                }
            }

            if (GameManager.GetInstance._EyeBrowsGenerateOrNot == true)
            {
                opWriter.WriteLine("EyeBrow");
                opWriter.WriteLine(eyebrow_class);
            }

            if (GameManager.GetInstance._EyeGlassGenerateOrNot == true)
            {
                opWriter.WriteLine("Glass");
                if (OpManager.GetInstance._opGlassShowOrNot == true)
                {
                    opWriter.WriteLine(OpManager.GetInstance._currGlassClass);
                }
                else
                {
                    opWriter.WriteLine(-1);
                }
            }
            opWriter.Close();
        }).Start();
        
        //////////////////////////////////////////////////////////////////////////////

        // unhide optional game object if set
        if (hideGameObject != null) hideGameObject.SetActive(true);

        // cleanup if needed
        if (optimizeForManyScreenshots == false)
        {
            Destroy(renderTexture);
            renderTexture = null;
            screenShot = null;
        }

        imgNum++;
    }


    /////////////////////////////////////////

    public void ScreenAndLandmarkSave(string fileName)
    {
        // hide optional game object if set
        if (hideGameObject != null) hideGameObject.SetActive(false);

        Camera camera = this.GetComponent<Camera>(); // NOTE: added because there was no reference to camera in original script; must add this script to Camera

        captureWidth = camera.pixelWidth;
        captureHeight = camera.pixelHeight;

        // create screenshot objects if needed
        if (renderTexture == null)
        {
            // creates off-screen render texture that can rendered into
            rect = new Rect(0, 0, captureWidth, captureHeight);
            renderTexture = new RenderTexture(captureWidth, captureHeight, 24);
            screenShot = new Texture2D(captureWidth, captureHeight, TextureFormat.RGB24, false);
        }

        // get main camera and manually render scene into rt
        camera.targetTexture = renderTexture;
        camera.Render();

        // read pixels will read from the currently active render texture so make our offscreen 
        // render texture active and then read the pixels
        RenderTexture.active = renderTexture;
        screenShot.ReadPixels(rect, 0, 0);

        // reset active camera texture and render texture
        camera.targetTexture = null;
        RenderTexture.active = null;

        //file name
        string file_name = PathManager.GetInstance._DeviceUniqueName + "_" + fileName;

        // get our unique filename
        string imgFilePath = string.Format("{0}/{1}.{2}", dbImageFolderPath, file_name, format.ToString().ToLower());
        string landmarkFilePath = string.Format("{0}/{1}.txt", dbLandmarkFolderPath, file_name);
        string parameterFilePath = string.Format("{0}/{1}.txt", dbParameterFolderPath, file_name);
 
        // pull in our file header/data bytes for the specified image format (has to be done from main thread)
        byte[] fileHeader = null;
        byte[] fileData = null;
        if (format == Format.RAW)
        {
            fileData = screenShot.GetRawTextureData();
        }
        else if (format == Format.PNG)
        {
            fileData = screenShot.EncodeToPNG();
        }
        else if (format == Format.JPG)
        {
            fileData = screenShot.EncodeToJPG();
        }
        else // ppm
        {
            // create a file header for ppm formatted file
            string headerStr = string.Format("P6\n{0} {1}\n255\n", rect.width, rect.height);
            fileHeader = System.Text.Encoding.ASCII.GetBytes(headerStr);
            fileData = screenShot.GetRawTextureData();
        }

        List<GameObject> featureObject = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_feature_object;
        List<Vector3> featureObjectPosition = new List<Vector3>(featureObject.Count);

        for (int i = 0; i < featureObject.Count; i++)
        {
            Vector3 ptInWorld = camera.WorldToScreenPoint(featureObject[i].transform.localPosition);
            Vector3 ptInWorldRescaled = new Vector3();

            ptInWorldRescaled.x = ptInWorld.x * (float)(captureWidth) / (float)camera.pixelWidth;
            ptInWorldRescaled.y = (float)(captureHeight) - ptInWorld.y * (float)(captureHeight) / (float)camera.pixelHeight - 1;
            ptInWorldRescaled.z = ptInWorld.z;

            featureObjectPosition.Add(ptInWorldRescaled);
        }

        float[] face_shape_weight_cover = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_face_shape_weight_cover;
        float[] nose_weight_cover = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_nose_weight_cover;
        float[] eye_weight_cover = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_right_eye_weight_cover;
        float[] expression_weight_cover = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_expression_blend_weight_cover;
                
        // create new thread to save the image to file (only operation that can be done in background)
        new System.Threading.Thread(() =>
        {
            // create file and write optional header with image bytes
            var f = System.IO.File.Create(imgFilePath);
            if (fileHeader != null) f.Write(fileHeader, 0, fileHeader.Length);
            f.Write(fileData, 0, fileData.Length);
            f.Close();
            Debug.Log(string.Format("Wrote screenshot {0} of size {1}", imgFilePath, fileData.Length));

            if (GameManager.GetInstance._FacialLandmarkSaveOrNot == true)
            {
                //Save Text File
                var writer = File.CreateText(landmarkFilePath);
                //////////////////////////////////////////////////////////////////////////////
                for (int i = 0; i < featureObjectPosition.Count; i++)
                {
                    writer.WriteLine(featureObjectPosition[i].x + "  " + featureObjectPosition[i].y + "  " + featureObjectPosition[i].z);
                }
                writer.Close();
            }

            //Save Text File
            var paramWriter = File.CreateText(parameterFilePath);
            //////////////////////////////////////////////////////////////////////////////

            float rotAngX = GameManager.GetInstance.headRotAng.x * 3.14159265358979323846264338327950288f / 180.0f;
            float rotAngY = GameManager.GetInstance.headRotAng.y * 3.14159265358979323846264338327950288f / 180.0f;
            float rotAngZ = GameManager.GetInstance.headRotAng.z * 3.14159265358979323846264338327950288f / 180.0f;

            paramWriter.WriteLine("Pose");
            paramWriter.WriteLine(0);
            paramWriter.WriteLine(0);
            paramWriter.WriteLine(0);

            paramWriter.WriteLine("Identity Face Parameter");
            paramWriter.WriteLine(face_shape_weight_cover.GetLength(0));
            for (int i = 0; i < face_shape_weight_cover.GetLength(0); i++)
            {
                paramWriter.WriteLine(face_shape_weight_cover[i]);
            }

            paramWriter.WriteLine("Identity Nose Parameter");
            paramWriter.WriteLine(nose_weight_cover.GetLength(0));
            for (int i = 0; i < nose_weight_cover.GetLength(0); i++)
            {
                paramWriter.WriteLine(nose_weight_cover[i]);
            }

            paramWriter.WriteLine("Identity Eye Parameter");
            paramWriter.WriteLine(eye_weight_cover.GetLength(0));
            for (int i = 0; i < eye_weight_cover.GetLength(0); i++)
            {
                paramWriter.WriteLine(eye_weight_cover[i]);
            }

            paramWriter.WriteLine("Blend Expression Parameter");

            paramWriter.WriteLine(expression_weight_cover.GetLength(0));
            for (int i = 0; i < expression_weight_cover.GetLength(0); i++)
            {
                paramWriter.WriteLine(0);
            }

            paramWriter.Close();                       

        }).Start();

        //////////////////////////////////////////////////////////////////////////////

        // unhide optional game object if set
        if (hideGameObject != null) hideGameObject.SetActive(true);

        // cleanup if needed
        if (optimizeForManyScreenshots == false)
        {
            Destroy(renderTexture);
            renderTexture = null;
            screenShot = null;
        }
    }

    /////////////////////////////////////////
    void Update()
    {
        // check keyboard 'k' for one time screenshot capture and holding down 'v' for continious screenshots
        captureScreenshot |= Input.GetKeyDown("k");
        captureVideo = Input.GetKey("v");

        if (captureScreenshot || captureVideo)
        {
            captureScreenshot = false;

            // hide optional game object if set
            if (hideGameObject != null) hideGameObject.SetActive(false);

            // create screenshot objects if needed
            if (renderTexture == null)
            {
                // creates off-screen render texture that can rendered into
                rect = new Rect(0, 0, captureWidth, captureHeight);
                renderTexture = new RenderTexture(captureWidth, captureHeight, 24);
                screenShot = new Texture2D(captureWidth, captureHeight, TextureFormat.RGB24, false);
            }

            // get main camera and manually render scene into rt
            Camera camera = this.GetComponent<Camera>(); // NOTE: added because there was no reference to camera in original script; must add this script to Camera
            camera.targetTexture = renderTexture;
            camera.Render();

            // read pixels will read from the currently active render texture so make our offscreen 
            // render texture active and then read the pixels
            RenderTexture.active = renderTexture;
            screenShot.ReadPixels(rect, 0, 0);

            // reset active camera texture and render texture
            camera.targetTexture.Release();
            RenderTexture.ReleaseTemporary(camera.targetTexture);
            camera.targetTexture = null;
            RenderTexture.active = null;

            // get our unique filename
            string filename = uniqueFilename((int)rect.width, (int)rect.height);

            // pull in our file header/data bytes for the specified image format (has to be done from main thread)
            byte[] fileHeader = null;
            byte[] fileData = null;
            if (format == Format.RAW)
            {
                fileData = screenShot.GetRawTextureData();
            }
            else if (format == Format.PNG)
            {
                fileData = screenShot.EncodeToPNG();
            }
            else if (format == Format.JPG)
            {
                fileData = screenShot.EncodeToJPG();
            }
            else // ppm
            {
                // create a file header for ppm formatted file
                string headerStr = string.Format("P6\n{0} {1}\n255\n", rect.width, rect.height);
                fileHeader = System.Text.Encoding.ASCII.GetBytes(headerStr);
                fileData = screenShot.GetRawTextureData();
            }

            List<GameObject> featureObject = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_feature_object;
            List<Vector3> featureObjectPosition = new List<Vector3>(featureObject.Count);

            for(int i = 0; i < featureObject.Count; i++)
            {
                Vector3 ptInWorld = camera.WorldToScreenPoint(featureObject[i].transform.localPosition);
                Vector3 ptInWorldRescaled = new Vector3();

                ptInWorldRescaled.x = ptInWorld.x * (float)captureWidth / (float)camera.pixelWidth;
                ptInWorldRescaled.y = ptInWorld.y * (float)captureHeight / (float)camera.pixelHeight;
                ptInWorldRescaled.z = ptInWorld.z;

                featureObjectPosition.Add(ptInWorldRescaled);
            }
                       
            // create new thread to save the image to file (only operation that can be done in background)
            new System.Threading.Thread(() =>
            {
                // create file and write optional header with image bytes
                var f = System.IO.File.Create(filename);
                if (fileHeader != null) f.Write(fileHeader, 0, fileHeader.Length);
                f.Write(fileData, 0, fileData.Length);
                f.Close();
                Debug.Log(string.Format("Wrote screenshot {0} of size {1}", filename, fileData.Length));


                //Save Text File
                var writer = File.CreateText(textFileName);
                //////////////////////////////////////////////////////////////////////////////
                for (int i = 0; i < featureObjectPosition.Count; i++)
                {
                    writer.WriteLine(featureObjectPosition[i].x + "  " + featureObjectPosition[i].y + "  " + featureObjectPosition[i].z);
                }
                writer.Close();


            }).Start();
           
            //////////////////////////////////////////////////////////////////////////////

            // unhide optional game object if set
            if (hideGameObject != null) hideGameObject.SetActive(true);

            // cleanup if needed
            if (optimizeForManyScreenshots == false)
            {
                Destroy(renderTexture);
                renderTexture = null;
                screenShot = null;
            }
        }
    }

    public void Dispose()
    {
        this.GetComponent<Camera>().targetTexture.Release();
    }
}

