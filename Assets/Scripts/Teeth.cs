﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Runtime.InteropServices;
using System.Text;

public class Teeth : MonoBehaviour {

    public GameObject target;
    Renderer m_teeth_renderer;

    Texture2D m_diffuseTex;
    Texture2D m_normalTex;

    float m_ty;
    float m_tz;
    float m_s;
    float m_rx;

    int m_teeth_top_idx;
    int m_teeth_bottom_idx;
    int m_tongue_idx;

    List<Vector3[]> m_teeth_top_data;
    List<Vector3[]> m_teeth_bottom_data;
    List<Vector3[]> m_tongue_data;

    // Use this for initialization
    void Start () {
        m_teeth_top_data = new List<Vector3[]>();
        m_teeth_bottom_data = new List<Vector3[]>();
        m_tongue_data = new List<Vector3[]>();

        for (int i = 0; i < target.transform.childCount; i++)
        {
            if(target.transform.GetChild(i).name == "teeth_top")
            {
                m_teeth_top_idx = i;
            }
            else if(target.transform.GetChild(i).name == "teeth_bottom")
            {
                m_teeth_bottom_idx = i;
            }
            else if (target.transform.GetChild(i).name == "tongue")
            {
                m_tongue_idx = i;
            }
        }

        m_teeth_top_data.Add((Vector3[])target.transform.GetChild(m_teeth_top_idx).GetComponent<MeshFilter>().mesh.vertices.Clone());
        m_teeth_bottom_data.Add((Vector3[])target.transform.GetChild(m_teeth_bottom_idx).GetComponent<MeshFilter>().mesh.vertices.Clone());
        m_tongue_data.Add((Vector3[])target.transform.GetChild(m_tongue_idx).GetComponent<MeshFilter>().mesh.vertices.Clone());
        
    }

    //유니티상에서 오리엔테이션 변경 시도했던 버젼
    public void SetTeethOrientation_2()
    {
        GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().GetTeethOrientation(ref m_ty, ref m_tz, ref m_s, ref m_rx);
        
        target.transform.localScale = new Vector3(m_s, m_s, m_s);
        //target.transform.localPosition = new Vector3(target.transform.localPosition.x, m_ty, m_tz);

        target.transform.localPosition = new Vector3(0.0f, m_ty, m_tz);

        m_rx = m_rx * 180.0f / 3.141592f;
        UnityEngine.Debug.Log("teeth bottom m_rx : " + m_rx);

        int jaw_vtx_0 = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_face_syntax._vertexIdx[GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_face_syntax._facePart["jaw"]][0];
        int jaw_vtx_2 = GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_face_syntax._vertexIdx[GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().m_face_syntax._facePart["jaw"]][2];

        float rot_pivot_y = (GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[jaw_vtx_0].y + GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[jaw_vtx_2].y) * 0.5f;
        float rot_pivot_z = (GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[jaw_vtx_0].z + GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<MeshFilter>().mesh.vertices[jaw_vtx_2].z) * 0.5f;
        
        GameObject.FindGameObjectWithTag("teeth_bottom").transform.localScale = new Vector3(m_s, m_s, m_s);
        GameObject.FindGameObjectWithTag("teeth_bottom").transform.localPosition = new Vector3(0.0f, m_ty, m_tz);
        GameObject.FindGameObjectWithTag("teeth_bottom").transform.RotateAround(new Vector3(0.0f, rot_pivot_y, rot_pivot_z), new Vector3(1, 0, 0), m_rx);

        //       target.transform.GetChild(m_teeth_bottom_idx).transform.RotateAround(new Vector3(0.0f, rot_pivot_y, rot_pivot_z), new Vector3(1, 0, 0), m_rx);
        //        target.transform.GetChild(m_tongue_idx).transform.localRotation = Quaternion.Euler(m_rx, 0.0f, 0.0f);


        //target.transform.GetChild(m_teeth_bottom_idx).transform.localRotation = Quaternion.Euler(m_rx, 0.0f, 0.0f);
        //target.transform.GetChild(m_tongue_idx).transform.localRotation = Quaternion.Euler(m_rx, 0.0f, 0.0f);
    }


    //c++ 내에서 변경하는 버젼
    public void SetTeethOrientation()
    {
        Vector3[] teeth_top_vertex = (Vector3[])target.transform.GetChild(m_teeth_top_idx).GetComponent<MeshFilter>().mesh.vertices.Clone();
        Vector3[] teeth_bottom_vertex = (Vector3[])target.transform.GetChild(m_teeth_bottom_idx).GetComponent<MeshFilter>().mesh.vertices.Clone();
        Vector3[] tongue_vertex = (Vector3[])target.transform.GetChild(m_tongue_idx).GetComponent<MeshFilter>().mesh.vertices.Clone();

        GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().SetTeethOrientation(ref m_teeth_top_data[0][0].x, ref teeth_top_vertex[0].x, teeth_top_vertex.GetLength(0), ref m_teeth_bottom_data[0][0].x, ref teeth_bottom_vertex[0].x, teeth_bottom_vertex.GetLength(0), ref m_tongue_data[0][0].x, ref tongue_vertex[0].x, tongue_vertex.GetLength(0));

        target.transform.GetChild(m_teeth_top_idx).GetComponent<MeshFilter>().mesh.vertices = teeth_top_vertex;
        target.transform.GetChild(m_teeth_bottom_idx).GetComponent<MeshFilter>().mesh.vertices = teeth_bottom_vertex;
        target.transform.GetChild(m_tongue_idx).GetComponent<MeshFilter>().mesh.vertices = tongue_vertex;


        //Mesh teeth_top_mesh = target.transform.GetChild(m_teeth_top_idx).GetComponent<MeshFilter>().mesh;
        //Mesh teeth_bottom_mesh = target.transform.GetChild(m_teeth_bottom_idx).GetComponent<MeshFilter>().mesh;
        //Mesh tongue_mesh = target.transform.GetChild(m_tongue_idx).GetComponent<MeshFilter>().mesh;

        //GameObject.FindGameObjectWithTag(GameManager.GetInstance._faceTag).GetComponent<PCA_FaceModel>().SetTeethOrientation(ref m_teeth_top_data[0][0].x, ref teeth_top_mesh.vertices[0].x, teeth_top_mesh.vertexCount, ref m_teeth_bottom_data[0][0].x, ref teeth_bottom_mesh.vertices[0].x, teeth_bottom_mesh.vertexCount, ref m_tongue_data[0][0].x, ref tongue_mesh.vertices[0].x, tongue_mesh.vertexCount);

        //target.transform.GetChild(m_teeth_top_idx).GetComponent<MeshFilter>().mesh.vertices = teeth_top_mesh.vertices;
        //target.transform.GetChild(m_teeth_bottom_idx).GetComponent<MeshFilter>().mesh.vertices = teeth_bottom_mesh.vertices;
        //target.transform.GetChild(m_tongue_idx).GetComponent<MeshFilter>().mesh.vertices = tongue_mesh.vertices;
    }

    // Update is called once per frame
    void Update () {
        //if (GameManager.GetInstance._currDeltaTime > GameManager.GetInstance._deltaTime)
        //{
        //    if (tag == "left_eye")
        //    {
        //        target.transform.localRotation = Quaternion.Euler(GameManager.GetInstance.leftEyeRotAng.x, GameManager.GetInstance.leftEyeRotAng.y, GameManager.GetInstance.leftEyeRotAng.z);
        //    }
        //    else if (tag == "right_eye")
        //    {
        //        target.transform.localRotation = Quaternion.Euler(GameManager.GetInstance.rightEyeRotAng.x, GameManager.GetInstance.rightEyeRotAng.y, GameManager.GetInstance.rightEyeRotAng.z);
        //    }
        //    //translateEyeAnchorCenter();
        //}
    }
}