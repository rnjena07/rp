﻿/*************************************************************************
*
* ILLUNI CONFIDENTIAL
* __________________
*
*  [2018] Illuni Incorporated
*  All Rights Reserved.
*
* NOTICE:  All information contained herein is, and remains
* the property of Illuni Incorporated and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Illuni Incorporated
* and its suppliers and may be covered by Republic of Korea, U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Illuni Incorporated.
*/

using System;
using System.Runtime.InteropServices;

namespace mpm
{
    public class MPM : mpmDisposeable
    {

        ////////////////////////////////////////////////////////////////////////
#if UNITY_IOS && !UNITY_EDITOR
                const string LIBNAME = "__Internal";
#else
        const string LIBNAME = "illuni_engine";
#endif

        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Release
        * @return void
        */
        [DllImport(LIBNAME)]
        private static extern void Release_MPM(IntPtr objPtr);

        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @return void
        */
        [DllImport(LIBNAME)]
        private static extern IntPtr Create_MPM();


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] filePath(string) : model.bin file data path
        * @param[in] mode(int) : 68 landmarks tracking or 51 landmarks tracking
        * @return void
        */
        [DllImport(LIBNAME)]
        private static extern int Init(IntPtr objPtr, string baseModel_path, string pca_path, string pcaPath_noseEye, string pcaPath_rightEye, string fmpmPath, string expModelPath);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] filePath(string) : model.bin file data path
        * @param[in] mode(int) : 68 landmarks tracking or 51 landmarks tracking
        * @return void
        */
        [DllImport(LIBNAME)]
        private static extern int Init_w_exp(IntPtr objPtr, string baseModel_path, string pca_path, string pcaPath_noseEye, string pcaPath_rightEye, string fmpmPath, string featureFolderPath, string expModelPath, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] string[] expList, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] string[] expFileList, int expNum);



        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Set_BaseOBJModel(IntPtr objPtr, IntPtr v, IntPtr vt, IntPtr f, int nbOfVertex, int nbOfTexCoord, int nbOfFace);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Get_MPM(IntPtr objPtr, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] face_shape_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] nose_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] eye_param, bool nose_boundary_smooth, int smooth_iter);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Get_MPBM_SpecificExpression(IntPtr objPtr, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] face_shape_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] nose_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] eye_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] bl_param, bool nose_boundary_smooth, int smooth_iter);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Get_MPM_2(IntPtr objPtr, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        //private static extern int Adjust_HairToFace(IntPtr objPtr, ref float v_src, int rows_src, int cols_src, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v_trg, int rows_trg, int cols_trg);
        private static extern int Adjust_HairToFace(IntPtr objPtr, ref float v_src, int rows_src, int cols_src, ref float v_trg, int rows_trg, int cols_trg);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Set_FFDBaseFace(IntPtr objPtr, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v_base, int rows_base, int cols_base);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Init_HairMerger(IntPtr objPtr, int width_idx_0, int width_idx_1, int depth_idx_0, int depth_idx_1, int mid_top_idx);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Load_FFDBaseFace(IntPtr objPtr, string baseModel_path);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Init_EyeMerger(IntPtr objPtr, string propertyFolder_path, string baseEyeModel_path);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Get_EyeScaleTranslationToFitFace(IntPtr objPtr, ref float v_base, int row_trg, int col_trg, ref float x1, ref float y1, ref float z1, ref float s1);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Get_NbOfFaceShapeIdentity(IntPtr objPtr);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Get_NbOfNoseIdentity(IntPtr objPtr);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Get_NbOfEyeIdentity(IntPtr objPtr);


        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Get_NbOfBlendShapeExpression(IntPtr objPtr);

        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Get_TestPing(IntPtr objPtr);

        [DllImport(LIBNAME)]
        private static extern int Get_Contour_Vertex(IntPtr objPtr, ref float v, ref float contour_vertex);

        [DllImport(LIBNAME)]
        private static extern int Calc_Geodesic_Curve_And_Neighborhood_Vertex_ID(IntPtr objPtr, ref float v);

        [DllImport(LIBNAME)]
        private static extern int Get_Geodesic_Neighborhood_Vertex_Size_0(IntPtr objPtr, int i);

        [DllImport(LIBNAME)]
        private static extern int Get_Geodesic_Neighborhood_Vertex_Size_1(IntPtr objPtr, int i, int j);

        [DllImport(LIBNAME)]
        private static extern int Get_Geodesic_Neighborhood_Vertex_Size_2(IntPtr objPtr, int i, int j, int k);

        [DllImport(LIBNAME)]
        private static extern int Get_Geodesic_Neighborhood_Vertex_ID(IntPtr objPtr, int i, int j, int k, int l);

        [DllImport(LIBNAME)]
        private static extern int Get_Geodesic_Path(IntPtr objPtr, int i, ref float path, ref int size);

        [DllImport(LIBNAME)]
        private static extern float Get_Head_OP_Scale_X(IntPtr objPtr);

        [DllImport(LIBNAME)]
        private static extern float Get_Head_OP_Scale_Y(IntPtr objPtr);

        [DllImport(LIBNAME)]
        private static extern float Get_Head_OP_Scale_Z(IntPtr objPtr);

        [DllImport(LIBNAME)]
        private static extern int Set_Param_Cover_Map_With_Constant(IntPtr objPtr, float identity_face_weight_cover_map_const, float identity_nose_weight_cover_map_const, float identity_eye_weight_cover_map_const);

        [DllImport(LIBNAME)]
        private static extern int Set_Param_Cover_Map(IntPtr objPtr, ref float identity_face_weight_cover_map, int identity_face_weight_cover_length, ref float identity_nose_weight_cover_map_const, int identity_nose_weight_cover_length, ref float identity_eye_weight_cover_map_const, int identity_eye_weight_cover_length);

        /**
        * @author     BH Park
        * @version    2018-05-03
        * @brief      NPDFNet Initialization
        * @param[in] iImg(IntPtr) : input image object
        * @param[in] rect(int[]) : input face region on iImage
        * @param[output] landmark(IntPtr) : output estimated facial landmarks
        * @return estimation score or error code(-1 : iImage error, -2 : rect error)
        */
        [DllImport(LIBNAME)]
        private static extern int Init_TeethMerger(IntPtr objPtr, string teeth_property_path, string base_teeth_top_model_path, string base_teeth_bottom_model_path, string base_tongue_model_path);

        [DllImport(LIBNAME)]
        private static extern int Get_Teeth_Orientation(IntPtr objPtr, ref float ty, ref float tz, ref float s, ref float rx);

        [DllImport(LIBNAME)]
        private static extern int Set_Teeth_Orientation(IntPtr objPtr, ref float v_teeth_top_original, ref float v_teeth_top, int v_teeth_top_count, ref float v_teeth_bottom_original, ref float v_teeth_bottom, int v_teeth_bottom_count, ref float v_tongue_original, ref float v_tongue, int v_tongue_count);

        [DllImport(LIBNAME)]
        private static extern int Get_Vertex_From_UV(IntPtr objPtr, ref float target_uv, int num_of_target_uv, ref float v, ref float vertex);

        [DllImport(LIBNAME)]
        private static extern int Hair_FFD_Registration(IntPtr objPtr, ref float hair_vertex, int nb_ob_vertex, string obj_name);

        [DllImport(LIBNAME)]
        private static extern int Get_Hair_Vertex_From_Head_FFD(IntPtr objPtr, ref float head_vertex, ref float hair_render_vertex, string obj_name);

        [DllImport(LIBNAME)]
        private static extern int EyeGlass_FFD_Registration(IntPtr objPtr, ref float eyeglass_vertex, int nb_ob_vertex, string obj_name);

        [DllImport(LIBNAME)]
        private static extern int Get_EyeGlass_Vertex_From_Head_FFD(IntPtr objPtr, ref float eyeglass_vertex, ref float eyeglass_render_vertex, string obj_name);

        [DllImport(LIBNAME)]
        private static extern int Op_FFD_Registration(IntPtr objPtr, ref float op_vertex, int nb_ob_vertex, string obj_name);

        [DllImport(LIBNAME)]
        private static extern int Get_Op_Vertex_From_Head_FFD(IntPtr objPtr, ref float op_vertex, ref float op_render_vertex, string obj_name);

        ////////////////////////////////////////////////////////////////////////
        public MPM()
        {
            nativeObj = Create_MPM();
        }

        public int Init(string baseModel_path, string pca_path, string pcaPath_noseEye, string pcaPath_rightEye, string fmpmPath, string expModelPath)
        {
            if (nativeObj == IntPtr.Zero)
            {
                return -1;
            }
            return Init(nativeObj, baseModel_path, pca_path, pcaPath_noseEye, pcaPath_rightEye, fmpmPath, expModelPath);
        }

        public int Init_W_Exp(string baseModel_path, string pca_path, string pcaPath_noseEye, string pcaPath_rightEye, string fmpmPath, string featureFolderPath, string expModelPath, string[] expList, string[] expFileList)
        {
            if (nativeObj == IntPtr.Zero)
            {
                return -1;
            }

            return Init_w_exp(nativeObj, baseModel_path, pca_path, pcaPath_noseEye, pcaPath_rightEye, fmpmPath, featureFolderPath, expModelPath, expList, expFileList, expList.Length);
        }

        public int SetBaseOBJModel(IntPtr v, IntPtr vt, IntPtr f, int nbOfVertex, int nbOfTexCoord, int nbOfFace)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Set_BaseOBJModel(nativeObj, v, vt, f, nbOfVertex, nbOfTexCoord, nbOfFace);
        }

        public int GetMPM([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] face_shape_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] nose_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] eye_param, bool nose_boundary_smooth, int smooth_iter)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_MPM(nativeObj, v, face_shape_param, nose_param, eye_param, nose_boundary_smooth, smooth_iter);
        }

        public int GetMPBM_SpecificExpression([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] face_shape_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] nose_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] eye_param, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] bl_param, bool nose_boundary_smooth, int smooth_iter)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_MPBM_SpecificExpression(nativeObj, v, face_shape_param, nose_param, eye_param, bl_param, nose_boundary_smooth, smooth_iter);
        }

        public int SetFFDBaseFace([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v_base, int rows_base, int cols_base)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Set_FFDBaseFace(nativeObj, v_base, rows_base, cols_base);
        }


        public int LoadFFDBaseFace(string baseModel_path)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Load_FFDBaseFace(nativeObj, baseModel_path);
        }


        public int InitHairMerger(int width_idx_0, int width_idx_1, int depth_idx_0, int depth_idx_1, int mid_top_idx)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Init_HairMerger(nativeObj, width_idx_0, width_idx_1, depth_idx_0, depth_idx_1, mid_top_idx);
        }


        //public int AdjustHairToFace([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v_src, int rows_src, int cols_src, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v_trg, int rows_trg, int cols_trg)
        //{
        //    if (nativeObj == IntPtr.Zero)
        //        return -1;

        //    return Adjust_HairToFace(nativeObj, v_src, rows_src, cols_src, v_trg, rows_trg, cols_trg);
        //}

        //public int AdjustHairToFace(ref float v_src, int rows_src, int cols_src, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v_trg, int rows_trg, int cols_trg)
        //{
        //    if (nativeObj == IntPtr.Zero)
        //        return -1;

        //    return Adjust_HairToFace(nativeObj, ref v_src, rows_src, cols_src, v_trg, rows_trg, cols_trg);
        //}


        public int AdjustHairToFace(ref float v_src, int rows_src, int cols_src, ref float v_trg, int rows_trg, int cols_trg)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Adjust_HairToFace(nativeObj, ref v_src, rows_src, cols_src, ref v_trg, rows_trg, cols_trg);
        }

        public int InitEyeMerger(string propertyFolder_path, string baseEyeModel_path)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Init_EyeMerger(nativeObj, propertyFolder_path, baseEyeModel_path);
        }

        public int GetEyeScaleTranslationToFitFace(ref float trg_model, int row_trg, int col_trg, ref float x1, ref float y1, ref float z1, ref float s1)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_EyeScaleTranslationToFitFace(nativeObj, ref trg_model, row_trg, col_trg, ref x1, ref y1, ref z1, ref s1);
        }

        public int GetMPM_2([MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)] float[] v)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_MPM_2(nativeObj, v);
        }

        public int GetNbOfFaceShapeIdentity()
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_NbOfFaceShapeIdentity(nativeObj);
        }

        public int GetNbOfNoseIdentity()
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_NbOfNoseIdentity(nativeObj);
        }

        public int GetNbOfEyeIdentity()
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_NbOfEyeIdentity(nativeObj);
        }

        public int GetNbOfBlendShapeExpression()
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_NbOfBlendShapeExpression(nativeObj);
        }

        public int GetContourVertex(ref float v, ref float contour_vertex)
        {
            if (nativeObj == IntPtr.Zero)
            {
                return -1;
            }

            return Get_Contour_Vertex(nativeObj, ref v, ref contour_vertex);
        }

        public int CalcGeodesicCurveAndNeighborhoodVertexID(ref float v)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Calc_Geodesic_Curve_And_Neighborhood_Vertex_ID(nativeObj, ref v);
        }

        public int GetGeodesicNeighborhoodSize_0(int i)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Geodesic_Neighborhood_Vertex_Size_0(nativeObj, i);
        }

        public int GetGeodesicNeighborhoodSize_1(int i, int j)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Geodesic_Neighborhood_Vertex_Size_1(nativeObj, i, j);
        }

        public int GetGeodesicNeighborhoodSize_2(int i, int j, int k)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Geodesic_Neighborhood_Vertex_Size_2(nativeObj, i, j, k);
        }


        public int GetGeodesicNeighborhoodVertexID(int i, int j, int k, int l)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Geodesic_Neighborhood_Vertex_ID(nativeObj, i, j, k, l);
        }

        public int GetGeodesicPath(int i, ref float path, ref int size)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Geodesic_Path(nativeObj, i, ref path, ref size);
        }

        public float GetHeadOPScaleX()
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Head_OP_Scale_X(nativeObj);
        }

        public float GetHeadOPScaleY()
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Head_OP_Scale_Y(nativeObj);
        }

        public float GetHeadOPScaleZ()
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Head_OP_Scale_Z(nativeObj);
        }

        public int InitTeethMerger(string teeth_property_path, string base_teeth_top_model_path, string base_teeth_bottom_model_path, string base_tongue_model_path)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Init_TeethMerger(nativeObj, teeth_property_path, base_teeth_top_model_path, base_teeth_bottom_model_path, base_tongue_model_path);
        }

        public int GetTeethOrientation(ref float ty, ref float tz, ref float s, ref float rx)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Teeth_Orientation(nativeObj, ref ty, ref tz, ref s, ref rx);
        }

        public int SetTeethOrientation(ref float v_teeth_top_original, ref float v_teeth_top, int v_teeth_top_count, ref float v_teeth_bottom_original, ref float v_teeth_bottom, int v_teeth_bottom_count, ref float v_tongue_original, ref float v_tongue, int v_tongue_count)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Set_Teeth_Orientation(nativeObj, ref v_teeth_top_original, ref v_teeth_top, v_teeth_top_count, ref v_teeth_bottom_original, ref v_teeth_bottom, v_teeth_bottom_count, ref v_tongue_original, ref v_tongue, v_tongue_count);
        }

        public int SetParamCoverMapWithConstant(float identity_face_weight_cover_map_const, float identity_nose_weight_cover_map_const, float identity_eye_weight_cover_map_const)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Set_Param_Cover_Map_With_Constant(nativeObj, identity_face_weight_cover_map_const, identity_nose_weight_cover_map_const, identity_eye_weight_cover_map_const);
        }

        public int SetParamCoverMap(ref float identity_face_weight_cover_map, int identity_face_weight_cover_map_length, ref float identity_nose_weight_cover_map, int identity_nose_weight_cover_map_length, ref float identity_eye_weight_cover_map, int identity_eye_weight_cover_map_length)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Set_Param_Cover_Map(nativeObj, ref identity_face_weight_cover_map, identity_face_weight_cover_map_length, ref identity_nose_weight_cover_map, identity_nose_weight_cover_map_length, ref identity_eye_weight_cover_map, identity_eye_weight_cover_map_length);
        }

        public int GetVertexFromUV(ref float target_uv, int num_of_target_uv, ref float v, ref float vertex)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Vertex_From_UV(nativeObj, ref target_uv, num_of_target_uv, ref v, ref vertex);
        }

        public int HairFFDRegistration(ref float hair_vertex, int nb_ob_vertex, string obj_name)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Hair_FFD_Registration(nativeObj, ref hair_vertex, nb_ob_vertex, obj_name);
        }

        public int GetHairVertexFromHead(ref float head_vertex, ref float hair_render_vertex, string obj_name)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Hair_Vertex_From_Head_FFD(nativeObj, ref head_vertex, ref hair_render_vertex, obj_name);
        }

        public int EyeGlassFFDRegistration(ref float eyeglass_vertex, int nb_ob_vertex, string obj_name)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return EyeGlass_FFD_Registration(nativeObj, ref eyeglass_vertex, nb_ob_vertex, obj_name);
        }

        public int GetEyeGlassVertexFromHead(ref float eyeglass_vertex, ref float eyeglass_render_vertex, string obj_name)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_EyeGlass_Vertex_From_Head_FFD(nativeObj, ref eyeglass_vertex, ref eyeglass_render_vertex, obj_name);
        }

        public int OpFFDRegistration(ref float op_vertex, int nb_ob_vertex, string obj_name)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Op_FFD_Registration(nativeObj, ref op_vertex, nb_ob_vertex, obj_name);
        }

        public int GetOpVertexFromHead(ref float op_vertex, ref float op_render_vertex, string obj_name)
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_Op_Vertex_From_Head_FFD(nativeObj, ref op_vertex, ref op_render_vertex, obj_name);
        }

        public int GetTestPing()
        {
            if (nativeObj == IntPtr.Zero)
                return -1;

            return Get_TestPing(nativeObj);
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                }
                if (IsEnabledDispose)
                {
                    if (nativeObj != IntPtr.Zero)
                        Release_MPM(nativeObj);
                    nativeObj = IntPtr.Zero;
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }
    }
}
