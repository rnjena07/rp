﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor.ShaderGraph;
using System.Reflection;
[Title("Input", "Scene", "LightDirection")]
public class LightDirectionNode : CodeFunctionNode
{
    public LightDirectionNode()
    {
        name = "LightDirection";
    }
    public override bool hasPreview
    {
        get { return true; }
    }
    protected override MethodInfo GetFunctionToConvert()
    {
        return GetType().GetMethod("LightDirectionFunction",
            BindingFlags.Static | BindingFlags.NonPublic);
    }

    static string LightDirectionFunction(
        [Slot(0, Binding.None)] out Vector3 Out)
    {
        Out = Vector3.zero;
        return
            @"
{
float3 ff = 1;
float3 _MainLightPosition_ = 0;
#if SHADER_HINT_NICE_QUALITY
_MainLightPosition_ = _MainLightPosition;
#else
        _MainLightPosition_ = float3(0.02,0.02,0.02);
#endif

Out = _MainLightPosition_;
} 
";
    }
    public override PreviewMode previewMode
    {
        get
        {
            return PreviewMode.Preview2D;
        }
    }
}
#endif