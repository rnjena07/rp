﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor.ShaderGraph;
using System.Reflection;
[Title("UV", "ParallaxOcclusionMapping")]
public class NodeParallaxOcclusionMapping : CodeFunctionNode
{
    public NodeParallaxOcclusionMapping()
    {
        name = "ParallaxOcclusionMapping";
    }

    protected override MethodInfo GetFunctionToConvert()
    {
        return GetType().GetMethod("ParallaxOcclusionMappingFunction",
        BindingFlags.Static | BindingFlags.NonPublic);
    }

    static string ParallaxOcclusionMappingFunction(
    [Slot(0, Binding.MeshUV0)] Vector2 UV,
    [Slot(1, Binding.TangentSpaceNormal)] Vector3 ViewDir_Tan,
    [Slot(2, Binding.None)] Texture2D Height,
    [Slot(3, Binding.None)] Vector1 Depth,
    [Slot(4, Binding.None)] Vector1 linearSearchSteps_Int,
    [Slot(5, Binding.None, 1.0f, 1.0f, 1.0f, 1.0f)] Vector1 UseChanel_1_4_RGBA,
    [Slot(6, Binding.None)] SamplerState Sampler_NECESSARY,
    [Slot(7, Binding.None)] Vector1 LWRP_HDRP_0_1,
    [Slot(8, Binding.None)] out Vector2 Out)
    {
        Out = Vector2.zero;

        return
        @" 
{ 
float3 ViewDir = 0;
if (LWRP_HDRP_0_1 == 0) {
ViewDir = (float3((0.0 + (ViewDir_Tan.x - 0.0) * (8.0 - 0.0) / (1.0 - 0.0)) , ( ViewDir_Tan.y * -1.0 ) , ( ViewDir_Tan.z * -1.0 )));
}
if (LWRP_HDRP_0_1 == 1) {
ViewDir = ViewDir_Tan;
}
            
             float3 p = float3(UV, 0);
             float3 newView = normalize(ViewDir * -1);
             newView.z = abs(newView.z);
            
             float depthBias = 1.0 - newView.z;
             depthBias *= depthBias;
             depthBias *= depthBias;
             depthBias = 1.0 - depthBias * depthBias;
            
             newView.xy *= depthBias;
             newView.xy *= Depth;
            
             const int binarySearchSteps = 10;
            
             newView /= newView.z * linearSearchSteps_Int;
            
             //produces depth
             int i;
             for( i=0; i<linearSearchSteps_Int; i++){
float tex = 0;
if (UseChanel_1_4_RGBA > -0.01 && UseChanel_1_4_RGBA < 2) { //use Red
tex = 1 - SAMPLE_TEXTURE2D (Height,Sampler_NECESSARY,p.xy).r;
}

if (UseChanel_1_4_RGBA > 1.5 && UseChanel_1_4_RGBA < 2.5) {// use Green
tex = 1 - SAMPLE_TEXTURE2D (Height,Sampler_NECESSARY,p.xy).g;
}

if (UseChanel_1_4_RGBA > 2.5 && UseChanel_1_4_RGBA < 3.5) {// use Blue
tex = 1 - SAMPLE_TEXTURE2D (Height,Sampler_NECESSARY,p.xy).b;
}
if (UseChanel_1_4_RGBA > 3.9) { // use Alpha
tex = 1 - SAMPLE_TEXTURE2D (Height,Sampler_NECESSARY,p.xy).a;
}

              if (p.z<tex) p+=newView;
             }
            
             for( i=0; i<binarySearchSteps; i++ ){
              newView *= 0.5;
float tex = 0;
              if (UseChanel_1_4_RGBA > -0.01 && UseChanel_1_4_RGBA < 2) { //use Red
tex = 1 - SAMPLE_TEXTURE2D (Height,Sampler_NECESSARY,p.xy).r;
}

if (UseChanel_1_4_RGBA > 1.5 && UseChanel_1_4_RGBA < 2.5) {// use Green
tex = 1 - SAMPLE_TEXTURE2D (Height,Sampler_NECESSARY,p.xy).g;
}

if (UseChanel_1_4_RGBA > 2.5 && UseChanel_1_4_RGBA < 3.5) {// use Blue
tex = 1 - SAMPLE_TEXTURE2D (Height,Sampler_NECESSARY,p.xy).b;
}
if (UseChanel_1_4_RGBA > 3.9) { // use Alpha
tex = 1 - SAMPLE_TEXTURE2D (Height,Sampler_NECESSARY,p.xy).a;
}

              if(p.z < tex) p+= newView;
              else p -= newView;
             }

Out = p; 
} 
";
    }
}
#endif