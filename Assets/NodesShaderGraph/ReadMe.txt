When using ParallaxMapping or CreateNormalChannel, be sure to set Sampler State otherwise there will be errors

In order for Parallax Occlusion Mapping to work correctly, you must set the correct value of the variable "LWRP_HDRP_0_1" in the node
0 = LWRP;
1 = HDRP;

!!! If the material becomes pink or does not work properly, open the shader in the Shader Graph and click Save, the problem disappears