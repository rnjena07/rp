import maya.cmds as cmds
import re
import sys

fileName = r"E:\Research\Databases\face\3D\illuni\BaseFaceModel\Shape\Face\Ver_2\property\face_skin_face_list.txt"


sel = cmds.ls(sl=True, fl=True)
#print sel

NbOfSelect = len(sel)


f = open(fileName, 'w')

for x in range(0, NbOfSelect):
    tempString = sel[x]
    vertexIndex = int(re.search(r'\d+', tempString).group())
    print vertexIndex
    f.write("{0}\n".format(vertexIndex))
  
f.close()
