#FaceParts
m_vForeHead	closed
m_vWholeFace	closed
m_vLeftEyebrow	open	283	285	289	331	292
m_vRightEyebrow open	1620	1659	1617	1613	1611
m_vLeftEye	closed	2901	2883	2886	2892	2895	2898
m_vRightEye	closed	2869	2863	2860	2878	2875	2872
m_vEyeCornerPoints	open	2878	2901
m_vEyeLine	open	
m_vNose		open	0	208	66	43	1518	1496	58	167	189
m_vNostril	open	567	550	42	1867	1885
m_vNoseTip	open	43
m_vLipOuterLine	closed	1706	1719	1663	392	337	397	382	408	412	411	1733	1730
m_vLipInnerLine	closed	1749	1772	454	457	430	445	449	1763
m_vMouthCornerPoints	open	1706	382
m_vJaw		open	2044	658	751
m_vEyeLine	open
m_vLipUpperOuterLine	open
m_vLipUpperInnerLine	open
m_vLipLowerInnerLine	open
m_vLipLowerOuterLine	open
m_vLeftCheek		closed
m_vRightCheek		closed
m_vPitchAxisLinePoints	open	2310	1017
m_vMidlinePoints	open	1281
m_vNoseAlae		open	1435	104